// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');
var argscheck = require('cordova/argscheck');
var AudioUnit = require('./FMODAudioUnit');



/**
 * Allows you to save the audio signal from any audio source into a Wave file.
 * 
 * @param {String} uuid - (optional) The recorder unique ID, it will be generated if null or omitted.
 * @class
 */
class FMODRecorder extends AudioUnit {
    constructor(uuid) {
        if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
        super('recorder', uuid);

        var onError = function (e) {
            console.error('Unable to initialize FMOD recorder:', e);
        };

        // create internal input instance
        cordova.exec(null, onError, "FMODRecorder", "init", [{ uuid: this.uuid }]);
    }
}



/**
 * Closes and frees the recorder and its resources.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODRecorder", "close", [this.uuid]);
},

/**
 * Starts the recording.
 *
 * To be easily loaded by your browser, the given file path must be relative to your root directory
 * or any accessible directories. If the file path is not absolute, the final path will be relative
 * to your app temporary (IOS) or external cache (Android) directory.
 * You could use the Cordava File plugin to create the appriopriate path or retrieve the file content.
 * Any folders specified in path will be created if not exists.
 * Any created files will be deleted on app terminate.
 *
 * @param {string} filepath - The path of the file where the audio data will be saved.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.start = function (filepath, onSuccess, onError) {
    if (!filepath) {
        const message = `Invalid audio file path: ${filepath}`;
        if (onError) onError(message);
        else console.warn(message);
    }
    else cordova.exec(onSuccess, onError, "FMODRecorder", "start", [{ filepath, uuid: this.uuid }]);
},

/**
 * Stops the recording.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODRecorder", "stop", [this.uuid]);
},

/**
 * Sets the audio signal to be recorded.
 *
 * @param {FMODAudioUnit} audio - The audio input.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.setInput = function (audio, onSuccess, onError) {
    argscheck.checkArgs('oFF', 'FMODRecorder.setInput', arguments);
    if (audio.constructor.name === 'FMODAudioUnit' || Object.getPrototypeOf(audio.constructor).name === 'FMODAudioUnit') {
        cordova.exec(onSuccess, onError, "FMODRecorder", "setInput", [{ audio, uuid: this.uuid }]);
    }
    else {
        const message = `Invalid audio unit: ${audio.constructor.name}`;
        if (onError) onError(message);
        else console.warn(message);
    }
},

/**
 * Sets the sample rate for the audio file: 8000, 11025, 16000, 22050, 32000, 44100, 48000, 88200,
 * 96000, 176400 or 192000 Hz.
 *
 * @param {int} samplerate — The sample rate for the audio file.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.setSampleRate = function (samplerate, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODRecorder", "setSampleRate", [{ samplerate, uuid: this.uuid }]);
},

/**
 * Sets the audio format for the audio file: 8, 16, 24 or 32 bit.
 *
 * @param {int} format - The audio format or bit depth for the audio file.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.setAudioFormat = function (format, onSuccess, onError) {
    const available = [1, 2, 3, 4, 8, 16, 24, 32];
    if (format && available.indexOf(format) !== -1) {
        if (format > 4) format /= 8;
        cordova.exec(onSuccess, onError, "FMODRecorder", "setAudioFormat", [{ format, uuid: this.uuid }]);
    }
    else {
        const message = `Invalid audio format: ${format}`;
        if (onError) onError(message);
        else console.warn(message);
    }
},

/**
 * Allows you to start the effective recording only when the sound is no longer considered silent, i.e. from a minimum sound level.
 *
 * @param {boolean} enable - Whether the leading silence should be considered or not.
 * @param {int} level - The minimum audio level (in dB) from where the effective recording should start.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.ignoreLeadingSilence = function (enable, level, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODRecorder", "ignoreLeadingSilence", [{ enable, level, uuid: this.uuid }]);
}

/**
 * Register the recorder ended callback.
 *
 * @param {Fmod~endedCallback-success} success — The callback that handles the recording end event.
 * @param {Fmod~endedCallback-error} error - The callback function to be called on error.
 */
FMODRecorder.prototype.registerEndedCallback = function (success, error) {
    cordova.exec(success, error, "FMODRecorder", "registerEndedCallback", [this.uuid]);
},

/**
 * Unregister the recorder ended callback
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODRecorder.prototype.unregisterEndedCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODRecorder", "unregisterEndedCallback", [this.uuid]);
},



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */

/**
 * The function to be invoked to handle recording end event.
 *
 * @callback Fmod~endedCallback-success
 * @param {String} path — The recorded audio file path.
 */

/**
 * The function to be invoked to handle recording error event.
 *
 * @callback Fmod~endedCallback-error
 * @param {String} message — The error message.
 */

module.exports = FMODRecorder;
