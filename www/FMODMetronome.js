// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');



/**
 * Allow to manage a DSP clock.
 * @class
 */
var FMODMetronome = function (uuid) {
    if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
    this.uuid = uuid;

    var onError = function (e) {
        console.error('Unable to initialize FMOD metronome:', e);
    };

    // create internal metronome
    cordova.exec(null, onError, "FMODMetronome", "init", [this.uuid]);
}





/**
 * Closes the metronome.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODMetronome", "close", [this.uuid]);
},

/**
 * Starts the metronome.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.start = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODMetronome", "start", [this.uuid]);
},

/**
 * Stops the metronome.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODMetronome", "stop", [this.uuid]);
},

/**
 * Reset the metronome clock.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.reset = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODMetronome", "reset", [this.uuid]);
},

/**
 * Register the metronome event callback.
 *
 * @param {Fmod~metroEventCallback} callback — The callback that handles metronome events.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.registerEventCallback = function (callback, onError) {
    cordova.exec(callback, onError, "FMODMetronome", "registerEventCallback", [this.uuid]);
},

/**
 * Unregister the metronome event callback.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.unregisterEventCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODMetronome", "unregisterEventCallback", [this.uuid]);
},

/**
 * Sets the beats frequency in BPM.
 * (0 means no cycle)
 *
 * @param {int} bpm - The metronome frequency in beats per minute.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.setBeatsPerMinute = function (bpm, onSuccess, onError) {
    if (!isFinite(bpm)) onError('Invalid BPM value');
    else cordova.exec(onSuccess, onError, "FMODMetronome", "setBeatsPerMinute", [{ bpm, uuid: this.uuid }]);
},

/**
 * Sets the beat period in samples.
 * (0 means no cycle)
 *
 * @param {int} samples - The metronome period in samples.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.setBeatPeriodInSamples = function (samples, onSuccess, onError) {
    if (!isFinite(samples)) onError('Invalid samples value');
    else cordova.exec(onSuccess, onError, "FMODMetronome", "setBeatPeriodInSamples", [{ samples, uuid: this.uuid }]);
},

/**
 * Sets the beat period in milliseconds.
 * (0 means no cycle)
 *
 * @param {float} ms - The metronome period in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.setBeatPeriodInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODMetronome", "setBeatPeriodInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the DSP clock to the given number of samples.
 *
 * @param {int} samples - The metronome clock in samples. 
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.setDSPClock = function (samples, onSuccess, onError) {
    if (!isFinite(samples)) onError('Invalid samples value');
    else cordova.exec(onSuccess, onError, "FMODMetronome", "setDSPClock", [{ samples, uuid: this.uuid }]);
},

/**
 * Sets the DSP clock to the specified milliseconds.
 *
 * @param {float} ms - The metronome clock in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.setDSPClockInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODMetronome", "setDSPClockInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Retrieves information about the internal DSP clock and DSP cycle usage.
 *
 * @param {Fmod~getDSPUsage} callback - The callback that handles the DSP usage data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODMetronome.prototype.getDSPUsage = function (callback, onError) {
    cordova.exec(callback, onError, "FMODMetronome", "getDSPUsage", [this.uuid]);
},



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */

/**
 * The function to be invoked to handle the metronome events.
 *
 * @callback Fmod~metroEventCallback
 * @param {Number} step — The current DSP clock step.
 */

/**
 * The function to be invoked to handle new DSP clock and DSP cycle usage data.
 *
 * The DSP usage data contains the following keys:
 *  - (int) samples, DSP clock in samples.
 *  - (float) millisecondes, DSP clock in milliseconds.
 *  - (float) latency, the latest execution time between two DSP cycles in milliseconds.
 *  - (float) expectedLatency, the expected time between two DSP cycles in milliseconds.
 *
 * @callback Fmod~getDSPUsage
 * @param {Object} usage — the DSP usage data.
 */

module.exports = FMODMetronome;
