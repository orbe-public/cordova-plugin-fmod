// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');
var argscheck = require('cordova/argscheck');
var AudioUnit = require('./FMODAudioUnit');



/**
 * Performs various audio analyses on a specified input signal.
 * 
 * @param {String} uuid - (optional) The input object unique ID, it will be generated if null or omitted.
 * @class
 */
class FMODAnalyser extends AudioUnit {
    constructor(uuid) {
        if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
        super('analyser', uuid);

        var onError = function (e) {
            console.error('Unable to initialize FMOD analyser:', e);
        };

        // create internal input instance
        cordova.exec(null, onError, "FMODAnalyser", "init", [this.uuid]);
    }
}



/**
 * Closes and frees an audio analyser and its resources.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODAnalyser", "close", [this.uuid]);
}, 

/**
 * Starts audio analysis for the specified options.
 * 
 * The available options / analyse type are:
 *  - 'metering', retrieve the signal metering information (peak and RMS level per channel)
 *  - 'fft', the FFT (Fast Fourier Transform) option analyzes the signal and provides information
 *     about its frequency spectrum (currently, only the spectral centroid).
 * 
 * @param (Array) options - The list of audio analyses requested.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 * 
 * @see {Fmod~analyserCallback} callback
 */
FMODAnalyser.prototype.start = function (options, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODAnalyser", "start", [{ options, uuid: this.uuid }]);
},

/**
 * Stops audio analysis.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODAnalyser", "stop", [this.uuid]);
},

/**
 * Sets the audio signal to be analysed.
 *
 * @param {FMODAudioUnit} audio - The audio input.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.setInput = function (audio, onSuccess, onError) {
    argscheck.checkArgs('oFF', 'FMODAnalyser.setInput', arguments);
    if (audio.constructor.name === 'FMODAudioUnit' || Object.getPrototypeOf(audio.constructor).name === 'FMODAudioUnit') {
        cordova.exec(onSuccess, onError, "FMODAnalyser", "setInput", [{ audio, uuid: this.uuid }]);
    }
    else {
        const message = `Invalid audio unit: ${audio.constructor.name}`;
        if (onError) onError(message);
        else console.warn(message);
    }
},

/**
 * Sets the interval between two analyses in milliseconds.
 * (0 means each DSP cycle)
 *
 * @param {float} ms - The time in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.setPeriodInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODAnalyser", "setPeriodInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the audio analysis callback.
 *
 * @param {Fmod~analyserCallback} callback — The callback that handles audio analysis data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.setCallback = function (callback, onError) {
    cordova.exec(callback, onError, "FMODAnalyser", "setCallback", [this.uuid]);
},

/**
 * Removes the audio analysis callback.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODAnalyser.prototype.unsetCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODAnalyser", "unsetCallback", [this.uuid]);
},



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */

/**
 * The function to be invoked to handle the audio analysis data.
 *
 * Depending on the analysis options requested, the data may contain the following keys:
 *  - (Object) fft, an object providing information about the signal frequency spectrum.
 *  - (Object) metering, an object containing the signal metering information.
 *
 * @callback Fmod~analyserCallback
 * @param {Object} data — The audio analysis data.
 */


module.exports = FMODAnalyser;
