// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.



/**
 * Provides various information about the resources created and played.
 * @class
 */

var FMOD = function () {};

// retrieves installed plugin version
FMOD.PLUGIN_VERSION = require('cordova/plugin_list').metadata['cordova-plugin-fmod'];

// supported audio formats
FMOD.AudioFormat = {
    PCM8 : 1,
    PCM16: 2,
    PCM24: 3,
    PCM32: 4
}

/** 
 * Provides informations about the internal FMOD library version.
 *
 * @param {Fmod~version} callback - The callback that handles the version data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.version = function (callback, onError) {
    cordova.exec(callback, onError, "FMOD", "getVersion", []);
},

/** 
 * Retrieves the number of currently playing channels.
 *
 * @param {Fmod~getChannelsPlaying} callback - The callback that handles the response data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.getChannelsPlaying = function (callback, onError) {
    cordova.exec(callback, onError, "FMOD", "getChannelsPlaying", []);
},

/** 
 * Retrieves the amount of CPU used for different parts of the Core engine.
 *
 * @param {Fmod~getCPUUsage} callback - The callback that handles the CPU usage data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.getCPUUsage = function (callback, onError) {
    cordova.exec(callback, onError, "FMOD", "getCPUUsage", []);
},

/**
 * Retrieves the number of milliseconds elapsed since the FMOD system was started.
 *
 * @param {Fmod~getEllapsedTime} callback - The callback that handles the response data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.getEllapsedTime = function (callback, onError) {
    cordova.exec(callback, onError, "FMOD", "getEllapsedTime", []);
}

/**
 * Suspend mixer thread and relinquish usage of audio hardware while maintaining internal state.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.suspendAudioOutput = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMOD", "suspendAudioOutput", []);
},

/**
 * Resume mixer thread and reacquire access to audio hardware.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMOD.prototype.resumeAudioOutput = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMOD", "resumeAudioOutput", []);
},

/**
 * Register callback to handle audio interruptions.
 *
 * @param {Fmod~audioInterruptCallback} callback — The callback that handles the audio interruptions.
 */
FMOD.prototype.registerAudioInterruptCallback = function (callback) {
    cordova.exec(callback, null, "FMOD", "registerAudioInterruptCallback", []);
},



/**
 * The function to be invoked to handle the FMOD version data.
 *
 * The FMOD version data contains the following keys:
 *  - (String) number, the version number.
 *  - (int) code, the version code.
 *  - (int) product, the product version number.
 *  - (int) major, the major version number.
 *  - (int) minor, the major version number.
 *
 * @callback Fmod~version
 * @param {Object} version — the version data.
 */

/**
 * The function to be invoked to handle the number of playing channels.
 *
 * @callback Fmod~getChannelsPlaying
 * @param {int} number — The number of playing channels (both real and virtual).
 */

/**
 * The function to be invoked to handle new CPU usage data.
 *
 * The CPU usage data contains the following keys:
 *  - (float) dsp, DSP mixing engine CPU usage (percentage).
 *  - (float) stream, streaming engine CPU usage (percentage).
 *  - (float) geometry, geometry engine CPU usage (percentage).
 *  - (float) update, FMOD system::update CPU usage (percentage).
 *  - (float) convolution1, convolution reverb processing thread #1 CPU usage (percentage).
 *  - (float) convolution2, convolution reverb processing thread #2 CPU usage (percentage).
 *
 * @callback Fmod~getCPUUsage
 * @param {Object} usage — the CPU usage data.
 */

/**
 * The function to be invoked to handle the number of milliseconds elapsed.
 *
 * @callback Fmod~getEllapsedTime
 * @param {int} number — The elapsed time.
 */

/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {String} response — The error message.
 */

/**
 * The function to be invoked to handle audio interruptions.
 *
 * @callback Fmod~audioInterruptCallback
 * @param {String} state — 'began': the native system has interrupted the audio session,
 *                         'ended': the interruption has ended.
 */

module.exports = FMOD;
