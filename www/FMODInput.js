// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');
var AudioUnit = require('./FMODAudioUnit');



/**
 * Provides the audio signal from the microphone or any other connected input device.
 * 
 * @param {String} uuid - (optional) The input object unique ID, it will be generated if null or omitted.
 * @class
 */
//var FMODInput = function (uuid) {
class FMODInput extends AudioUnit {
    constructor(uuid) {
        if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
        super('input', uuid);

        var onError = function (e) {
            console.error('Unable to initialize FMOD input:', e);
        };

        // create internal input instance
        cordova.exec(null, onError, "FMODInput", "init", [this.uuid]);
    }
}



/**
 * Closes and frees an audio input and its resources.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODInput.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODInput", "close", [this.uuid]);
}, 

/**
 * Starts listening to the audio input.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODInput.prototype.start = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODInput", "start", [this.uuid]);
},

/**
 * Stops listening to the audio input.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODInput.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODInput", "stop", [this.uuid]);
},

/**
 * Sets the number of channels of the audio input.
 *
 * @param {int} channels - The number of input channels: 1 for mono or 2 for stereo.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODInput.prototype.setInputChannels = function (channels, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODInput", "setInputChannels", [{ channels, uuid: this.uuid }]);
},

/**
 * Sets the audio format of the audio input: 8, 16, 24 or 32 bit.
 *
 * @param {int} format - The audio format or bit depth for the audio input signal.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODInput.prototype.setAudioFormat = function (format, onSuccess, onError) {
    const available = [1, 2, 3, 4, 8, 16, 24, 32];
    if (format && available.indexOf(format) !== -1) {
        if (format > 4) format /= 8;
        cordova.exec(onSuccess, onError, "FMODInput", "setAudioFormat", [{ format, uuid: this.uuid }]);
    }
    else {
        const message = `Invalid audio format: ${format}`;
        if (onError) onError(message);
        else console.warn(message);
    }
},



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */


module.exports = FMODInput;
