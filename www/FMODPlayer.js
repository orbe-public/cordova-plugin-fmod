// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var argscheck = require('cordova/argscheck');
var utils = require('cordova/utils');



/**
 * A complete sound file player with all the main playback features.
 * 
 * @param {String} uuid - (optional) The player unique ID, It will be generated if null or omitted.
 * @class
 */
var FMODPlayer = function (uuid)
{
    if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
    this.uuid = uuid;
    
    var onError = function(e) {
        console.error('Unable to initialize FMOD player:', e);
    };
    
    // create internal player
    cordova.exec(null, onError, "FMODPlayer", "init", [this.uuid]);
}

// loop modes
FMODPlayer.LOOP_NONE    = 0x00000001;
FMODPlayer.LOOP_NORMAL  = 0x00000002;
FMODPlayer.LOOP_BIDI    = 0x00000004;




/**
 * Closes and frees the player and its resources.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "close", [this.uuid]);
},

/**
 * Loads a sound into memory or opens it for streaming.
 *
 * @param {Fmod~playerOptions} options — The player configuration.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.load = function (options, onSuccess, onError) {
    argscheck.checkArgs('oFF', 'FMODPlayer.load', arguments);
    options.uuid = this.uuid;
    if ('buffer' in options) {
        delete options.source;
        if (options.buffer instanceof Uint8Array) options.buffer = Array.from(options.buffer);
        if (!Array.isArray(options.buffer)) {
            onError('Invalid data type. The buffer must be an instance of Uint8Array or Array');
            return;
        }
    }
    cordova.exec(onSuccess, onError, "FMODPlayer", "load", [options]);
},

/**
 * Unload a sound and cleanup all resources.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.release = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "release", [this.uuid]);
},

/**
 * Start the playback of the currently loaded sound.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.play = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "play", [this.uuid]);
},

/**
 * Stop the playback of the sound.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "stop", [this.uuid]);
},

/**
 * Retrieves the playing state.
 *
 * @param {Fmod~isPlaying} callback — The callback that handles the current playing state.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.isPlaying = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "isPlaying", [this.uuid]);
},

/**
 * Pause or resume the playback of the sound.
 *
 * @param {boolean} paused — The paused state, specifies whether to pause or resume the playback.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.pause = function (paused, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "pause", [{ paused, uuid: this.uuid }]);
},

/**
 * Retrieves the paused state.
 *
 * @param {Fmod~isPaused} callback — The callback that handles the current paused state.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.isPaused = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "isPaused", [this.uuid]);
},

/**
 * Sets the loop mode of the sound.
 *
 * @param {Fmod~playerLoopMode} mode - The loop mode of the sound.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setLoopMode = function (mode, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setLoopMode", [{ mode, uuid: this.uuid }]);
},

/**
 * Sets the volume level.
 *
 * @param {float} value - The volume level. 0 for silent, 1 for full, negative level inverts the signal, values larger than 1 amplify the signal. 
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setVolume = function (value, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setVolume", [{ value, uuid: this.uuid }]);
},

/**
 * Sets the fade in of the sound at the beginning of its playback.
 *
 * @param {int} ms - The duration of the fade in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setFadeIn = function (ms, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setFadeIn", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the fade out of the sound at the end of its playback (end of the file or manual stop).
 *
 * @param {int} ms - The duration of the fade in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setFadeOut = function (ms, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setFadeOut", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the relative pitch / playback rate.
 *
 * @param {float} rate - The pitch value where 0.5 represents half pitch (one octave down), 1.0 represents unmodified pitch and 2.0 represents double pitch (one octave up).
 * @param (boolean) shift - Enables or disables the pitch shifter who can be used to change the pitch of a sound without affecting the duration of playback. This option increases CPU usage when enabled.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setPitch = function (rate, shift, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setPitch", [{ rate, shift, uuid: this.uuid }]);
},

/**
 * Retrieves the length in milliseconds.
 *
 * @param {Fmod~getLength} callback — The callback that handles the length of the current sound.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.getLength = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "getLength", [this.uuid]);
},

/**
 * Sets the current playback position in milliseconds.
 *
 * @param {int} position - The playback position in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setPosition = function (position, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setPosition", [{ position, uuid: this.uuid }]);
},

/**
 * Retrieves the current playback position in milliseconds.
 *
 * @param {Fmod~getPosition} callback — The callback that handles the playback position.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.getPosition = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "getPosition", [this.uuid]);
},

/**
 * Sets the mute state.
 *
 * @param {boolean} muted — The muted state, specifies whether to sound is muted or not.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setMute = function (muted, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setMute", [{ muted, uuid: this.uuid }]);
},

/**
 * Retrieves the mute state.
 *
 * @param {Fmod~getMute} callback — The callback that handles the mute state.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.getMute = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "getMute", [this.uuid]);
},

/**
 * Sets the left/right pan level.
 *
 * @param {float} pan - The pan level where -1 represents full left, 0 represents center and 1 represents full right.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.setPan = function (pan, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "setPan", [{ pan, uuid: this.uuid }]);
},

/**
 * Register the player event callback.
 *
 * @param {Fmod~eventCallback} callback — The callback that handles playback events.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.registerEventCallback = function (callback, onError) {
    cordova.exec(callback, onError, "FMODPlayer", "registerEventCallback", [this.uuid]);
},

/**
 * Unregister the player event callback.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODPlayer.prototype.unregisterEventCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODPlayer", "unregisterEventCallback", [this.uuid]);
},



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */

/**
 * The player configuration options contains the following keys:
 *
 *  - (String) source, the file path to open.
 *  - (Array) buffer, alternativly to define the source as bytes array.
 *  - (Fmod~playerLoopMode) loop, the loop mode of the player (default LOOP_NONE).
 *  - (boolean) stream, whether the sound must be openedfor streaming or loads into memory (default false).
 *  - (boolean) autoplay, whether the sound must be played on load or not (default false).
 *  - (String) dls, (optional) the DLS sound bank for MIDI playback.
 *
 * @argument Fmod~playerOptions
 * @param {Object} options — The player configuration.
 */

/**
 * The loop mode of the sound.
 *
 * - LOOP_NONE, for non looping sounds.
 * - LOOP_NORMAL, for forward looping sounds.
 * - LOOP_BIDI, for forward/backward looping sounds (sound preloaded in memory only).
 *
 * @callback Fmod~playerLoopMode
 * @param {int} mode — The loop mode.
 */

/**
 * The function to be invoked to handle the current playing state.
 *
 * @callback Fmod~isPlaying
 * @param {boolean} playing — The playing state where true = started and false = stopped.
 */

/**
 * The function to be invoked to handle the current paused state.
 *
 * @callback Fmod~isPaused
 * @param {boolean} paused — The paused state where true = playback halted and false = playback active.
 */

/**
 * The function to be invoked to handle the length of the current sound.
 *
 * @callback Fmod~getLength
 * @param {int} length — The sound length in milliseconds.
 */

/**
 * The function to be invoked to handle the current playback position.
 *
 * @callback Fmod~getPosition
 * @param {int} position — The playback position in milliseconds.
 */

/**
 * The function to be invoked to handle the current mute state.
 *
 * @callback Fmod~getMute
 * @param {boolean} mute — The mute state where true = silent and false = audible.
 */

/**
 * The function to be invoked to handle playback events.
 *
 * @callback Fmod~eventCallback
 * @param {String} event — The playback event name or message.
 */

module.exports = FMODPlayer;
