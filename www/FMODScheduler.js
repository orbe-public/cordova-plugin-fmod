// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

var utils = require('cordova/utils');



/**
 * Allow to schedule playback or callback events at a specific DSP clock, as well as run as an audio sequencer.
 * @class
 */
var FMODScheduler = function(uuid)
{
    if (!uuid || typeof (uuid) == 'undefined') uuid = utils.createUUID();
    this.uuid = uuid;

    var onError = function (e) {
        console.error('Unable to initialize FMOD scheduler:', e);
    };

    // create internal scheduler
    cordova.exec(null, onError, "FMODScheduler", "init", [this.uuid]);
}

// event action (type playback)
FMODScheduler.EVENT_NONE   = 0;
FMODScheduler.EVENT_START  = 1;
FMODScheduler.EVENT_PAUSE  = 2;
FMODScheduler.EVENT_RESUME = 3;
FMODScheduler.EVENT_STOP   = 4;
// event type
FMODScheduler.EVENT_TYPE_PLAYBACK = 1;
FMODScheduler.EVENT_TYPE_CALLBACK = 2;




/**
 * Closes and frees the scheduler.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.close = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "close", [this.uuid]);
},

/**
 * Starts the scheduler.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.start = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "start", [this.uuid]);
},

/**
 * Stops the scheduler.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.stop = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "stop", [this.uuid]);
},

/**
 * Pauses the scheduler.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.pause = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "pause", [this.uuid]);
},

/**
 * Resumes the scheduler.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.resume = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "resume", [this.uuid]);
},

/**
 * Reset the scheduler clock.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.reset = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "reset", [this.uuid]);
},

/**
 * Schedules a new event.
 *
 * Event types "playback" or "callback" can be mixed. One or more events can be scheduled at the same time.
 * To execute code on your side on each event, add a callback to the setEventCallback method.
 * The event type and the even tags will be passed in order as argument.
 *
 * @param {Fmod~schedulerEvent} event - The event definition to be recorded.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 * 
 * @see setEventCallback
 */
FMODScheduler.prototype.registerEvent = function (event, onSuccess, onError) {
    if (typeof event === 'object' && !event.hasOwnProperty('type')) event.type = FMODScheduler.EVENT_TYPE_PLAYBACK; // for compatibility
    cordova.exec(onSuccess, onError, "FMODScheduler", "registerEvent", [{ event, uuid: this.uuid }]);
},

/**
 * Delete one or more event (all events matching the given tag).
 *
 * @param {String} tag — The event to be removed.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.unregisterEvent = function (tag, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "unregisterEvent", [{ tag, uuid: this.uuid }]);
},

/**
 * Deletes all recorded events.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.unregisterAllEvents = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "unregisterAllEvents", [this.uuid]);
},

/**
 * Sets events callback.
 *
 * @param {Fmod~metroEventCallback} callback — The callback that handles events.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setEventCallback = function (callback, onError) {
    var onSuccess = function (e) { callback(e.type, e.tags); };
    cordova.exec(onSuccess, onError, "FMODScheduler", "setEventCallback", [this.uuid]);
},

/**
 * Remove events callback.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.unsetEventCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "unsetEventCallback", [this.uuid]);
},

/**
 * Register the metronome event callback.
 *
 * @param {Fmod~metroEventCallback} callback — The callback that handles metronome events.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.registerMetroEventCallback = function (callback, onError) {
    cordova.exec(callback, onError, "FMODScheduler", "registerMetroEventCallback", [this.uuid]);
},

/**
 * Unregister the metronome event callback.
 *
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.unregisterMetroEventCallback = function (onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "unregisterMetroEventCallback", [this.uuid]);
},

/**
 * Attachs the scheduler to a specific external Metronome.
 *
 * @param {string} id - The external Metronome identifier.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setMetronome = function (id, onSuccess, onError) {
    cordova.exec(onSuccess, onError, "FMODScheduler", "setMetronome", [{ metronome: id, uuid: this.uuid }]);
},

/**
 * Sets the beats frequency in BPM.
 * (0 means disable)
 *
 * @param {int} bpm - The metronome frequency in beats per minute.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setBeatsPerMinute = function (bpm, onSuccess, onError) {
    if (!isFinite(bpm)) onError('Invalid BPM value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setBeatsPerMinute", [{ bpm, uuid: this.uuid }]);
},

/**
 * Sets the beat period in samples.
 * (0 means disable)
 *
 * @param {int} samples - The metronome period in samples.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setBeatPeriodInSamples = function (samples, onSuccess, onError) {
    if (!isFinite(samples)) onError('Invalid samples value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setBeatPeriodInSamples", [{ samples, uuid: this.uuid }]);
},

/**
 * Sets the beat period in milliseconds.
 * (0 means disable)
 *
 * @param {float} ms - The metronome period in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setBeatPeriodInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setBeatPeriodInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the scheduler cycle period to the given number of samples.
 * (0 means no cycle)
 *
 * @param {int} samples - The scheduler cycle in samples. 
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setPeriod = function (samples, onSuccess, onError) {
    if (!isFinite(samples)) onError('Invalid samples value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setPeriod", [{ samples, uuid: this.uuid }]);
},

/**
 * Sets the scheduler cycle period to the specified milliseconds.
 * (0 means no cycle)
 *
 * @param {float} ms - The scheduler cycle in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setPeriodInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setPeriodInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Sets the internal DSP clock to the given number of samples.
 *
 * @param {int} samples - The DSP clock in samples. 
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setClock = function (samples, onSuccess, onError) {
    if (!isFinite(samples)) onError('Invalid samples value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setClock", [{ samples, uuid: this.uuid }]);
},

/**
 * Sets the internal DSP clock to the specified milliseconds.
 *
 * @param {float} ms - The DSP clock in milliseconds.
 * @param {Fmod~success} onSuccess - The callback function to be called on success.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.setClockInMilliseconds = function (ms, onSuccess, onError) {
    if (!isFinite(ms)) onError('Invalid milliseconds value');
    else cordova.exec(onSuccess, onError, "FMODScheduler", "setClockInMilliseconds", [{ ms, uuid: this.uuid }]);
},

/**
 * Retrieves information about the internal DSP clock and DSP cycle usage.
 *
 * @param {Fmod~getDSPUsage} callback - The callback that handles the DSP usage data.
 * @param {Fmod~error} onError - The callback function to be called on error.
 */
FMODScheduler.prototype.getDSPUsage = function (callback, onError) {
    cordova.exec(callback, onError, "FMODScheduler", "getDSPUsage", [this.uuid]);
},


// for compatibility

/**
 * @deprecated since version 2.2.0, use the setClockInMilliseconds() method instead
 * @see setClockInMilliseconds
 */
FMODScheduler.prototype.setMilliseconds = function (ms, onSuccess, onError) {
    this.setClockInMilliseconds(ms, onSuccess, onError);
},

/**
 * @deprecated since version 2.2.2, use the setBeatsPerMinute() method instead
 * @see setBeatsPerMinute
 */
FMODScheduler.prototype.setMetronomeFrequency = function (bpm, onSuccess, onError) {
    this.setBeatsPerMinute(bpm, onSuccess, onError);
}

/**
 * @deprecated since version 2.2.2, use the setBeatPeriodInSamples() method instead
 * @see setBeatPeriodInSamples
 */
FMODScheduler.prototype.setMetronomePeriodInSamples = function (samples, onSuccess, onError) {
    this.setBeatPeriodInSamples(samples, onSuccess, onError);
}

/**
 * @deprecated since version 2.2.2, use the setBeatPeriodInMilliseconds() method instead
 * @see setBeatPeriodInMilliseconds
 */
FMODScheduler.prototype.setMetronomePeriodInMilliseconds = function (ms, onSuccess, onError) {
    this.setBeatPeriodInMilliseconds(ms, onSuccess, onError);
}



/**
 * The callback to execute when an action succeeded.
 *
 * @callback Fmod~success
 */

/**
 * The callback to execute when a method failed.
 *
 * @callback Fmod~error
 * @param {string} response — The error message.
 */

/**
 * The function to be invoked to handle the DSP metronome events.
 *
 * @callback Fmod~metroEventCallback
 * @param {Number} step — The current DSP clock step.
 */

/**
 * The event definition must contains all of the following keys:
 *
 *  - (int) type, the event type: FMODScheduler.EVENT_TYPE_PLAYBACK (default) or FMODScheduler.EVENT_TYPE_CALLBACK
 *  - (String) tags, one or more tags to define this event. Each tag must be separated by a comma.
 *  - (int) samples or (float) time, the DSP clock at which the action should be performed (in samples or millisecondes).
 *  - (boolean) once, whether the action should be executed only once or repeated (default) in each cycle of the scheduler.
 *
 * Optional:
 *  - (boolean) now, whether or not the event must be recorded from the current DSP clock, or time 0 (default).
 *
 * For event type "playback" (FMODScheduler.EVENT_TYPE_PLAYBACK) must also contains:
 *
 *  - (String) player, the target player identifier.
 *  - (int) action, the action to be performed: EventActions.START, EventActions.PAUSE, EventActions.RESUME or EventActions.STOP.
 *
 * Examples:
 *
 *      // type "callback"
 *      // calls the target method after 2 seconds of the scheduler cycle
 *      const event = {
 *          type: FMODScheduler.EVENT_TYPE_CALLBACK,
 *          tags: 'event, callback, 1',
 *          time: 2000
 *      };
 *
 *      // type "playback”
 *      // plays sound once at the beginning of the first scheduler cycle
 *      const event = {
 *          type: FMODScheduler.EVENT_TYPE_PLAYBACK,
 *          tags: `sound`,
 *          time: 0,
 *          player: 'xxx-xxx-xxx'
 *      };
 *
 * @argument Fmod~schedulerEvent
 * @param {Object} event — The event definition.
 */

/**
 * The function to be invoked to handle new DSP clock and DSP cycle usage data.
 *
 * The DSP usage data contains the following keys:
 *  - (int) samples, DSP clock in samples.
 *  - (float) millisecondes, DSP clock in milliseconds.
 *  - (float) latency, the latest execution time between two DSP cycles in milliseconds.
 *  - (float) expectedLatency, the expected time between two DSP cycles in milliseconds.
 *
 * @callback Fmod~getDSPUsage
 * @param {Object} usage — the DSP usage data.
 */

module.exports = FMODScheduler;
