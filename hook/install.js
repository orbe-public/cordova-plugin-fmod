const fs = require('fs');
const path = require('path');

module.exports = (context) => {
    // ios platform
    if (context.opts.cordova.platforms.includes('ios')) {
        const plateformIOSRootdir = path.join(context.opts.projectRoot, 'platforms/ios');
        const cordovaBuildConfDir = path.join(plateformIOSRootdir, 'cordova');
        const buildConfigurations = ['debug', 'release'];

        // copy config file then 
        // include it in the cordova build configuration file
        buildConfigurations.forEach((configuration) => {
            const cfg = `cordova-plugin-fmod-${configuration}.xcconfig`;
            const src = path.join(context.opts.plugin.dir, `config/ios/${cfg}`);
            const dst = path.join(plateformIOSRootdir, cfg);
            fs.copyFile(src, dst, (err) => {
                if (err) console.warn(err);
                try {
                    fs.appendFileSync(
                        path.join(cordovaBuildConfDir, `build-${configuration}.xcconfig`),
                        `\n// cordova-plugin-fmod build settings\n#include "../${cfg}"`
                    );
                }
                catch (error) {
                    console.log(error);
                }
            });
        });

        // remove libraries from frameworks build phase
        // to avoid issues during linkage
        const proj = fs.readdirSync(plateformIOSRootdir).filter(fn => fn.endsWith('.xcodeproj'));
        const pbxf = path.join(plateformIOSRootdir, proj[0], 'project.pbxproj');

        fs.readFile(pbxf, 'utf8', (err, data) => {
            if (!err) {
                const search = /.*libfmod.* in Frameworks \*\/,\n/g;
                data = data.replace(search, '');
                fs.writeFile(pbxf, data, (e) => {
                    if (e) console.warn('Failed to disable frameworks as membership for target, proceed manually before the first build');
                });
            }
        });
    }

    // android platform
    // copy the orbe-audio library CMake config, not permited via plugin.xml with multiple plugins 
    // (no override option)
    if (context.opts.cordova.platforms.includes('android')) {
        const src = path.join(context.opts.plugin.dir, `config/android/orbe-audio-lib/CMakeLists.txt`);
        const dst = path.join(context.opts.projectRoot, 'platforms/android/app/src/orbe-audio-lib/CMakeLists.txt');

        fs.stat(dst, (err, stats) => {
            if (!stats) {
                // create symlink (dev mode)
                // or copy file
                if (context.cmdLine.includes('--link')) fs.symlink(src, dst, (err) => {
                    if (err) throw err;
                });
                else fs.cp(src, dst, { force: false }, (err) => {
                    if (err) throw err;
                });
            }
        });
    }
};