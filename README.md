---
title: Cordova FMOD Plugin
description: Native audio player and scheduler for Cordova, based on FMOD core API.
---
<!--
The MIT License (MIT)
Copyright © 2021 Orbe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
# Cordova FMOD Plugin

Low-level sound file player and scheduler for Cordova, based on [FMOD core](https://fmod.com/core) API.  
This plugin is intended to cover the basics of sound on native side to avoid limitations, problems or audio artifacts on different webview technologies.

*For a commercial project, read the [FMOD license document](https://fmod.com/licensing)*



## Installation
```
cordova plugin add https://gitlab.com/orbe-public/cordova-plugin-fmod.git
```

## Supported Platforms
* Android
* IOS



## Features

* The FMOD object provides various information about the resources created and played.
* The FMODAudioUnit abstract object to connect signals between modules.
* The FMODPlayer object is a complete sound file player with all the main playback features.
* The FMODMetronome object allow to manage a DSP clock.
* The FMODScheduler object allow to schedule playback events at a specific DSP clock, as well as run as an audio sequence.
* The FMODInput object provides the audio signal from the microphone or any other connected input device (extend FMODAudioUnit).
* The FMODRecorder object allows you to save the audio signal from any audio source into a Wave file.
* The FMODAnalyser object allows various audio analyses on a specified input signal.



## Methods

### The FMOD object

FMOD.PLUGIN_VERSION, retrieves installed plugin version.  
FMOD.AudioFormat, supported audio formats

Provides informations about the internal FMOD library version.  
`version(callback, onError)`
- (String) number, the version number.
- (int) code, the version code.
- (int) product, the product version number.
- (int) major, the major version number.
- (int) minor, the major version number.

Retrieves the number of currently playing channels.  
`getChannelsPlaying(callback, onError)`
- (int) number, the number of playing channels (both real and virtual).

Retrieves the amount of CPU used for different parts of the Core engine.  
`getCPUUsage(callback, onError)`
- (float) dsp, DSP mixing engine CPU usage (percentage).
- (float) stream, streaming engine CPU usage (percentage).
- (float) geometry, geometry engine CPU usage (percentage).
- (float) update, FMOD system::update CPU usage (percentage).
- (float) convolution1, convolution reverb processing thread #1 CPU usage (percentage).
- (float) convolution2, convolution reverb processing thread #2 CPU usage (percentage).

Retrieves the number of milliseconds elapsed since the FMOD system was started.  
`getEllapsedTime(callback, onError)`
- (int) ms, The elapsed time in milliseconds.

Suspend mixer thread and relinquish usage of audio hardware while maintaining internal state.  
`suspendAudioOutput(onSuccess, onError)`

Resume mixer thread and reacquire access to audio hardware.  
`resumeAudioOutput(onSuccess, onError)`

Register callback to handle audio interruptions.  
`registerAudioInterruptCallback(callback)`
- (String) state, 'began' when the native system has interrupted the audio session or ended' when the interruption has ended.


### The FMODPlayer object

Closes and frees the player and its resources.  
`close(onSuccess, onError)`

Loads a sound into memory or opens it for streaming.  
*(Object) options, the player configuration:*  
- (String) source, the file path to open.
- (Array) buffer, alternativly to define the source as bytes array.
- (int) loop, the loop mode of the player (default LOOP_NONE).
- (boolean) stream, whether the sound must be openedfor streaming or loads into memory (default false).
- (boolean) autoplay, whether the sound must be played on load or not (default false).
- (String) dls, (optional) the DLS sound bank for MIDI playback.

`load(options, onSuccess, onError)`

Unload a sound and cleanup all resources.  
`release(onSuccess, onError)`

Start the playback of the currently loaded sound.  
`play(onSuccess, onError)`

Stop the playback of the sound.  
`stop(onSuccess, onError)`

Retrieves the playing state.    
`isPlaying(callback, onError)`

Pause or resume the playback of the sound.  
*(boolean) paused, specifies whether to pause or resume the playback.*  
`pause(paused, onSuccess, onError)`

Retrieves the paused state.  
`isPaused(callback, onError)`

Sets the loop mode of the sound.  
*(int) mode, the loop mode of the sound:*  
- LOOP_NONE, for non looping sounds.
- LOOP_NORMAL, for forward looping sounds.
- LOOP_BIDI, for forward/backward looping sounds (sound preloaded in memory only).

`setLoopMode(mode, onSuccess, onError)`

Sets the volume level.  
*(float) value, the volume level. 0 for silent, 1 for full, negative level inverts the signal, values larger than 1 amplify the signal.*   
`setVolume(value, onSuccess, onError)`

Sets the fade of the sound at the beginning of its playback.  
*(int) ms, the duration of the fade in milliseconds.*  
`setFadeIn(ms, onSuccess, onError)`

Sets the fade of the sound at the end of its playback (end of the file or manual stop).  
*(int) ms, the duration of the fade in milliseconds.*  
`setFadeOut(ms, onSuccess, onError)`

Sets the relative pitch / playback rate.  
*(float) rate, the pitch value where 0.5 represents half pitch (one octave down), 1.0 represents unmodified pitch and 2.0 represents double pitch (one octave up).*  
*(boolean) shift, enables or disables the pitch shifter who can be used to change the pitch of a sound without affecting the duration of playback. This option increases CPU usage when enabled.*  
`setPitch(rate, shift, onSuccess, onError)`

Retrieves the length in milliseconds.  
`getLength(callback, onError)`

Sets the current playback position in milliseconds.  
`setPosition(position, onSuccess, onError)`

Retrieves the current playback position in milliseconds.  
`getPosition(callback, onError)`

Sets the mute state.  
*(boolean) muted, specifies whether to sound is muted or not.*  
`setMute(muted, onSuccess, onError)`

Retrieves the mute state.  
`getMute(callback, onError)`

Sets the left/right pan level.  
*(float) pan, the pan level where -1 represents full left, 0 represents center and 1 represents full right.*  
`setPan(pan, onSuccess, onError)`

Register the player event callback.  
`registerEventCallback(callback, onError)`

Unregister the player event callback.  
`unregisterEventCallback(onSuccess, onError)`


### The FMODMetronome object

Closes the metronome.  
`close(onSuccess, onError)`

Starts the metronome.  
`start(onSuccess, onError)`

Stops the metronome.  
`stop(onSuccess, onError)`

Reset the metronome.  
`reset(onSuccess, onError)`

Register the metronome event callback.  
*(Function) callback, the callback that handles metronome events.*  
`registerEventCallback(callback, onError)`  
   Callback argument:
   - (int) step, The current DSP clock step.

Unregister the metronome event callback. 
`unregisterEventCallback(onSuccess, onError)`

Sets the beats frequency in BPM (0 means disable).  
*(int) bpm, the metronome frequency in beats per minute.*  
`setBeatsPerMinute(bpm, onSuccess, onError)`

Sets the beat period in samples (0 means disable).  
*(int) samples, the metronome period in samples.*  
`setBeatPeriodInSamples(samples, onSuccess, onError)`

Sets the beat period in milliseconds (0 means disable).  
*(float) ms, the metronome period in milliseconds.*  
`setBeatPeriodInMilliseconds(ms, onSuccess, onError)`

Sets the DSP clock to the given number of samples.  
*(int) samples, the metronome clock in samples.*  
`setDSPClock(samples, onSuccess, onError)`

Sets the DSP clock to the specified milliseconds.  
*(float) ms, the metronome clock in milliseconds.*  
`setDSPClockInMilliseconds(ms, onSuccess, onError)`

Retrieves information about the internal DSP clock and DSP cycle usage.  
`getDSPUsage(callback, onError)`
- (int) samples, DSP clock in samples.
- (float) millisecondes, DSP clock in milliseconds.
- (float) latency, the latest execution time between two DSP cycles in milliseconds.
- (float) expectedLatency, the expected time between two DSP cycles in milliseconds.


### The FMODScheduler object

Closes and frees the scheduler.  
`close(onSuccess, onError)`

Starts the scheduler.  
`start(onSuccess, onError)`

Stops the scheduler.  
`stop(onSuccess, onError)`

Pauses the scheduler.  
`pause(onSuccess, onError)`

Resumes the scheduler.  
`resume(onSuccess, onError)`

Reset the scheduler clock.  
`reset(onSuccess, onError)`

Schedule a new playback/callback event.  
*(Object) event, the event definition:*
- (int) type, the event type: EVENT_TYPE_PLAYBACK (default) or EVENT_TYPE_CALLBACK.
- (String) tags, one or more tags to define this event. Each tag must be separated by a comma.
- (int) samples or (float) time, the DSP clock at which the action should be performed (in samples or millisecondes).
- (boolean) once, whether the action should be executed only once or repeated (default) in each cycle of the scheduler.
- (boolean) now, whether or not the event must be recorded from the current DSP clock, or time 0 (default).

*EVENT_TYPE_PLAYBACK specific keys:*
- (String) player, the target player identifier.
- (int) action, the action to be performed: EVENT_START, EVENT_PAUSE, EVENT_RESUME or EVENT_STOP.

`registerEvent(event, onSuccess, onError)`

Delete one or more playback event (all events matching the given tag).  
*(String) tag, the playback event to be removed.*  
`unregisterEvent(tag, onSuccess, onError)`

Deletes all recorded playback events.  
`unregisterAllEvents(onSuccess, onError)`

Register the metronome event callback.  
*(Function) callback, the callback that handles metronome events.*  
`registerMetroEventCallback(callback, onError)`  
   Callback argument:
   - (int) step, The current DSP clock step.

Unregister the metronome event callback. 
`unregisterMetroEventCallback(onSuccess, onError)`

Sets the metronome beats frequency in BPM (0 means disable).  
*(int) bpm, the metronome frequency in beats per minute.*  
`setMetronomeFrequency(bpm, onSuccess, onError)`

Sets the metronome beat period in samples (0 means disable).  
*(int) samples, the metronome period in samples.*  
`setMetronomePeriodInSamples(samples, onSuccess, onError)`

Sets the metronome beat period in milliseconds (0 means disable).  
*(float) ms, the metronome period in milliseconds.*  
`setMetronomePeriodInMilliseconds(ms, onSuccess, onError)`

Sets the DSP cycle period to the given number of samples (0 means no cycle).  
*(int) samples, the DSP clock in samples.*  
`setPeriod(samples, onSuccess, onError)`

Sets the DSP cycle period to the specified milliseconds (0 means no cycle).  
*(float) ms, the DSP clock in milliseconds.*  
`setPeriodInMilliseconds(ms, onSuccess, onError)`

Sets the DSP clock to the given number of samples.  
*(int) samples, the DSP clock in samples.*  
`setClock(samples, onSuccess, onError)`

Sets the DSP clock to the specified milliseconds.  
*(float) ms, the DSP clock in milliseconds.*  
`setClockInMilliseconds(ms, onSuccess, onError)`

Retrieves information about the internal DSP clock and DSP cycle usage.  
`getDSPUsage(callback, onError)`
- (int) samples, DSP clock in samples.
- (float) millisecondes, DSP clock in milliseconds.
- (float) latency, the latest execution time between two DSP cycles in milliseconds.
- (float) expectedLatency, the expected time between two DSP cycles in milliseconds.


### The FMODInput object

Closes and frees an audio input and its resources.  
`close(onSuccess, onError)`

Starts listening to the audio input.  
`start(onSuccess, onError)`

Stops listening to the audio input.  
`stop(onSuccess, onError)`

Sets the number of channels of the audio input.  
*(int) channels, the number of input channels: 1 for mono or 2 for stereo.*  
`setInputChannels(channels, onSuccess, onError)`

Sets the audio format of the audio input.  
*(int) format, The audio format or bit depth for the audio input signal: 8, 16, 24 or 32 bit.*  
`setAudioFormat(channels, onSuccess, onError)`


### The FMODRecorder object

Closes and frees the recorder and its resources.  
`close(onSuccess, onError)`

Starts the recording.  
*(string) filepath, the path of the file where the audio data will be saved. To be easily loaded by 
your browser, the given file path must be relative to your root directory or any accessible directories. 
If the file path is not absolute, the final path will be relative to your app temporary (IOS) or external 
cache (Android) directory. You could use the Cordava File plugin to create the appriopriate path or 
retrieve the file content.*  
*Any folders specified in path will be created if not exists.*  
*Any created files will be deleted on app terminate.*  
`start(filepath, onSuccess, onError)`

Stops the recording.  
`stop(onSuccess, onError)`

Sets the audio signal to be recorded.  
*(FMODAudioUnit) audio, the audio input*  
`setInput(audio, onSuccess, onError)`

Sets the sample rate for the audio file.  
*(int) samplerate, the sample rate for the audio file: : 8000, 11025, 16000, 22050, 32000, 44100, 
48000, 88200, 96000, 176400 or 192000 Hz*  
`setSampleRate(samplerate, onSuccess, onError)`

Sets the audio format for the audio file.  
*(int) format, the audio format or bit depth: 8, 16, 24 or 32 bit.*  
`setAudioFormat(format, onSuccess, onError)`

Allows you to start the effective recording only when the sound is no longer considered silent,
i.e. from a minimum sound level.  
*(boolean) enable, whether the leading silence should be considered or not.*  
*(int) level, the minimum audio level (in dB) from where the effective recording should start.*  
`ignoreLeadingSilence(enable, level, onSuccess, onError)`

Register the recorder ended callback.  
*(Function) success, the function to be invoked to handle recording end event.*  
*(Function) error, the function to be invoked to handle recording error event.*  
`registerEndedCallback(success, error)`  
   Callback argument:
   - (string) the recorded audio file path on success or the error message on error.

Unregister the recorder ended callback.  
`unregisterEndedCallback(onSuccess, onError)`


### The FMODAnalyser object

Closes and frees an audio analyser and its resources.  
`close(onSuccess, onError)`

Starts audio analysis for the specified options.  
 *(Array) options, the list of audio analyses requested:*  
- 'metering', retrieve the signal metering information (peak and RMS level per channel)  
- 'fft', the FFT (Fast Fourier Transform) option analyzes the signal and provides information about its frequency spectrum (currently, only the spectral centroid).

`start(options, onSuccess, onError)`

Stops listening to the audio input.  
`stop(onSuccess, onError)`

Sets the audio signal to be analysed.  
*(FMODAudioUnit) audio, the audio input*  
`setInput(audio, onSuccess, onError)`

Sets the interval between two analyses in milliseconds (0 means each DSP cycle).  
*(float) ms, the period in milliseconds (default 50ms).*  
`setPeriodInMilliseconds(ms, onSuccess, onError)`

Sets the audio analysis callback.  
*(Function) callback, the function to be invoked to handle the audio analysis data.*  
`setCallback(callback, onError)`  
   Callback argument:
   - (Object) the audio analysis data.  
   Depending on the analysis options requested, the data may contain the following keys:
     - (Object) fft, an object providing information about the signal frequency spectrum.
     - (Object) metering, an object containing the signal metering information.

Removes the audio analysis callback.  
`unsetCallback(onSuccess, onError)`



## Example

FMOD object example.
```js
console.log(`FMOD plugin version: ${FMOD.PLUGIN_VERSION}`);

var fmod = new FMOD();
fmod.version(function (v) {
    console.log('FMOD library version', v);
});
```

FMOD player example.
```js
// configuration
var options = {
    source: "media/butterfly.mp3",
    loop: FMODPlayer.LOOP_NONE,
    stream: false,
    autoplay: true
};

// channel event handler
var eventCallback = function (event) {
    console.log('FMOD sound file event:', event);
};

// load and play soundfile
var player = new FMODPlayer();
player.registerEventCallback(eventCallback);
player.load(
    options,
    function () {
        // todo on load...
    },
    function (error) { console.error(error); }
);
```

FMOD metronome example.
```js
var metronome = new FMODMetronome();
metronome.registerEventCallback(
    function (index) {
        // todo something with index...
    },
    function (error) { console.error(error); }
);
metronome.setBeatsPerMinute(60);
metronome.start();
```

FMOD scheduler example.
```js
var p1 = new FMODPlayer("singing");
var p2 = new FMODPlayer("dog");
var scheduler = new FMODScheduler();

// the scheduler event handler
var schedulerEventCallback = function (type, tag) {
    console.log('Scheduler event type:', type, 'tag:', tag);
};

// create players
p1.load({ source: "media/singing.wav" });
p2.load({ source: "media/dog.wav" });

// start with the voice after 3 seconds delay
scheduler.registerEvent({
    tags: "singing",
    player: "singing",
    action: FMODScheduler.EVENT_START,
    samples: 3 * 48000 // using samples (based on 48000Hz)
});
// play dog one seconds later
scheduler.registerEvent({
    tags: "dog",
    player: "dog",
    action: FMODScheduler.EVENT_START,
    time: 4000, // using milliseconds
    once: true, // play once, the first scheduler cycle
    type: FMODScheduler.EVENT_TYPE_PLAYBACK // can be ommited, default value
});
// add a callback events for each even seconds
for (var i = 0; i < 5; i+=2) {
    scheduler.registerEvent({
        type: FMODScheduler.EVENT_TYPE_CALLBACK,
        tags: "event " + i,
        time: i * 1000
    });
}
// set the sequencer loop to 5 seconds
scheduler.setPeriodInMilliseconds(5000);
scheduler.setEventCallback(schedulerEventCallback);
scheduler.start();
```

FMOD input / output example.
```js
var audioin = new FMODInput();
var recorder = new FMODRecorder();
var monitoring = new FMODPlayer();

// set callback: play recorded sound on stop
recorder.registerEndedCallback(
    function (file) {
        console.log('recorded file', file);
        monitoring.load({ autoplay: true, source: file });
    },
    function (error) { console.error(error); }
)

// link object
recorder.setInput(audioin);

// open your mic and start recording
audioin.start(
    function() {
        recorder.start(
            "record.wav",
            function () { console.log('Start recording') }
        );
    }
);

// wait few minutes then stop recording
setTimeout(
    function () {
        recorder.stop(
            function () { console.log('Stop recording') }
        );
    },
    7000
);
```

Shared clock example.
```js
var metronome = new FMODMetronome("main clock");
metronome.setBeatsPerMinute(60);

for(var i=1; i<=2; i++) {
    var sequencer = new FMODScheduler();
    sequencer.setMetronome("main clock");
    sequencer.registerMetroEventCallback(
        function (step) {
            // todo something with step...
            console.log("Sequencer step:", step);
        },
        function (error) { console.error(error); }
    );
    sequencer.setPeriodInMilliseconds(i*4000);
    sequencer.start();
}
```

FMOD analyser example.
```js
var audioin = new FMODInput();
var analyser = new FMODAnalyser();

// set callback: play recorded sound on stop
analyser.setCallback(
    function (data) {
        console.log('audio analysis data', data);
    },
    function (error) { console.error(error); }
)

// link object
analyser.setInput(audioin);

// open your mic and start recording
audioin.start(
    function() {
        analyser.start(
            ["fft", "metering"],
            function () { console.log('Start audio analysis') }
        );
    }
);
```



## Requirements

iOS 9.0 and later.  
Android 8.1.0 (API level 27) and later.
