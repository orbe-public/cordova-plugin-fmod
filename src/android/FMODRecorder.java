// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FMODRecorder extends CordovaPlugin
{
    // list of each recorder callbacks
    private Map<String, CallbackContext> callbacks = new HashMap<>();
    // the user file paths stored by recorder id
    private Map<String, String> audiofiles = new HashMap<>();
    // list of recorded audio files
    ArrayList<String> files = new ArrayList<String>();

    // the temporary storage directory
    File tempdir = null;


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);

        tempdir = cordova.getContext().getExternalCacheDir();
        initNative();

        Log.d("FMODRecorder", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();

        // cleanup temporary files
        for (int i=0; i<files.size(); i++) {
            File f = new File(files.get(i));
            if (f.exists()) {
                Log.d("FMODRecorder", "Delete recorded file "+f.getPath());
                f.delete();
            }
        }

        Log.d("FMODRecorder", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "close":
                this.close(uuid, context);
                return true;
            case "start" :
                this.start(uuid, json, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "setInput" :
                this.setInput(uuid, json, context);
                return true;
            case "setSampleRate" :
                this.setSampleRate(uuid, json, context);
                return true;
            case "setAudioFormat" :
                this.setAudioFormat(uuid, json, context);
                return true;
            case "ignoreLeadingSilence" :
                this.ignoreLeadingSilence(uuid, json, context);
                return true;
            case "registerEndedCallback":
                this.registerEndedCallback(uuid, context);
                return true;
            case "unregisterEndedCallback":
                this.unregisterEndedCallback(uuid, context);
                return true;
        }

        return false;
    }


    /**
     * Parse and send JNI result.
     *
     * @param uuid The native object identifier.
     * @param result The native command status.
     * @param context The context to be fired.
     */
    private void parseResult(String uuid, int result, CallbackContext context)
    {
        if (result==FMOD.JNI_RESULT_ERROR) {
            String message = getLastErrorMessage(uuid);
            FMOD.sendResult(context, result, message);
        }
        else FMOD.sendResult(context, result);
    }



    private void init(String uuid, CallbackContext context)
    {
        create(uuid);
        context.success();
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_NOTFOUND:
                FMOD.sendNotFound(context);
                break;
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    // -- actions ----------------------------------------------------------------------------------

    private void start(String uuid, JSONObject options, CallbackContext context)
    {
        String filepath = options.optString("filepath");
        String prefix   = (filepath.startsWith("/") || filepath.startsWith("file://")) ? "" : tempdir.getPath() + "/";
        File recpath    = new File(prefix, filepath.replaceFirst("^file://", ""));
        File targetdir  = new File(recpath.getParent());

        if(!targetdir.exists() && !targetdir.mkdirs()) {
            Log.w("FMODRecorder", "Failed to create directory: "+targetdir.getPath());
            FMOD.sendResult(context, FMOD.JNI_RESULT_ERROR, "Permission denied to save file to the target directory");
            return;
        }

        int result = start(uuid, recpath.getPath());
        if (result==FMOD.JNI_RESULT_OK) audiofiles.put(uuid, prefix+filepath);

        parseResult(uuid, result, context);
    }

    private void stop(String uuid, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                int result = stop(uuid);
                parseResult(uuid, result, context);
            }
        });
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setInput(String uuid, JSONObject options, CallbackContext context)
    {
        JSONObject unit = options.optJSONObject("audio");
        String type = unit.optString("type");
        String id = unit.optString("uuid");
        int result = setInput(uuid, type, id);

        parseResult(uuid, result, context);
    }

    private void setSampleRate(String uuid, JSONObject options, CallbackContext context)
    {
        int samplerate = options.optInt("samplerate");
        int result = setSampleRate(uuid, samplerate);

        parseResult(uuid, result, context);
    }

    private void setAudioFormat(String uuid, JSONObject options, CallbackContext context)
    {
        int format = options.optInt("format");
        int result = setAudioFormat(uuid, format);

        parseResult(uuid, result, context);
    }

    private void ignoreLeadingSilence(String uuid, JSONObject options, CallbackContext context)
    {
        boolean on = options.optBoolean("enable");
        int level  = options.optInt("level");
        int result = ignoreLeadingSilence(uuid, on, level);

        parseResult(uuid, result, context);
    }

    // -- events -----------------------------------------------------------------------------------

    private void registerEndedCallback(String uuid, CallbackContext context)
    {
        callbacks.put(uuid, context);
    }

    private void unregisterEndedCallback(String uuid, CallbackContext context)
    {
        deleteCallbackForUUID(uuid);
        context.success();
    }

    private void deleteCallbackForUUID(String uuid)
    {
        callbacks.remove(uuid);
    }



    // --- JNI callback ----------------------------------------------------------------------------
    private void onRecordEnd(String uuid, String path, boolean error)
    {
        CallbackContext context = callbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            String message = error ? "unable to save audio file" : audiofiles.get(uuid);
            PluginResult.Status status = error ? PluginResult.Status.ERROR: PluginResult.Status.OK;

            // store created file
            // and remove the referenced file for the target recorder
            if (!error) {
                files.add(path);
                audiofiles.remove(uuid);
            }
            else Log.e("FMODRecorder", message);

            PluginResult pluginResult = new PluginResult(status, message);
            pluginResult.setKeepCallback(true);
            context.sendPluginResult(pluginResult);
        }
    }



    // --- native methods --------------------------------------------------------------------------

    private native void initNative();
    private native void closeNative();

    private native int create(String uuid);
    private native int close(String uuid);
    private native int start(String uuid, String file);
    private native int stop(String uuid);

    private native int setInput(String uuid, String type, String id);
    private native int setSampleRate(String uuid, int samplerate);
    private native int setAudioFormat(String uuid, int format);
    private native int ignoreLeadingSilence(String uuid, boolean enable, int level);

    private native String getLastErrorMessage(String uuid);
}