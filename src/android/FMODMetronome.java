// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
* Allow to manage a DSP clock.
*/
public class FMODMetronome extends CordovaPlugin
{
    // list of each metronome callbacks
    private static Map<String, CallbackContext> callbacks = new HashMap<>();


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);
        initNative();
        Log.w("FMODMetronome", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();
        Log.d("FMODMetronome", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "close" :
                this.close(uuid, context);
                return true;
            case "start" :
                this.start(uuid, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "reset" :
                this.reset(uuid, context);
                return true;
            case "registerEventCallback" :
                this.registerEventCallback(uuid, context);
                return true;
            case "unregisterEventCallback" :
                this.unregisterEventCallback(uuid, context);
                return true;
            case "setBeatsPerMinute" :
                this.setBeatsPerMinute(uuid, json, context);
                return true;
            case "setBeatPeriodInSamples" :
                this.setBeatPeriodInSamples(uuid, json, context);
                return true;
            case "setBeatPeriodInMilliseconds" :
                this.setBeatPeriodInMilliseconds(uuid, json, context);
                return true;
            case "setDSPClock" :
                this.setDSPClock(uuid, json, context);
                return true;
            case "setDSPClockInMilliseconds" :
                this.setDSPClockInMilliseconds(uuid, json, context);
                return true;
            case "getDSPUsage":
                this.getDSPUsage(uuid, context);
                return true;
        }

        return false;
    }



    private void init(String uuid, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                create(uuid);
                context.success();
            }
        });
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        FMOD.sendResult(context, result);
    }

    // -- actions ----------------------------------------------------------------------------------

    private void start(String uuid, CallbackContext context)
    {
        int result = start(uuid);
        FMOD.sendResult(context, result, "Already started");
    }

    private void stop(String uuid, CallbackContext context)
    {
        int result = stop(uuid);
        FMOD.sendResult(context, result);
    }

    private void reset(String uuid, CallbackContext context)
    {
        int result = reset(uuid);
        FMOD.sendResult(context, result);
    }

    // -- events ----------------------------------------------------------------------------------

    protected static void registerEventCallback(String uuid, CallbackContext context)
    {
        callbacks.put(uuid, context);
    }

    protected static void unregisterEventCallback(String uuid, CallbackContext context)
    {
        callbacks.remove(uuid);
        context.success();
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setBeatsPerMinute(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            int bpm = options.getInt("bpm");
            int result = setBeatsPerMinute(uuid, bpm);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid BPM value");
        }
    }

    private void setBeatPeriodInSamples(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            long samples = options.getLong("samples");
            int result = setBeatPeriodInSamples(uuid, samples);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid samples value");
        }
    }

    private void setBeatPeriodInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setBeatPeriodInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    private void setDSPClock(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            long samples = options.getLong("samples");
            int result = setDSPClock(uuid, samples);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid samples value");
        }
    }

    private void setDSPClockInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setDSPClockInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    // -- getters ----------------------------------------------------------------------------------

    private void getDSPUsage(String uuid, CallbackContext context)
    {
        JSONObject out = new JSONObject();

        try {
            out.put("samples", getDSPClock(uuid));
            out.put("millisecondes", getDSPClockInMilliseconds(uuid));
            out.put("latency", getDSPCycleLatency(uuid));
            out.put("expectedLatency", getDSPCycleExpectedLatency(uuid));
            context.success(out);
        }
        catch (JSONException e) {
            context.error(e.getMessage());
        }
    }



    // --- JNI callback ----------------------------------------------------------------------------
    private void onMetroEvent(String uuid, long step)
    {
        CallbackContext context = callbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, (int)step);
            pluginResult.setKeepCallback(true);
            context.sendPluginResult(pluginResult);
        }
    }



    // --- native methods --------------------------------------------------------------------------

    private native void initNative();
    private native void closeNative();

    private native void create(String uuid);
    private native int close(String uuid);
    private native int start(String uuid);
    private native int stop(String uuid);
    private native int reset(String uuid);

    private native int setBeatsPerMinute(String uuid, int bpm);
    private native int setBeatPeriodInSamples(String uuid, long samples);
    private native int setBeatPeriodInMilliseconds(String uuid, float ms);
    private native int setDSPClock(String uuid, long samples);
    private native int setDSPClockInMilliseconds(String uuid, float ms);

    private native long getDSPClock(String uuid);
    private native float getDSPClockInMilliseconds(String uuid);
    private native float getDSPCycleLatency(String uuid);
    private native float getDSPCycleExpectedLatency(String uuid);
}