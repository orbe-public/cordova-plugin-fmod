// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
* A complete sound file player with all the main playback features.
*/
public class FMODPlayer extends CordovaPlugin
{
    private Map<String, CallbackContext> callbacks = new HashMap<>();


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);
        initNative();

        Log.d("FMODPlayer", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();
        Log.d("FMODPlayer", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "load" :
                this.load(uuid, json, context);
                return true;
            case "release" :
                this.release(uuid, context);
                return true;
            case "close" :
                this.close(uuid, context);
                return true;
            case "play" :
                this.play(uuid, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "pause" :
                this.pause(uuid, json, context);
                return true;
            case "isPaused" :
                this.isPaused(uuid, context);
                return true;
            case "isPlaying" :
                this.isPlaying(uuid, context);
                return true;
            case "setLoopMode" :
                this.setLoopMode(uuid, json, context);
                return true;
            case "setVolume" :
                this.setVolume(uuid, json, context);
                return true;
            case "setFadeIn" :
                this.setFadeIn(uuid, json, context);
                return true;
            case "setFadeOut" :
                this.setFadeOut(uuid, json, context);
                return true;
            case "setPitch" :
                this.setPitch(uuid, json, context);
                return true;
            case "setPosition" :
                this.setPosition(uuid, json, context);
                return true;
            case "getPosition" :
                this.getPosition(uuid, context);
                return true;
            case "setMute" :
                this.setMute(uuid, json, context);
                return true;
            case "setPan" :
                this.setPan(uuid, json, context);
                return true;
            case "getMute" :
                this.getMute(uuid, context);
                return true;
            case "getLength" :
                this.getLength(uuid, context);
                return true;
            case "registerEventCallback":
                this.registerEventCallback(uuid, context);
                return true;
            case "unregisterEventCallback":
                this.unregisterEventCallback(uuid, context);
                return true;
        }

        return false;
    }


    /**
     * Parse and send JNI result.
     *
     * @param uuid The native object identifier.
     * @param result The native command status.
     * @param context The context to be fired.
     */
    private void parseResult(String uuid, int result, CallbackContext context)
    {
        if (result==FMOD.JNI_RESULT_ERROR) {
            String message = getLastErrorMessage(uuid);
            FMOD.sendResult(context, result, message);
        }
        else FMOD.sendResult(context, result);
    }


    private void init(String uuid, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                create(uuid);
                context.success();
            }
        });
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_NOTFOUND:
                FMOD.sendNotFound(context);
                break;
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                deleteCallbackForUUID(uuid);
                context.success();
                break;
        }
    }

    // -- actions ----------------------------------------------------------------------------------

    private void load(String uuid, JSONObject options, CallbackContext context)
    {
        String source = options.optString("source");
        String dls = options.optString("dls");
        int mode = options.optInt("loop");
        boolean stream = options.optBoolean("stream");
        boolean autoplay = options.optBoolean("autoplay");

        // load from buffer
        if (options.has("buffer")) {
            try {
                JSONArray buffer = options.getJSONArray("buffer");
                char[] data = new char[buffer.length()];
                for (int i = 0; i < buffer.length(); i++) data[i] = (char)buffer.getInt(i);

                cordova.getThreadPool().execute(new Runnable() {
                    public void run() {
                        int result = loadFromBuffer(uuid, data, mode, stream, autoplay, dls);
                        parseResult(uuid, result, context);
                    }
                });
            }
            catch (JSONException e) {
                context.error("Invalid buffer");
            }
        }
        // or from file
        else {
            if (source.isEmpty()) {
                context.error("Invalid source");
                return;
            }

            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    int result = load(uuid, source, mode, stream, autoplay, dls);
                    parseResult(uuid, result, context);
                }
            });
        }
    }

    private void release(String uuid, CallbackContext context)
    {
        int result = release(uuid);
        parseResult(uuid, result, context);
    }

    private void play(String uuid, CallbackContext context)
    {
        int result = play(uuid);
        parseResult(uuid, result, context);
    }

    private void stop(String uuid, CallbackContext context)
    {
        int result = stop(uuid);
        parseResult(uuid, result, context);
    }

    private void pause(String uuid, JSONObject options, CallbackContext context)
    {
        boolean paused = options.optBoolean("paused");
        int result = pause(uuid, paused);

        parseResult(uuid, result, context);
    }

    // --- playback status -------------------------------------------------------------------------

    private void isPlaying(String uuid, CallbackContext context)
    {
        int state = isPlaying(uuid);
        FMOD.sendValue(state, context);
    }

    private void isPaused(String uuid, CallbackContext context)
    {
        int state = isPaused(uuid);
        FMOD.sendValue(state, context);
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setLoopMode(String uuid, JSONObject options, CallbackContext context)
    {
        int mode = options.optInt("mode");
        int result = setMode(uuid, mode);

        parseResult(uuid, result, context);
    }

    private void setVolume(String uuid, JSONObject options, CallbackContext context)
    {
        float value = (float) options.optDouble("value");
        int result = setVolume(uuid, value);

        parseResult(uuid, result, context);
    }

    private void setFadeIn(String uuid, JSONObject options, CallbackContext context)
    {
        long ms = options.optLong("ms");
        int result = setFadeIn(uuid, ms);

        parseResult(uuid, result, context);
    }

    private void setFadeOut(String uuid, JSONObject options, CallbackContext context)
    {
        long ms = options.optLong("ms");
        boolean now = options.optBoolean("now");
        int result = setFadeOut(uuid, ms, now);

        parseResult(uuid, result, context);
    }

    private void setPitch(String uuid, JSONObject options, CallbackContext context)
    {
        float value = (float) options.optDouble("rate");
        boolean shift = options.optBoolean("shift");
        int result = setPitch(uuid, value, shift);

        parseResult(uuid, result, context);
    }

    private void setPosition(String uuid, JSONObject options, CallbackContext context)
    {
        long ms = options.optLong("position");
        int result = setPosition(uuid, ms);

        parseResult(uuid, result, context);
    }

    private void setMute(String uuid, JSONObject options, CallbackContext context)
    {
        boolean mute = options.optBoolean("muted");
        int result = setMute(uuid, mute);

        parseResult(uuid, result, context);
    }

    private void setPan(String uuid, JSONObject options, CallbackContext context)
    {
        float pan = (float) options.optDouble("pan");
        int result = setPan(uuid, pan);

        parseResult(uuid, result, context);
    }

    // -- getters ----------------------------------------------------------------------------------

    private void getPosition(String uuid, CallbackContext context)
    {
        long ms = getPosition(uuid);
        FMOD.sendValue(ms, context);
    }

    private void getMute(String uuid, CallbackContext context)
    {
        int muted = getMute(uuid);
        FMOD.sendValue(muted, context);
    }

    private void getLength(String uuid, CallbackContext context)
    {
        long ms = getLength(uuid);
        FMOD.sendValue(ms, context);
    }

    // -- events -----------------------------------------------------------------------------------

    private void registerEventCallback(String uuid, CallbackContext context)
    {
        callbacks.put(uuid, context);
    }

    private void unregisterEventCallback(String uuid, CallbackContext context)
    {
        deleteCallbackForUUID(uuid);
        context.success();
    }

    private void deleteCallbackForUUID(String uuid)
    {
        callbacks.remove(uuid);
    }



    // --- JNI callback ----------------------------------------------------------------------------
    private void onChannelEvent(String uuid, String message)
    {
        CallbackContext context = callbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, message);
            pluginResult.setKeepCallback(true);
            context.sendPluginResult(pluginResult);
        }
    }



    // --- native methods --------------------------------------------------------------------------

    private native void initNative();
    private native void closeNative();

    private native void create(String uuid);
    private native int load(String uuid, String source, int mode, boolean stream, boolean autoplay, String dls);
    private native int loadFromBuffer(String uuid, char[] buffer, int mode, boolean stream, boolean autoplay, String dls);
    private native int release(String uuid);
    private native int close(String uuid);
    private native int play(String uuid);
    private native int stop(String uuid);
    private native int pause(String uuid, boolean paused);
    private native int isPaused(String uuid);
    private native int isPlaying(String uuid);

    private native int setVolume(String uuid, float volume);
    private native int setFadeIn(String uuid, long ms);
    private native int setFadeOut(String uuid, long ms, boolean now);
    private native int setMode(String uuid, int mode);
    private native int setPosition(String uuid, long ms);
    private native int setPitch(String uuid, float rate, boolean shift);
    private native int setMute(String uuid, boolean muted);
    private native int setPan(String uuid, float pan);

    private native long getLength(String uuid);
    private native long getPosition(String uuid);
    private native int getMute(String uuid);

    private native String getLastErrorMessage(String uuid);
}