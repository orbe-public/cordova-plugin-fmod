// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provides various information about the resources created and played.
 */
public class FMOD extends CordovaPlugin implements Runnable
{
    final static int JNI_RESULT_NOTFOUND = -1;
    final static int JNI_RESULT_ERROR = 0;
    final static int JNI_RESULT_OK = 1;

    private Thread thread;
    private boolean isRunning;


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);

        isRunning = false;

        Context context = cordova.getContext();
        initAudioSession(context);

        Log.d("FMOD", "initialized");
    }

    @Override
    public void onDestroy()
    {
        try
        {
            isRunning = false;
            thread.join();

            Log.d("FMOD", "destroyed");
        }
        catch (InterruptedException e) { }
    }

    @Override
    public void run()
    {
        // init plugin context
        init();
        Log.d("FMOD", "context initialized");

        // update periodically
        isRunning = true;
        long ms = 50;
        while( isRunning ) {
            try { Thread.sleep(ms); }
            catch( InterruptedException e ) {}

            update();
        }

        // close plugin context
        close();
        // close FMOD context
        org.fmod.FMOD.close();
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        switch (action) {
            case "getVersion":
                this.getVersion(context);
                return true;
            case "getCPUUsage" :
                this.getCPUUsage(context);
                return true;
            case "getChannelsPlaying" :
                this.getChannelsPlaying(context);
                return true;
            case "getEllapsedTime" :
                this.getEllapsedTime(context);
                return true;
        }
        return false;
    }



    /**
     * Initialize the audio context
     * @param context The Android context.
     */
    private void initAudioSession(Context context)
    {
        int samplerate  = 48000;
        int buffer      = 512;
        int outchannels = 2;

        AudioManager session = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        // query the actual supported rate and max channels
        if (Build.VERSION.SDK_INT>=17) {
            String prate = session.getProperty(android.media.AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            if ( prate!=null ) samplerate = Integer.parseInt(prate);
            String pchan = session.getProperty(android.media.AudioManager.EXTRA_MAX_CHANNEL_COUNT);
            if ( pchan!=null ) outchannels = Integer.parseInt(pchan);
        }
        Log.i( "[AUDIO SESSION]", "sample rate: "+samplerate+", number of output channels: "+outchannels );

        // request necessary permissions
        if (!cordova.hasPermission(Manifest.permission.RECORD_AUDIO)) {
            Log.d( "[AUDIO SESSION]", "ask for audio record permission");
            cordova.requestPermission(this, 0, Manifest.permission.RECORD_AUDIO);
        }
        if (!cordova.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.d( "[AUDIO SESSION]", "ask for write external storage permission");
            cordova.requestPermission(this, 0, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // init FMOD (fmod.jar)
        org.fmod.FMOD.init(context);

        // then run plugin context
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Retrieves the target class name from the given JS command.
     *
     * @param context The context to extract name
     * @return The formatted classname
     */
    public static String getClassName(CallbackContext context)
    {
        String name = context.getCallbackId().replaceAll("^FMOD([A-Za-z]+).*", "$1");
        return name.toLowerCase();
    }

    /**
     * Extract the native object identifier from the given JS command.
     *
     * @param args The JS command arguments.
     * @param context The context to be fired on error.
     * @return The native object identifier.
     * @throws JSONException Invalid arguments
     */
    protected static String getUUIDFromJS(JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = "";
        JSONObject json = new JSONObject();

        // inside JSON
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
            uuid = json.getString("uuid");
        }
        else uuid = args.optString(0);

        // ID shouldn't be empty
        if (uuid.isEmpty()) {
            String name = getClassName(context);
            String msg = String.format("Missing or invalid native %s identifier", name.toLowerCase());
            context.error(msg);
        }

        return uuid;
    }

    /**
     * Send the native command result.
     *
     * @param context The context to be fired.
     * @param result The native command status.
     */
    protected static void sendResult(CallbackContext context, int result)
    {
        sendResult(context, result, "unknown");
    }

    /**
     * Send the native command result.
     *
     * @param context The context to be fired.
     * @param result The native command status.
     * @param error The error message to be sent (if case)
     */
    protected static void sendResult(CallbackContext context, int result, String error)
    {
        switch(result) {
            case JNI_RESULT_NOTFOUND:
                sendNotFound(context);
                break;
            case JNI_RESULT_ERROR:
                context.error(error);
                break;
            case JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    /**
     * Send the native command value.
     *
     * @param value The value to be sent to the user.
     * @param context The context to be fired.
     */
    public static void sendValue(int value, CallbackContext context)
    {
        if (value==JNI_RESULT_NOTFOUND) sendNotFound(context);
        else context.success(value);
    }

    /**
     * Send the native command value.
     *
     * @param value The value to be sent to the user.
     * @param context The context to be fired.
     */
    public static void sendValue(long value, CallbackContext context)
    {
        if (value==JNI_RESULT_NOTFOUND) sendNotFound(context);
        else {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, value);
            context.sendPluginResult(pluginResult);
        }
    }

    /**
     * Notify the user that the native object is not found.
     *
     * @param context The context to be fired.
     */
    public static void sendNotFound(CallbackContext context)
    {
        String name = getClassName(context);
        String msg = String.format("Native %s not found", name.toLowerCase());
        context.error(msg);
    }



    // --- getters ---------------------------------------------------------------------------------

    private void getCPUUsage(CallbackContext context)
    {
        double[] usage = getCPUUsage();
        JSONObject out = new JSONObject();

        try {
            out.put("dsp", usage[0]);
            out.put("stream", usage[1]);
            out.put("geometry", usage[2]);
            out.put("update", usage[3]);
            out.put("convolution1", usage[4]);
            out.put("convolution2", usage[5]);
            context.success(out);
        }
        catch (JSONException e) {
            context.error(e.getMessage());
        }
    }

    private void getChannelsPlaying(CallbackContext context)
    {
        int channels = getChannelsPlaying();
        context.success(channels);
    }

    private void getEllapsedTime(CallbackContext context)
    {
        long ms = getEllapsedTime();
        context.success((int)ms);
    }

    private void getVersion(CallbackContext context)
    {
        int version = getVersion();
        int product = (version >> 16) & 0xFFFF;
        int major = (version >> 8) & 0xFF;
        int minor = version & 0xFF;

        JSONObject out = new JSONObject();

        try {
            out.put("number", String.format("%x.%02x.%02x", product, major, minor));
            out.put("code", Integer.parseInt(String.format("%x", version)));
            out.put("product", Integer.parseInt(String.format("%x", product)));
            out.put("major", Integer.parseInt(String.format("%x", major)));
            out.put("minor", Integer.parseInt(String.format("%x", minor)));
            context.success(out);
        }
        catch (JSONException e) {
            context.error(e.getMessage());
        }
    }



    // --- native methods --------------------------------------------------------------------------

    private native int getVersion();
    private native double[] getCPUUsage();
    private native int getChannelsPlaying();
    private native long getEllapsedTime();

    private native void init();
    private native void update();
    private native void close();

    static {
        try {
            System.loadLibrary("fmod");
            System.loadLibrary("orbeaudio");
        }
        catch (UnsatisfiedLinkError e) {
            Log.e("FMOD", "Native libraries failed to load: " + e.getMessage());
        }
    }
}