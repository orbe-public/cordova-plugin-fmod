// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FMODAnalyser extends CordovaPlugin
{
    // list of each recorder callbacks
    private Map<String, CallbackContext> callbacks = new HashMap<>();


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);
        initNative();
        Log.d("FMODAnalyzer", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();
        Log.d("FMODAnalyzer", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "close":
                this.close(uuid, context);
                return true;
            case "start" :
                this.start(uuid, json, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "setInput" :
                this.setInput(uuid, json, context);
                return true;
            case "setPeriodInMilliseconds" :
                this.setPeriodInMilliseconds(uuid, json, context);
                return true;
            case "setCallback":
                this.setCallback(uuid, context);
                return true;
            case "unsetCallback":
                this.unsetCallback(uuid, context);
                return true;
        }

        return false;
    }


    /**
     * Parse and send JNI result.
     *
     * @param uuid The native object identifier.
     * @param result The native command status.
     * @param context The context to be fired.
     */
    private void parseResult(String uuid, int result, CallbackContext context)
    {
        if (result==FMOD.JNI_RESULT_ERROR) {
            String message = getLastErrorMessage(uuid);
            FMOD.sendResult(context, result, message);
        }
        else FMOD.sendResult(context, result);
    }


    private void init(String uuid, CallbackContext context)
    {
        int result = create(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_NOTFOUND:
                FMOD.sendNotFound(context);
                break;
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    // -- actions ----------------------------------------------------------------------------------

    private void start(String uuid, JSONObject args, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                try {
                    JSONArray options = args.getJSONArray("options");
                    String[] types = new String[options.length()];
                    for (int i = 0; i < options.length(); i++) types[i] = options.getString(i);

                    int result = start(uuid, types);
                    parseResult(uuid, result, context);
                }
                catch (JSONException e) {
                    String message = "The list of requested analyses cannot be found. No audio analysis will be performed.";
                    Log.w("FMODAnalyser", e.getMessage());
                    context.error(message);
                }
            }
        });
    }

    private void stop(String uuid, CallbackContext context)
    {
        int result = stop(uuid);
        parseResult(uuid, result, context);
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setInput(String uuid, JSONObject options, CallbackContext context)
    {
        JSONObject unit = options.optJSONObject("audio");
        String type = unit.optString("type");
        String id = unit.optString("uuid");
        int result = setInput(uuid, type, id);

        parseResult(uuid, result, context);
    }

    private void setPeriodInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setPeriodInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    // -- events -----------------------------------------------------------------------------------

    private void setCallback(String uuid, CallbackContext context)
    {
        callbacks.put(uuid, context);
    }

    private void unsetCallback(String uuid, CallbackContext context)
    {
        deleteCallbackForUUID(uuid);
        context.success();
    }

    private void deleteCallbackForUUID(String uuid)
    {
        callbacks.remove(uuid);
    }



    // --- JNI callback ----------------------------------------------------------------------------

    private void onAnalysisData(String uuid, double[] fft, double[] metering)
    {
        CallbackContext context = callbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            JSONObject jfft = new JSONObject();
            JSONObject jmetering = new JSONObject();
            JSONObject out = new JSONObject();
            try {
                jfft.put("centroid", fft[0]);
                jmetering.put("peak", new JSONArray(new Object[] { metering[0], metering[1] }));
                jmetering.put("rms", new JSONArray(new Object[] { metering[1], metering[2] }));

                out.put("fft", jfft);
                out.put("metering", jmetering);

                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, out);
                pluginResult.setKeepCallback(true);
                context.sendPluginResult(pluginResult);
            }
            catch (JSONException e) {
                Log.e("FMODAnalyser", e.getMessage());
            }
        }
    }


    // --- native methods --------------------------------------------------------------------------

    private native void initNative();
    private native void closeNative();

    private native int create(String uuid);
    private native int close(String uuid);
    private native int start(String uuid, String[] options);
    private native int stop(String uuid);

    private native int setInput(String uuid, String type, String id);
    private native int setPeriodInMilliseconds(String uuid, float ms);

    private native String getLastErrorMessage(String uuid);
}