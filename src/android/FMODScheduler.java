// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
* Allow to schedule playback events at a specific DSP clock, as well as run as an audio sequence.
*/
public class FMODScheduler extends CordovaPlugin
{
    // list of each scheduler event callbacks
    private static Map<String, CallbackContext> stepsCallbacks = new HashMap<>();
    private static Map<String, CallbackContext> eventCallbacks = new HashMap<>();

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);
        initNative();
        Log.w("FMODScheduler", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();
        Log.d("FMODScheduler", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "close" :
                this.close(uuid, context);
                return true;
            case "start" :
                this.start(uuid, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "pause" :
                this.pause(uuid, context);
                return true;
            case "resume" :
                this.resume(uuid, context);
                return true;
            case "reset" :
                this.reset(uuid, context);
                return true;
            case "registerEvent" :
                this.registerEvent(uuid, json, context);
                return true;
            case "unregisterEvent" :
                this.unregisterEvent(uuid, json, context);
                return true;
            case "unregisterAllEvents" :
                this.unregisterAllEvents(uuid, context);
                return true;
            case "setEventCallback" :
                this.setEventCallback(uuid, context);
                return true;
            case "unsetEventCallback" :
                this.unsetEventCallback(uuid, context);
                return true;
            case "setMetronome" :
                this.setMetronome(uuid, json, context);
                return true;
            case "registerMetroEventCallback" :
                this.registerMetroEventCallback(uuid, context);
                return true;
            case "unregisterMetroEventCallback" :
                this.unregisterMetroEventCallback(uuid, context);
                return true;
            case "setBeatsPerMinute" :
                this.setBeatsPerMinute(uuid, json, context);
                return true;
            case "setBeatPeriodInSamples" :
                this.setBeatPeriodInSamples(uuid, json, context);
                return true;
            case "setBeatPeriodInMilliseconds" :
                this.setBeatPeriodInMilliseconds(uuid, json, context);
                return true;
            case "setPeriod" :
                this.setPeriod(uuid, json, context);
                return true;
            case "setPeriodInMilliseconds" :
                this.setPeriodInMilliseconds(uuid, json, context);
                return true;
            case "setClock" :
                this.setClock(uuid, json, context);
                return true;
            case "setClockInMilliseconds" :
                this.setClockInMilliseconds(uuid, json, context);
                return true;
            case "getDSPUsage":
                this.getDSPUsage(uuid, context);
                return true;
        }

        return false;
    }



    private void init(String uuid, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                create(uuid);
                context.success();
            }
        });
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        FMOD.sendResult(context, result);
    }

    // -- actions ----------------------------------------------------------------------------------

    private void start(String uuid, CallbackContext context)
    {
        int result = start(uuid);
        FMOD.sendResult(context, result, "Already started");
    }

    private void stop(String uuid, CallbackContext context)
    {
        int result = stop(uuid);
        FMOD.sendResult(context, result);
    }

    private void pause(String uuid, CallbackContext context)
    {
        int result = pause(uuid, true);
        FMOD.sendResult(context, result);
    }

    private void resume(String uuid, CallbackContext context)
    {
        int result = pause(uuid, false);
        FMOD.sendResult(context, result);
    }

    private void reset(String uuid, CallbackContext context)
    {
        int result = reset(uuid);
        FMOD.sendResult(context, result);
    }

    // -- events ----------------------------------------------------------------------------------

    private void registerEvent(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            JSONObject event = options.getJSONObject("event");
            try {
                int type = event.optInt("type");
                String tags = event.optString("tags");
                String player = event.optString("player");
                int action = event.optInt("action");
                boolean once = event.optBoolean("once");

                int result = FMOD.JNI_RESULT_ERROR;
                if (event.has("samples")) {
                    long samples = event.getLong("samples");
                    result = registerEvent(uuid, type, tags, samples, action, player, once);
                } else {
                    float time = (float) event.getDouble("time");
                    result = registerEventInMilliseconds(uuid, type, tags, time, action, player, once);
                }

                // parse result
                switch (result) {
                    case -5: context.error("Invalid type"); break;
                    case -4: context.error("Invalid tag parameter"); break;
                    case -3: context.error("Target player not found" + player); break;
                    case -2: context.error("Invalid action"); break;
                    default: FMOD.sendResult(context, result);
                }
            }
            catch (JSONException e) {
                context.error("Missing samples or time parameter");
            }
        }
        catch (JSONException e) {
            context.error("Invalid event parameter");
        }
    }

    private void unregisterEvent(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            String tag = options.getString("tag");
            int result = unregisterEvent(uuid, tag);
            FMOD.sendResult(context, result, "Tag not found");
        }
        catch (JSONException e) {
            context.error("Invalid tag");
        }
    }

    private void unregisterAllEvents(String uuid, CallbackContext context)
    {
        int result = unregisterAllEvents(uuid);
        FMOD.sendResult(context, result);
    }

    private void setEventCallback(String uuid, CallbackContext context)
    {
        eventCallbacks.put(uuid, context);
    }

    private void unsetEventCallback(String uuid, CallbackContext context)
    {
        eventCallbacks.remove(uuid);
        context.success();
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setPeriod(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            long samples = options.getLong("samples");
            int result = setPeriod(uuid, samples);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid samples value");
        }
    }

    private void setPeriodInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setPeriodInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    private void setClock(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            long samples = options.getLong("samples");
            int result = setClock(uuid, samples);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid samples value");
        }
    }

    private void setClockInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setClockInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    // -- getters ----------------------------------------------------------------------------------

    private void getDSPUsage(String uuid, CallbackContext context)
    {
        JSONObject out = new JSONObject();

        try {
            out.put("samples", getDSPClock(uuid));
            out.put("millisecondes", getDSPClockInMilliseconds(uuid));
            out.put("latency", getDSPCycleLatency(uuid));
            out.put("expectedLatency", getDSPCycleExpectedLatency(uuid));
            context.success(out);
        }
        catch (JSONException e) {
            context.error(e.getMessage());
        }
    }

    // -- metronome -----------------------------------------------------------------------------------

    private void setMetronome(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            String metro = options.getString("metronome");
            int result = setMetronome(uuid, metro);
            if (result==-2) context.error("Target metronome not found");
            else FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid metronome identifier");
        }
    }

    private void setBeatsPerMinute(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            int bpm = options.getInt("bpm");
            int result = setBeatsPerMinute(uuid, bpm);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid BPM value");
        }
    }

    private void setBeatPeriodInSamples(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            long samples = options.getLong("samples");
            int result = setBeatPeriodInSamples(uuid, samples);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Invalid samples value");
        }
    }

    private void setBeatPeriodInMilliseconds(String uuid, JSONObject options, CallbackContext context)
    {
        try {
            float ms = (float) options.getDouble("ms");
            int result = setBeatPeriodInMilliseconds(uuid, ms);
            FMOD.sendResult(context, result);
        }
        catch (JSONException e) {
            context.error("Missing milliseconds value");
        }
    }

    private void registerMetroEventCallback(String uuid, CallbackContext context)
    {
        stepsCallbacks.put(uuid, context);
    }

    private void unregisterMetroEventCallback(String uuid, CallbackContext context)
    {
        stepsCallbacks.remove(uuid);
        context.success();
    }



    // --- JNI callback ----------------------------------------------------------------------------
    private void onSchedulerEvent(String uuid, int type, String tags)
    {
        CallbackContext context = eventCallbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            JSONObject out = new JSONObject();

            try {
                out.put("type", type);
                out.put("tags", tags);

                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, out);
                pluginResult.setKeepCallback(true);
                context.sendPluginResult(pluginResult);
            }
            catch (JSONException e) {
                context.error(e.getMessage());
            }
        }
    }

    private void onSchedulerStepEvent(String uuid, long step)
    {
        CallbackContext context = stepsCallbacks.get(uuid);
        // the event may not be registered
        if (context!=null) {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, (int)step);
            pluginResult.setKeepCallback(true);
            context.sendPluginResult(pluginResult);
        }
    }



    // --- native methods --------------------------------------------------------------------------

    private native void initNative();
    private native void closeNative();

    private native void create(String uuid);
    private native int close(String uuid);
    private native int start(String uuid);
    private native int stop(String uuid);
    private native int pause(String uuid, boolean paused);
    private native int reset(String uuid);

    private native int registerEvent(String uuid, int type, String tags, long samples, int action, String player, boolean once);
    private native int registerEventInMilliseconds(String uuid, int type, String tags, float ms, int action, String player, boolean once);
    private native int unregisterEvent(String uuid, String tag);
    private native int unregisterAllEvents(String uuid);

    private native int setPeriod(String uuid, long samples);
    private native int setPeriodInMilliseconds(String uuid, float ms);
    private native int setClock(String uuid, long samples);
    private native int setClockInMilliseconds(String uuid, float ms);

    private native int setMetronome(String uuid, String metro);
    private native int setBeatsPerMinute(String uuid, int bpm);
    private native int setBeatPeriodInSamples(String uuid, long samples);
    private native int setBeatPeriodInMilliseconds(String uuid, float ms);

    private native long getDSPClock(String uuid);
    private native float getDSPClockInMilliseconds(String uuid);
    private native float getDSPCycleLatency(String uuid);
    private native float getDSPCycleExpectedLatency(String uuid);
}