// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package mobi.orbe.cordova.fmod;

import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FMODInput extends CordovaPlugin
{


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);
        Log.d("FMODInput", "initialized");
    }

    @Override
    public void onDestroy()
    {
        closeNative();
        Log.d("FMODInput", "destroyed");
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext context) throws JSONException
    {
        String uuid = FMOD.getUUIDFromJS(args, context);
        if (uuid.isEmpty()) return false;

        JSONObject json = new JSONObject();
        if (args.get(0).getClass().equals(JSONObject.class)) {
            json = args.getJSONObject(0);
        }

        switch (action) {
            case "init":
                this.init(uuid, context);
                return true;
            case "close":
                this.close(uuid, context);
                return true;
            case "start" :
                this.start(uuid, context);
                return true;
            case "stop" :
                this.stop(uuid, context);
                return true;
            case "setInputChannels" :
                this.setInputChannels(uuid, json, context);
                return true;
            case "setAudioFormat" :
                this.setAudioFormat(uuid, json, context);
                return true;
        }

        return false;
    }


    /**
     * Parse and send JNI result.
     *
     * @param uuid The native object identifier.
     * @param result The native command status.
     * @param context The context to be fired.
     */
    private void parseResult(String uuid, int result, CallbackContext context)
    {
        if (result==FMOD.JNI_RESULT_ERROR) {
            String message = getLastErrorMessage(uuid);
            FMOD.sendResult(context, result, message);
        }
        else FMOD.sendResult(context, result);
    }


    private void init(String uuid, CallbackContext context)
    {
        int result = create(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    private void close(String uuid, CallbackContext context)
    {
        int result = close(uuid);
        switch(result) {
            case FMOD.JNI_RESULT_NOTFOUND:
                FMOD.sendNotFound(context);
                break;
            case FMOD.JNI_RESULT_ERROR:
                String msg = getLastErrorMessage(uuid);
                context.error(msg);
                break;
            case FMOD.JNI_RESULT_OK:
                context.success();
                break;
        }
    }

    // -- actions ----------------------------------------------------------------------------------

    private void start(String uuid, CallbackContext context)
    {
        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                int result = start(uuid);
                parseResult(uuid, result, context);
            }
        });
    }

    private void stop(String uuid, CallbackContext context)
    {
        int result = stop(uuid);
        parseResult(uuid, result, context);
    }

    // -- setters ----------------------------------------------------------------------------------

    private void setInputChannels(String uuid, JSONObject options, CallbackContext context)
    {
        int channels = options.optInt("channels");
        int result = setInputChannels(uuid, channels);

        parseResult(uuid, result, context);
    }

    private void setAudioFormat(String uuid, JSONObject options, CallbackContext context)
    {
        int format = options.optInt("format");
        int result = setAudioFormat(uuid, format);

        parseResult(uuid, result, context);
    }


    // --- native methods --------------------------------------------------------------------------

    private native void closeNative();

    private native int create(String uuid);
    private native int close(String uuid);
    private native int start(String uuid);
    private native int stop(String uuid);

    private native int setInputChannels(String uuid, int channels);
    private native int setAudioFormat(String uuid, int format);

    private native String getLastErrorMessage(String uuid);
}