// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include "FMODPlugin.h"

// external static DSP plugin
#if CORDOVA_PLUGIN_RESONANCEAUDIO
#include "ResonanceAudioPlugin.h"
#include "ResonanceAudioDSP.h"
#endif


// version 0xaaaabbcc -> aaaa = product version, bb = major version, cc = minor version.
const char *FMOD_convertVersion(unsigned int version)
{
    char* output = new char[10];

    int product = (version >> 16) & 0xFFFF;
    int major = (version >> 8) & 0xFF;
    int minor = version & 0xFF;

    sprintf(output, "%x.%02x.%02x", product, major, minor);
    return output;
};

map<std::string, FMODPlugin::AudioUnit*> units; // FMOD audio units stored by ID





FMODPlugin::Context::Context(int sr)
{
    inited = false;
    channels = 0;
    samplerate = sr;
    system = NULL;
    
    if (!system) {
        FMOD_RESULT result = FMOD_OK;
        
        result = FMOD::System_Create(&system);
        FMOD_ERRCHECK(result);
        
        if (system) {
            result = system->setSoftwareFormat(samplerate, FMOD_SPEAKERMODE_DEFAULT, 0);
            FMOD_ERRCHECK(result);

            
            // Adjusting these values can lead to extremely low latency performance (smaller values),
            // or greater stability in sound output (larger values).
            //
            // FMOD chooses the most optimal size by default for best stability, depending on the output
            // type. It is not recommended changing this value unless you really need to. You may get
            // worse performance than the default settings chosen by FMOD.
            //result = system->setDSPBufferSize(256, 4);
            //FMOD_ERRCHECK(result);
        
            result = system->init(16, FMOD_INIT_NORMAL, 0);
            FMOD_ERRCHECK(result);
            
            // require by stream
            result = system->setStreamBufferSize(64*1024, FMOD_TIMEUNIT_RAWBYTES);
            FMOD_ERRCHECK(result);

            // register plugin DSP(s)
            registerDSPPlugins();
            
            // retrieves the master ChannelGroup that all sounds ultimately route to
            result = system->getMasterChannelGroup(&master);
            FMOD_ERRCHECK(result);

            Log_d("system created");
            inited = true;
        }
    }
    else Log_w("FMOD system already inited");

    if (!system) Log_e("the FMOD system is not initialized");
}

FMODPlugin::Context::~Context()
{
    close();
}



void FMODPlugin::Context::update()
{
    if (system && inited) {
        FMOD_RESULT result;
        
        result = system->update();
        FMOD_ERRCHECK(result);
        result = system->getChannelsPlaying(&channels);
        FMOD_ERRCHECK(result);
        
        // audio unit
        for(auto it:units) it.second->update();
    }
}

bool FMODPlugin::Context::suspend()
{
    if (system) {
        Log_w("suspend mixer thread and relinquish usage of audio hardware");
        FMOD_RESULT result = system->mixerSuspend();
        FMOD_ERRCHECK(result);
        return result == FMOD_OK;
    }
    
    return false;
}

bool FMODPlugin::Context::resume()
{
    if (system) {
        Log_w("resume mixer thread and reacquire access to audio hardware");
        FMOD_RESULT result = system->mixerResume();
        FMOD_ERRCHECK(result);
        return result == FMOD_OK;
    }
    
    return false;
}

void FMODPlugin::Context::close()
{
    if (system) {
        FMOD_RESULT result;

        result = system->close();
        FMOD_ERRCHECK(result);
        result = system->release();
        FMOD_ERRCHECK(result);

        inited = false;
        system = NULL;
        
        Log_d("FMOD system closed");
    }
}



FMODPlugin::Player* FMODPlugin::Context::createPlayer(string id)
{
    FMODPlugin::Player *p = new FMODPlugin::Player(id);
    p->init(system);

    return p;
}

FMODPlugin::Metronome* FMODPlugin::Context::createMetronome(string id)
{
    FMODPlugin::Metronome *p = new FMODPlugin::Metronome(id);
    p->init(system);

    return p;
}

FMODPlugin::Scheduler* FMODPlugin::Context::createScheduler(string id)
{
    FMODPlugin::Scheduler *p = new FMODPlugin::Scheduler(id);
    p->init(system);

    return p;
}

FMODPlugin::Input* FMODPlugin::Context::createInput(string id)
{
    FMODPlugin::Input *p = new FMODPlugin::Input(id);
    p->init(system);
    
    units[id] = (AudioUnit*)p;

    return p;
}

FMODPlugin::Recorder* FMODPlugin::Context::createRecorder(string id)
{
    FMODPlugin::Recorder *p = new FMODPlugin::Recorder(id);
    p->init(system);
    
    units[id] = (AudioUnit*)p;
    return p;
}

FMODPlugin::Analyser* FMODPlugin::Context::createAnalyser(string id)
{
    FMODPlugin::Analyser *p = new FMODPlugin::Analyser(id);
    p->init(system);
    
    units[id] = (AudioUnit*)p;
    return p;
}


FMODPlugin::AudioUnit* FMODPlugin::Context::getAudioUnit(string id)
{
    return (units.find(id) != units.end()) ? units[id] : nullptr;
}



unsigned int FMODPlugin::Context::getVersionCode()
{
    unsigned int version = 0; // FMOD_VERSION or use the library version by default
    if (system) system->getVersion(&version);
    
    return version;
}

const char* FMODPlugin::Context::getVersionNumber()
{
    unsigned int version = getVersionCode();
    return FMOD_convertVersion(version);
}

int FMODPlugin::Context::getChannelsPlaying() {
    return channels;
}

FMOD_CPU_USAGE FMODPlugin::Context::getCPUUsage()
{
    FMOD_CPU_USAGE usage;
    usage.dsp = 0;
    usage.stream = 0;
    usage.geometry = 0;
    usage.update = 0;
    usage.convolution1 = 0;
    usage.convolution2 = 0;
    
    if (system) system->getCPUUsage(&usage);
    return usage;
}

unsigned long long FMODPlugin::Context::getEllapsedTime()
{
    unsigned long long samples = 0;
    
    if (master) {
        FMOD_RESULT result = master->getDSPClock(0, &samples);
        FMOD_ERRCHECK(result);
    }
    
    return SAMPLES_TO_MS(samples, samplerate);
}



void FMODPlugin::Context::registerDSPPlugins()
{
    FMOD_RESULT result = FMOD_OK;
    
    // register plugin DSP
    result = system->registerDSP(Clock_GetDSPDescription(), 0);
    FMOD_ERRCHECK(result);
    result = system->registerDSP(Recorder_GetDSPDescription(), 0);
    FMOD_ERRCHECK(result);
    
    // register Resonance Audio DSPs
    #if CORDOVA_PLUGIN_RESONANCEAUDIO
    result = system->registerDSP(ResonanceAudioPlugin_GetFmodDSPDescription(), 0);
    FMOD_ERRCHECK(result);
    result = system->registerDSP(ResonanceAudioSource_GetFmodDSPDescription(), 0);
    FMOD_ERRCHECK(result);
    #endif
    
    
    #if DEBUG
    int          pluginnumber;
    unsigned int pluginhandle;
    char         pluginname[256];
    unsigned int pluginversion;
    
    // list loaded DSP plugins
    result = system->getNumPlugins(FMOD_PLUGINTYPE_DSP, &pluginnumber);
    FMOD_ERRCHECK(result);
    
    Log_d("Number of loaded plugins (type DSP): %i", pluginnumber);
    for (int i=0; i<pluginnumber; i++) {
        result = system->getPluginHandle(FMOD_PLUGINTYPE_DSP, i, &pluginhandle);
        FMOD_ERRCHECK(result);
        result = system->getPluginInfo(pluginhandle, 0, pluginname, 256, &pluginversion);
        FMOD_ERRCHECK(result);
        
        Log_d("\tloaded DSP [%i]: %s v%s", i, pluginname, FMOD_convertVersion(pluginversion));
    }
    
    // list nested DSP plugins
    result = system->getNumNestedPlugins(FMOD_PLUGINTYPE_DSP, &pluginnumber);
    FMOD_ERRCHECK(result);
    
    Log_d("Number of nested plugins (type DSP): %i", pluginnumber);
    for (int i=0; i<pluginnumber; i++) {
        result = system->getPluginHandle(FMOD_PLUGINTYPE_DSP, i, &pluginhandle);
        FMOD_ERRCHECK(result);
        result = system->getPluginInfo(pluginhandle, 0, pluginname, 256, &pluginversion);
        FMOD_ERRCHECK(result);
        
        Log_d("\tnested DSP [%i]: %s v%s", i, pluginname, FMOD_convertVersion(pluginversion));
    }
    #endif
}





// external Resonance Audio plugin
#if CORDOVA_PLUGIN_RESONANCEAUDIO
void FMODPlugin::Context::addResonanceAudioPlugin()
{
    FMOD_RESULT result;
    
    int samplerate;
    unsigned int blocksize;
    
    // creates the ResonanceAudio system
    result = system->getSoftwareFormat(&samplerate, 0, 0);
    FMOD_ERRCHECK(result);
    result = system->getDSPBufferSize(&blocksize, 0);
    FMOD_ERRCHECK(result);
    
    ResonanceAudioPlugin::createSystem(samplerate, ResonanceAudioPlugin::kOutputChannels, blocksize);
    
    // add the main / listener DSP
    ResonanceAudioPlugin::DSP::fmod::addListenerDSP(system);
}
void FMODPlugin::Context::removeResonanceAudioPlugin()
{
    ResonanceAudioPlugin::DSP::fmod::removeListenerDSP(system);
}
#endif
