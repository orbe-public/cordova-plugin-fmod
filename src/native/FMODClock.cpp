// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "FMODPlugin.h"



FMOD_DSP_DESCRIPTION Clock_dspDesc;

extern "C"
F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL FMODPlugin::Clock_GetDSPDescription()
{
    memset(&Clock_dspDesc, 0, sizeof(Clock_dspDesc));
    strncpy(Clock_dspDesc.name, "FMOD clock", sizeof(Clock_dspDesc.name));

    Clock_dspDesc.version             = 0x00010000;                                // plug-in version
    Clock_dspDesc.numinputbuffers     = 1;                                         // number of input buffers to process
    Clock_dspDesc.numoutputbuffers    = 1;                                         // number of output buffers to process
    Clock_dspDesc.read                = FMODPlugin::Clock_dspRead;             // Called on each DSP update
    Clock_dspDesc.create              = FMODPlugin::Clock_dspCreate;           // DSP::createDSP callback
    Clock_dspDesc.shouldiprocess      = FMODPlugin::Clock_dspShouldIProcess;   // DSP::shouldIProcess callback
    
    return &Clock_dspDesc;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Clock_dspCreate (FMOD_DSP_STATE *dsp_state)
{
    FMOD::DSP *dsp;
    FMOD::System *system;
    FMOD_RESULT result;
    void *userdata;

    // retrieve the internal DSP and FMOD system
    dsp = ((FMOD::DSP*)dsp_state->instance);
    result = dsp->getSystemObject(&system);
    FMOD_ERRCHECK(result);

    // set the DSP state data to the user data (i.e. the target clock)
    result = dsp->getUserData(&userdata);
    dsp_state->plugindata = (FMODPlugin::Clock*)userdata;
    
    return !dsp_state->plugindata ? FMOD_ERR_MEMORY : FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Clock_dspRead
(FMOD_DSP_STATE *dsp_state, float *inbuffer, float *outbuffer, unsigned int length, int inchannels, int *outchannels)
{
    // nothing to do here
    // Clock_dspShouldIProcess return "don't process"
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Clock_dspShouldIProcess
(FMOD_DSP_STATE *dsp_state, FMOD_BOOL inputsidle, unsigned int length, FMOD_CHANNELMASK inmask, int inchannels, FMOD_SPEAKERMODE speakermode)
{
    FMODPlugin::Clock *clock = (FMODPlugin::Clock*)dsp_state->plugindata;
    unsigned long long samples = 0;
    clock->mixer->getDSPClock(0, &samples);
    
    switch (clock->type) {
        case FMOD_CLOCK_TYPE_METRONOME:
            ((FMODPlugin::Metronome*)clock->target)->process(samples);
            break;
        default:
            break;
    }
    
    return FMOD_ERR_DSP_DONTPROCESS;
}
