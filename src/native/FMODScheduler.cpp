// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <sstream>
#include "FMODPlugin.h"





static string trim(string s)
{
    const char* whitespaces = " \t";
    s.erase(s.find_last_not_of(whitespaces) + 1);
    s.erase(0, s.find_first_not_of(whitespaces));
    return s;
}





FMODPlugin::Scheduler::Scheduler(string id)
{
    period = 0;
    
    name = id;
    step = 0;
    index = 0;
    milliseconds = 0;
    startat = 0;
    pauseat = 0;
    offset = 0;
    clock = 0;
    
    paused = false;
    running = false;
    sharedClock = false;
}

FMODPlugin::Scheduler::~Scheduler()
{
    close();
}



void FMODPlugin::Scheduler::init(FMOD::System *system)
{
    // get system sample rate
    FMOD_RESULT result = system->getSoftwareFormat(&samplerate, 0, 0);
    FMOD_ERRCHECK(result);
    
    // create an internal 
    metronome = new Metronome(name);
    metronome->init(system);
    metronome->addCallback(name, this);
};

void FMODPlugin::Scheduler::close()
{
    unregisterAllEvents();
    stop();
    
    if (metronome){
        metronome->removeCallback(name);
        metronome->closeDSP();
    }
}

bool FMODPlugin::Scheduler::start()
{
    // TODO: define message to explain ?
    if (running || !metronome) return false;
    
    step = 0; // unlike the metronome, always reset the step index
    setClock(startat, true);
    
    metronome->startDSP();
    running = true;
    
    return true;
};

bool FMODPlugin::Scheduler::stop()
{
    // already stopped
    if (!metronome || (!sharedClock && !metronome->stopDSP())) return false;
    
    targets.begin();
    for(pair<string, Player*> t : targets) t.second->stop();
    
    running = false;
    return true;
};

void FMODPlugin::Scheduler::pause(bool pause)
{
    if (!running) return;
    if (metronome && pause != paused) {
        paused = pause;
        
        if (paused) pauseat = metronome->getDSPClock();
        else setClock(pauseat, true, false);
        
        // manage playback
        // players will be paused or resumed only if already playing
        targets.begin();
        for(pair<string, Player*> t : targets) t.second->pause(paused  );
    }
}

void FMODPlugin::Scheduler::reset()
{
    targets.begin();
    for(pair<string, Player*> t : targets) t.second->stop();
    
    step = 0;
    setClock(0);
};


bool FMODPlugin::Scheduler::setMetronome(Metronome* m)
{
    if (m) {
        if (metronome) {
            // @todo: metronome are shared do we really need to close them here
            if (m != metronome) metronome->close();
        }
        sharedClock = true;
        metronome = m;
        metronome->addCallback(name, this);
        return true;
    }
    return false;
}

bool FMODPlugin::Scheduler::setPeriod(unsigned long long samples)
{
    period = samples;
    return true;
}

bool FMODPlugin::Scheduler::setPeriodInMilliseconds(float ms)
{
    return setPeriod( MS_TO_SAMPLES(ms,samplerate) );
}

bool FMODPlugin::Scheduler::setClock(unsigned long long samples, bool clear, bool stop)
{
    if (!metronome) return false;

    // forget user's clocks (no longer need)
    // or prepate the next one (if needed)
    if (clear) pauseat = startat = 0;
    else if (!running) {
        startat = samples;
        return true;
    }
    
    // update step
    unsigned long long period = metronome->getBeatPeriodInSamples();
    if (period) step = ceil(samples / period);
    
    bool result = metronome->setDSPClock(samples);
    offset = metronome->getMasterDSPClock();
    offset -= samples;
    clock = samples;
    milliseconds = SAMPLES_TO_MS(clock, samplerate);
    
    // search next event
    goToEvent(clock, stop);
    
    Log_d("Scheduler clock set to %llu [step: %llu, clock: %llu, ms: %f, offset: %llu]\n", samples, step, clock, milliseconds, offset);
    
    return result;
}

bool FMODPlugin::Scheduler::setClockInMilliseconds(float ms)
{
    return setClock( MS_TO_SAMPLES(ms,samplerate) );
};



void FMODPlugin::Scheduler::registerEvent(SCHEDULER_EVENT_TYPE type, string tags, unsigned long long clock, FMODPlugin::SCHEDULER_EVENT_ACTION action, Player *player, bool once, bool now)
{
    // create event
    FMODSchedulerEvent e;
    e.type = type;
    e.utag = tags;
    e.clock = clock;
    e.once = once;
    e.action = action;
    e.target = player;
    
    // add the current DSP samples
    if (now && this->running) e.clock += this->clock;
    
    // split tags by comma
    stringstream sstr(e.utag);
    while(sstr.good()) {
        string tag;
        getline(sstr, tag, ',');
        e.tags.push_back(trim(tag));
    }
    
    // register
    registerEvent(e);
}

void FMODPlugin::Scheduler::registerEventInMilliseconds(SCHEDULER_EVENT_TYPE type, string tags, float ms, FMODPlugin::SCHEDULER_EVENT_ACTION action, Player *player, bool once, bool now)
{
    registerEvent(type, tags, MS_TO_SAMPLES(ms, samplerate), action, player, once, now);
}

void FMODPlugin::Scheduler::registerEvent(FMODSchedulerEvent e)
{
    // add event and sort by clock
    events.push_back(e);
    events.sort();
    
    // the next event may not exist -> update
    if (this->running && event==events.end()) goToEvent(this->clock, false);
    
    // register player in a separate list
    if (e.target) targets[e.target->getId()] = e.target;
}


bool FMODPlugin::Scheduler::unregisterEvent(string tag)
{
    tag = trim(tag);
    bool found = false;
    
    for(auto e = events.begin(); e != events.end(); ++e) {
        if(find(e->tags.begin(), e->tags.end(), tag) != e->tags.end()) {
            if (e->target) e->target->stop();
            e = events.erase(e);
            found = true;
        }
    }
    
    // TODO: search for next event if deleted
    goToEvent(clock, false);

    return found;
}

void FMODPlugin::Scheduler::unregisterAllEvents()
{
    event = events.end();
    events.clear();
}



void FMODPlugin::Scheduler::goToEvent(unsigned long long samples, bool stop)
{
    event = events.begin();
    while(clock>event->clock && event!=events.end()) {
        if (stop && event->target) event->target->stop(); // do not stop on scheduler pause
        event++;
    }
}

void FMODPlugin::Scheduler::process(unsigned long long i, unsigned long long samples)
{
    if (!running || paused) return;
    
    // end of period loop or stop
    if (period>0 && samples-offset>=period) {
        reset();
        return; // @fixme: we lose a DSP cycle here but correct the current step
    }
    
    // update clock
    clock = samples - offset;
    
    // send next step
    if (index!=i) {
        FMOD_scheduler_stepevent(name.c_str(), step++);
        index = i;
    }
    
    // performs next event(s)
    if (event!=events.end()) {
        while(clock>=event->clock) {

            // perform action
            if (event->type==FMOD_EVENT_TYPE_PLAYBACK) {
                // TODO: handle error if pause
                switch (event->action) {
                    case FMOD_EVENT_START   : event->target->play(); break;
                    case FMOD_EVENT_STOP    : event->target->stop(); break;
                    case FMOD_EVENT_PAUSE   : event->target->pause(true); break;
                    case FMOD_EVENT_RESUME  : event->target->pause(false); break;
                    case FMOD_EVENT_NONE    : break;
                }
            }

            // send callback
            FMOD_scheduler_event(name.c_str(), event->type, event->utag.c_str());

            if (!event->once) {
                if (++event==events.end()) break;
            }
            else event = events.erase(event);
        }
    }
};





bool FMODPlugin::Scheduler::setBeatsPerMinute(int BPM) { return metronome ? metronome->setBeatsPerMinute(BPM) : false;; }
bool FMODPlugin::Scheduler::setBeatPeriodInSamples(unsigned long long samples)  { return metronome ? metronome->setBeatPeriodInSamples(samples) : false; }
bool FMODPlugin::Scheduler::setBeatPeriodInMilliseconds(float ms) { return metronome ? metronome->setBeatPeriodInMilliseconds(ms) : false; }

unsigned long long FMODPlugin::Scheduler::getDSPClock() { return metronome ? metronome->getDSPClock() : 0; }
float FMODPlugin::Scheduler::getDSPClockInMilliseconds() { return metronome ? metronome->getDSPClockInMilliseconds() : 0; }
float FMODPlugin::Scheduler::getDSPCycleLatency() { return metronome ? metronome->getDSPCycleLatency() : 0; }
float FMODPlugin::Scheduler::getDSPCycleExpectedLatency() { return metronome ? metronome->getDSPCycleExpectedLatency() : 0; }
