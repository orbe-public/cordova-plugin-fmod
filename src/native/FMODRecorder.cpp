// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "FMODPlugin.h"

#ifdef __ANDROID__
#include <chrono>
#include <thread>
#define FMOD_DSP_BUFFER_LATENCY_MS 1800 // @todo: completely arbitrary, find a way to compute this value
#endif

#define FMOD_DSP_RECORDER_MIN_LEVEL    -46 // dB



FMOD_DSP_DESCRIPTION Recorder_dspDesc;

extern "C"
F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL FMODPlugin::Recorder_GetDSPDescription()
{
    memset(&Recorder_dspDesc, 0, sizeof(Recorder_dspDesc));
    strncpy(Recorder_dspDesc.name, "FMOD Wave audio recorder", sizeof(Recorder_dspDesc.name));

    Recorder_dspDesc.version             = 0x00010000;                          // plug-in version
    Recorder_dspDesc.numinputbuffers     = 1;                                   // number of input buffers to process
    Recorder_dspDesc.numoutputbuffers    = 1;                                   // number of output buffers to process
    Recorder_dspDesc.create              = FMODPlugin::Recorder_dspCreate;      // DSP::createDSP callback
    Recorder_dspDesc.release             = FMODPlugin::Recorder_dspRelease;     // DSP::release callback.
    Recorder_dspDesc.process             = FMODPlugin::Recorder_dspProcess;     // DSP::process callback.
    
    return &Recorder_dspDesc;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Recorder_dspCreate (FMOD_DSP_STATE *dsp_state)
{
    void* userdata;
    FMOD_RESULT result = dsp_state->functions->getuserdata(dsp_state, &userdata);
    FMOD_ERRCHECK(result);
    
    FMOD_DSP_RECORDER_DATA *data = (FMOD_DSP_RECORDER_DATA*)userdata;
    int format = 8 * (data->format <= FMOD_SOUND_FORMAT_PCM32 ? data->format : FMOD_SOUND_FORMAT_PCM32);
    
    dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "Audio input channels: %i\n", data->channels);
    dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "Audio input sample rate: %iHz\n", data->sampleRate);
    dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "Audio input bit depth: %ibit\n", format);
    if (data->waitForSignal) {
        dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "Wait for a minimum level to start recording\n");
        dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "Minimum level: %idB\n", data->level);
    }
    
    
    data->recording = !data->waitForSignal;
    
    // set bit depth and sample rate
    data->recorder.setBitDepth(format);
    data->recorder.setSampleRate(data->sampleRate);
    
    // allocate audio buffers (filled with zeros)
    data->recorder.setNumChannels(data->channels);
    
    
    dsp_state->plugindata = data;
    if (!dsp_state->plugindata) return FMOD_ERR_MEMORY;
    
    dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderCreate", "DSP recorder created\n");
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Recorder_dspRelease (FMOD_DSP_STATE *dsp_state)
{
    if (dsp_state->plugindata) {

        FMOD_DSP_RECORDER_DATA *data = (FMOD_DSP_RECORDER_DATA*)dsp_state->plugindata;
        dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderRelease", "File output: %s\n", data->file.c_str());
        
        // save data and free resources
        bool err = !data->recorder.save(data->file.c_str());
        data->recorder.samples.clear();

        dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderRelease", "DSP recorder released\n");
        
        FMOD_recorder_ended(data->uuid.c_str(), data->file.c_str(), err);
    }
    
    return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODPlugin::Recorder_dspProcess
(FMOD_DSP_STATE* dsp_state, unsigned int length, const FMOD_DSP_BUFFER_ARRAY* inbuffer, FMOD_DSP_BUFFER_ARRAY* outbuffer, FMOD_BOOL idle, FMOD_DSP_PROCESS_OPERATION operation)
{
    if (operation == FMOD_DSP_PROCESS_QUERY) {
        return static_cast<bool>(idle) ? FMOD_ERR_DSP_DONTPROCESS : FMOD_OK;
    }

    if (outbuffer) {
        FMOD_DSP_RECORDER_DATA *data = (FMOD_DSP_RECORDER_DATA*)dsp_state->plugindata;
        float *audiobuf = inbuffer->buffers[0];

        #ifdef __ANDROID__
        data->samples += length;
        if (data->samples < data->buflatency) return FMOD_OK;
        #endif
        
        const int channels = inbuffer->buffernumchannels[0];
        for (unsigned int samp = 0; samp < length; samp++) {
            for (int chan = 0; chan < channels; chan++) {
                int p = (samp * channels) + chan;
                if (!data->recording) {
                    int dB = 20 * log10(abs(audiobuf[p]));
                    if (dB >= data->level) {
                        dsp_state->functions->log(FMOD_DEBUG_LEVEL_LOG, __FILE__, __LINE__, "FMOD_DSP_RecorderProcess", "Start recording @ %idB\n", dB);
                        data->recording = true;
                    }
                }
                else data->recorder.samples[chan].push_back(audiobuf[p]);
            }

        }
    }

    return FMOD_OK;
}




FMODPlugin::Recorder::Recorder(string id)
{
    name = id;
    lasterror = "";
    
    data.uuid = id;
    data.channels = 1;
    data.sampleRate = 48000;;
    data.format = FMOD_SOUND_FORMAT_PCM16;
    data.level = FMOD_DSP_RECORDER_MIN_LEVEL;
    data.waitForSignal = false;
    data.recording = false;
    data.source = nullptr;
    
    dsp = nullptr;
}

FMODPlugin::Recorder::~Recorder()
{
    close();
}



void FMODPlugin::Recorder::init(FMOD::System *sys)
{
    system = sys;
    if (!system) lasterror = "FMOD system is not initialized";
}

void FMODPlugin::Recorder::close()
{
    dsp = nullptr;
}



bool FMODPlugin::Recorder::start(const char* filepath)
{
    if (!system) return false;
    
    if (!data.source) {
        lasterror = "No audio signal to record";
        Log_w(lasterror);
        return false;
    }
    
    // stores the current signal
    sound   = data.source->sound;
    channel = data.source->channel;
    
    if (!channel) {
        lasterror = "You must start your audio source before recording it";
        Log_w(lasterror);
        return false;
    }
    
    FMOD_RESULT result;
    FMOD_DSP_DESCRIPTION *desc = Recorder_GetDSPDescription();
    
    // update data
    #ifdef __ANDROID__
    data.samples = 0;
    // @todo: compute those values
    data.buflatencyMS = FMOD_DSP_BUFFER_LATENCY_MS;
    data.buflatency = MS_TO_SAMPLES(data.buflatencyMS, data.sampleRate) / 1.5;
    #endif
    data.file = filepath;
    result = sound->getFormat(NULL,NULL, &data.channels, NULL);
    FMOD_ERRCHECK(result);

    // and store it to the DSP description
    desc->userdata = &data;
    
    // create and and the DSP
    result = system->createDSP(desc, &dsp);
    FMOD_ERRCHECK(result);
    result = channel->addDSP(FMOD_CHANNELCONTROL_DSP_TAIL, dsp);
    FMOD_ERRCHECK(result);
    
    return result == FMOD_OK;
}

void FMODPlugin::Recorder::stop()
{
    FMOD_RESULT result;
    if (channel) {
        #ifdef __ANDROID__
        this_thread::sleep_for(chrono::milliseconds(data.buflatencyMS));
        #endif
        
        result = channel->removeDSP(dsp);
        FMOD_ERRCHECK(result);
        
        result = dsp->release();
        FMOD_ERRCHECK(result);
    }
}



bool FMODPlugin::Recorder::setInput(AudioUnit* signal)
{
    data.source = signal;
    return true;
}

bool FMODPlugin::Recorder::setSampleRate(int samplerate)
{
    data.sampleRate = samplerate;
    return true;
}

bool FMODPlugin::Recorder::setAudioFormat(FMOD_SOUND_FORMAT format)
{
    data.format = format;
    return true;
}

bool FMODPlugin::Recorder::ignoreLeadingSilence(bool enable, unsigned int level)
{
    data.waitForSignal = enable;
    data.level = level;
    return true;
}



const char* FMODPlugin::Recorder::getLastErrorMessage()
{
    return lasterror;
}
