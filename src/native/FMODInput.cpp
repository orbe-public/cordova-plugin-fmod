// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "FMODPlugin.h"

#define FMOD_INPUT_DEVICE_INDEX 0





FMODPlugin::Input::Input(string id)
{
    name = id;
    inited = false;
    lasterror = "";
    
    exinfo = {0};
    sound = nullptr;
    channel = nullptr;
}

FMODPlugin::Input::~Input()
{
    close();
}



void FMODPlugin::Input::init(FMOD::System *sys)
{
    system = sys;
    
    if (!system) lasterror = "FMOD system is not initialized";
    else {
        
        FMOD_RESULT result;
        
        int ndrivers = 0;
        int nativeRate = 0;
        int nativeChannels = 0;
        
        result = system->getRecordNumDrivers(NULL, &ndrivers);
        FMOD_ERRCHECK(result);

        if (ndrivers == 0) {
            lasterror = "No recording devices found";
            Log_w(lasterror);
            return;
        }
        
        result = system->getRecordDriverInfo(FMOD_INPUT_DEVICE_INDEX, NULL, 0, NULL, &nativeRate, NULL, &nativeChannels, NULL);
        FMOD_ERRCHECK(result);
        
        
        // create sound to record into
        exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
        exinfo.numchannels      = nativeChannels;
        exinfo.format           = FMOD_SOUND_FORMAT_PCM16;
        exinfo.defaultfrequency = nativeRate;
        exinfo.length           = nativeRate * sizeof(short) * nativeChannels;
        
        inited = result==FMOD_OK;
    }
}

void FMODPlugin::Input::close()
{
}



bool FMODPlugin::Input::start()
{
    if (!inited) return false;
    
    FMOD_RESULT result = system->createSound(0, FMOD_LOOP_NORMAL | FMOD_OPENUSER, &exinfo, &sound);
    FMOD_ERRCHECK(result);
    
    result = system->recordStart(FMOD_INPUT_DEVICE_INDEX, sound, true);
    FMOD_ERRCHECK(result);
    
    if (result != FMOD_OK) {
        lasterror = FMOD_ErrorString(result);
        result = sound->release();
        FMOD_ERRCHECK(result);
        sound = nullptr;
        return false;
    }
    
    return true;
}

void FMODPlugin::Input::update()
{
    if (!inited || !sound) return;
    
    FMOD_RESULT result;
    if (!channel) {
        result = system->playSound(sound, 0, false, &channel);
        FMOD_ERRCHECK(result);
        result = channel->setMute(true);
        FMOD_ERRCHECK(result);

        if (result != FMOD_OK) {
            lasterror = FMOD_ErrorString(result);
        }
        
        // @todo: the actual start event is here, send to js
    }
}

void FMODPlugin::Input::stop()
{
    FMOD_RESULT result = system->recordStop(FMOD_INPUT_DEVICE_INDEX);
    FMOD_ERRCHECK(result);
    
    if (sound) {
        result = sound->release();
        FMOD_ERRCHECK(result);
        
        channel = nullptr;
        sound = nullptr;
    }
}



bool FMODPlugin::Input::setInputChannels(int channels)
{
    exinfo.numchannels = channels;
    return true;
}

bool FMODPlugin::Input::setAudioFormat(FMOD_SOUND_FORMAT format)
{
    exinfo.format = format;
    return true;
}



bool FMODPlugin::Input::isInited()
{
    return inited;
}

const char* FMODPlugin::Input::getLastErrorMessage()
{
    return lasterror;
}
