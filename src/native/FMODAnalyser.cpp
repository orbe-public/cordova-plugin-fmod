// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <chrono>
#include "FMODPlugin.h"

#define FMOD_INPUT_DEVICE_INDEX 0





FMODPlugin::Analyser::Analyser(string id)
{
    name = id;
    lasterror = "";
    
    doFFT = false;
    doMetering = false;
    running = false;

    source = nullptr;
    channel = nullptr;
    fft = nullptr;
    
    period = 1000 / 20;
}

FMODPlugin::Analyser::~Analyser()
{
    close();
}



void FMODPlugin::Analyser::init(FMOD::System *sys)
{
    system = sys;
    if (!system) lasterror = "FMOD system is not initialized";
}

void FMODPlugin::Analyser::close()
{
}



bool FMODPlugin::Analyser::start()
{
    if (!system) return false;
    
    if (!source) {
        lasterror = "No audio signal to analyse";
        Log_w(lasterror);
        return false;
    }
    
    running = true;
    return true;
}

void FMODPlugin::Analyser::update()
{
    if (!running || !source->channel) return;
    
    // refers to the last channel source
    // and attach DSP to the currnet one
    if (!channel) {
        channel = source->channel;
        last_ms = 0;
        
        FMOD_RESULT result;
        
        if (doFFT || doMetering) {
            result = system->createDSPByType(FMOD_DSP_TYPE_FFT, &fft);
            FMOD_ERRCHECK(result);
            result = channel->addDSP(FMOD_CHANNELCONTROL_DSP_TAIL, fft);
            FMOD_ERRCHECK(result);
            result = fft->setMeteringEnabled(doMetering, doMetering);
            FMOD_ERRCHECK(result);
        }
    }
    
    // check the interval between the last analysis (if needed)
    if (period > 0) {
        uint64_t ms = chrono::duration_cast<chrono::milliseconds>(chrono::system_clock::now().time_since_epoch()).count();
        if (ms - last_ms < period) return;
        else last_ms = ms;
    }

    FMOD_DSP_ANALYSER_DATA data;
    fill(data.metering.peak, data.metering.peak+2, 0);
    fill(data.metering.rms, data.metering.rms+2, 0);
    data.fft.centroid = 0;
    
    // FFT + metering analysis
    if (fft) {
        if (doFFT) {
            fft->getParameterFloat(FMOD_DSP_FFT_DOMINANT_FREQ, &data.fft.centroid, 0, 0);
        }
        if (doMetering) {
            FMOD_DSP_METERING_INFO out = {};
            fft->getMeteringInfo(0, &out);
            data.metering.peak[0] = out.peaklevel[0];
            data.metering.peak[1] = out.peaklevel[1];
            data.metering.rms[0] = out.rmslevel[0];
            data.metering.rms[1] = out.rmslevel[1];
        }
    }
    
    FMOD_analyser_data(name.c_str(), &data);
}

void FMODPlugin::Analyser::stop()
{
    FMOD_RESULT result;
    if (channel) {
        if (fft) {
            result = channel->removeDSP(fft);
            FMOD_ERRCHECK(result);
            result = fft->release();
            FMOD_ERRCHECK(result);
            fft = nullptr;
        }
        
        channel = nullptr;
        running = false;
    }
}



bool FMODPlugin::Analyser::setInput(AudioUnit* signal)
{
    source = signal;
    return true;
}

void FMODPlugin::Analyser::setPeriodInMilliseconds(float ms)
{
    period = ms;
}

void FMODPlugin::Analyser::setFFTAnalysis(bool enable)
{
    doFFT = enable;
}

void FMODPlugin::Analyser::setSignalMetering(bool enable)
{
    doMetering = enable;
}



const char* FMODPlugin::Analyser::getLastErrorMessage()
{
    return lasterror;
}
