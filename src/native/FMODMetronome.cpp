// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "FMODPlugin.h"





FMODPlugin::Metronome::Metronome(string id)
{
    name = id;
    
    cycle_t = 0;
    cycle_e = 0;
    cycle_p = 0;
    cycle_i = -1;
    index = 0;
    milliseconds = 0;
//    startat = 0;
//    pauseat = 0;
    offset = 0;
    clock = 0;
    
    running = false;
    
    dsp = NULL;
    master = NULL;
    
    init(NULL);
}

FMODPlugin::Metronome::~Metronome()
{
    close();
}



void FMODPlugin::Metronome::init(FMOD::System *sys)
{
    system = sys;
    createDSP(FMOD_CLOCK_TYPE_METRONOME);
};

void FMODPlugin::Metronome::close()
{
    closeDSP();
    callbacks.clear();
}

bool FMODPlugin::Metronome::start()
{
    return startDSP();
};

bool FMODPlugin::Metronome::stop()
{
    return stopDSP();
};

void FMODPlugin::Metronome::reset()
{
    resetDSP();
};



bool FMODPlugin::Metronome::setBeatsPerMinute(int BPM)
{
    return setBeatPeriodInSamples( MS_TO_SAMPLES(60000/(float)BPM,samplerate) );
}

bool FMODPlugin::Metronome::setBeatPeriodInMilliseconds(float ms)
{
    return setBeatPeriodInSamples( MS_TO_SAMPLES(ms,samplerate) );
}

bool FMODPlugin::Metronome::setBeatPeriodInSamples(unsigned long long samples)
{
    cycle_p = samples;
    return true;
}

unsigned long long FMODPlugin::Metronome::getBeatPeriodInSamples()
{
    return cycle_p;
}

bool FMODPlugin::Metronome::setDSPClock(unsigned long long samples)
{
    FMOD_RESULT result = master->getDSPClock(0, &offset);
    FMOD_ERRCHECK(result);
    
    offset -= samples;
    clock = samples;
    milliseconds = SAMPLES_TO_MS(clock, samplerate);
    
    Log_d("Clock set to %llu [clock: %llu, ms: %f, offset: %llu]\n", samples, clock, milliseconds, offset);
    
    return result==FMOD_OK;
}

bool FMODPlugin::Metronome::setDSPClockInMilliseconds(float ms)
{
    return setDSPClock( MS_TO_SAMPLES(ms,samplerate) );
};



unsigned long long FMODPlugin::Metronome::getMasterDSPClock()
{
    unsigned long long samples = 0;
    
    if (master) {
        FMOD_RESULT result = master->getDSPClock(0, &samples);
        FMOD_ERRCHECK(result);
    }
    return samples;
};

unsigned long long FMODPlugin::Metronome::getDSPClock()
{
   return clock;
};

float FMODPlugin::Metronome::getDSPClockInMilliseconds()
{
    return milliseconds;
};

float FMODPlugin::Metronome::getDSPCycleLatency()
{
    return running ? cycle_t : 0;
}

float FMODPlugin::Metronome::getDSPCycleExpectedLatency()
{
    return cycle_e;
}

void FMODPlugin::Metronome::addCallback(string id, Scheduler *scheduler)
{
    callbacks[id] = scheduler;
}

void FMODPlugin::Metronome::removeCallback(string id)
{
    auto it = callbacks.find(id);
    if (it != callbacks.end()) callbacks.erase(it);
}





void FMODPlugin::Metronome::createDSP(CLOCK_TYPE type)
{
    if (system) {
        if (!dsp) {
            FMOD_RESULT result;
            int nblocks;
            
            result = system->getMasterChannelGroup(&master);
            FMOD_ERRCHECK(result);
            result = system->getDSPBufferSize(&blocksize, &nblocks);
            FMOD_ERRCHECK(result);
            result = system->getSoftwareFormat(&samplerate, 0, 0);
            FMOD_ERRCHECK(result);

            // expected DSP cycle time
            cycle_e = SAMPLES_TO_MS(blocksize, samplerate);

            Log_d("FMOD mixer sample rate      = %iHz", samplerate);
            Log_d("FMOD mixer buffer length    = %u (default: 1024)", blocksize);
            Log_d("FMOD mixer number of block  = %i (default: 4)", nblocks);
            Log_d("FMOD mixer blocksize        = %.02f ms", cycle_e);
            Log_d("FMOD mixer total buffersize = %.02f ms", cycle_e * nblocks);
            Log_d("FMOD mixer average Latency  = %.02f ms", cycle_e * ((float)nblocks - 1.5f));
            
            // get DSP description and store the clock reference
            FMOD_DSP_DESCRIPTION *dspdesc = Clock_GetDSPDescription();
            FMODPlugin::Clock *clock = new FMODPlugin::Clock();
            clock->type = type;
            clock->mixer = master;
            clock->target = this;
            dspdesc->userdata = clock;
            
            // attach to the master group (first in DSP chain)
            result = system->createDSP(dspdesc, &dsp);
            FMOD_ERRCHECK(result);
            result = master->addDSP(FMOD_CHANNELCONTROL_DSP_HEAD, dsp);
            FMOD_ERRCHECK(result);
        
            Log_d("DSP Clock created");
        }
        else Log_w("DSP Clock already created");
    }
};

void FMODPlugin::Metronome::closeDSP()
{
    FMOD_RESULT result = master->removeDSP(dsp);
    FMOD_ERRCHECK(result);
    
    dsp = NULL;
}

bool FMODPlugin::Metronome::startDSP()
{
    // TODO: check for errors or already started
    if (running) return false;
    
    running = setDSPClock(0);
//    startat = 0;
    
    return running;
};

bool FMODPlugin::Metronome::stopDSP()
{
    // already stopped
    if (!running) return false;
    
    running = false;
    return true;
};

void FMODPlugin::Metronome::resetDSP()
{
    setDSPClock(0);
    index = 0;
};

void FMODPlugin::Metronome::process(unsigned long long samples)
{
    if (!running) return;
    
    unsigned long long bsize = samples - offset - clock;
    clock = samples - offset;
    
    float ms = SAMPLES_TO_MS(clock, samplerate);
    cycle_t = ms - milliseconds;
    milliseconds = ms;
    
    if (cycle_t>=(cycle_e+1.f)) Log_w("Clock latency (%f ms)\n", cycle_t);
    else if (bsize && bsize!=blocksize) Log_w("DSP buffer size mismatch %llu\n", bsize); // bsize can be equal to 0, especially when resetting the cycle
    
    
    // send the current step to user
    if (cycle_p) {
        unsigned long long i = clock / cycle_p;
        if (i!=cycle_i) {
            FMOD_metronome_event(name.c_str(), index++);
            cycle_i = i;
        }
    }

    // send samples to callback
    for(auto it:callbacks) {
        it.second->process(index, samples);
    }
    
};
