// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <jni.h>
#include <stdio.h>
#include <string>
#include <map>


#define STATUS_OK  1
#define STATUS_ERR 0
#define STATUS_NOTFOUND -1

#include "FMODJNI.h"


FMODPlugin::Context *context = 0;                    // FMOD context
map<std::string, FMODPlugin::Input*> inputs;         // FMOD inputs stored by ID
map<std::string, FMODPlugin::Player*> players;       // FMOD players stored by ID
map<std::string, FMODPlugin::Recorder*> recorders;   // FMOD recorders stored by ID
map<std::string, FMODPlugin::Scheduler*> schedulers; // FMOD schedulers stored by ID
map<std::string, FMODPlugin::Metronome*> metronomes; // FMOD metronomes stored by ID
map<std::string, FMODPlugin::Analyser*> analysers;   // FMOD analysers stored by ID

// to invoke Java methods
typedef struct jClassMethod
{
    JNIEnv*     env;         // pointer to the current thread of the JNI interface
    jobject     objRef;      // the global object reference
    jclass      classID;     // the global class identifier
    jmethodID   methodID;    // the class method identifier
}
jClassMethod;
jClassMethod jFMODPlayer;
jClassMethod jFMODRecorder;
jClassMethod jFMODMetronome;
jClassMethod jFMODScheduler;
jClassMethod jFMODAnalyser;



/**
 * Calls when the native library is loaded.
 */
static JavaVM* jvm;
jint JNI_OnLoad(JavaVM *vm, void *reserved)
{
    jvm = vm;

    JNIEnv* env;
    if (jvm->GetEnv((void **)&env, JNI_VERSION_1_2) != JNI_OK) return -1;
    return JNI_VERSION_1_2;
}

/**
 * Returns the appropriate JNI interface.
 * @return the pointer to the current environment thread or NULL.
 */
static JNIEnv* getJNIEnv()
{
    if (jvm == NULL) {
        Log_e("Failed to get JNIEnv: getJavaVM() is NULL");
        return NULL;
    }

    // get jni environment
    JNIEnv *env = NULL;
    jint ret = jvm->GetEnv((void**)&env, JNI_VERSION_1_2);

    switch (ret) {
        // success
        case JNI_OK : return env;
        // thread not attached
        case JNI_EDETACHED :
            Log_d("the current thread is not attached to the VM");
            // TODO : If calling AttachCurrentThread() on a native thread
            // must call DetachCurrentThread() in future.
            // see: http://developer.android.com/guide/practices/design/jni.html
            if (jvm->AttachCurrentThread(&env, NULL) < 0) {
                Log_d("Failed to get the environment using AttachCurrentThread()");
                return NULL;
            }
            else return env;
        // invalid version
        case JNI_EVERSION :
            // Cannot recover from this error
            Log_d("the specified JNI version is not supported");
        default :
            Log_d("Failed to get the current JNI environment using GetEnv()");
            return NULL;
    }
}


// -------------------------------------------------------------------------------------------------
    #pragma mark FMOD helper
// -------------------------------------------------------------------------------------------------

const char *FMOD_media_path(const char *filename)
{
    char *path = (char *)calloc(256, sizeof(char));
    std::string str(filename);

    // distant URL
    if (str.substr(0, 7) == "http://")  return filename;
    if (str.substr(0, 8) == "https://") return filename;

    // already an local asset (in case)
    if (str.substr(0, 22) == "file:///android_asset/") return filename;

    // for file URL (i.e. file://), use the absolute path instead
    if (str.substr(0, 7) == "file://") {
        str.erase(0,7);
        return str.c_str();
    }

    // absolute file path
    if (filename[0] == '/') return filename;

    // relative to bundle not DocumentRoot (i.e www)
    if (str.find("www/") == 0) str.erase(0, 4);

    strcat(path, "file:///android_asset/www/");
    strcat(path, str.c_str());

    return path;
}

/**
 * FMOD player channel event handler.
 * @param uuid - the player identifier
 */
void FMOD_channel_event(const char *uuid)
{
    unsigned int length = 0;
    unsigned int position = 0;

    if (players.find(uuid) != players.end()) {
        length = players[uuid]->getLength(FMOD_TIMEUNIT_PCM);
        position = players[uuid]->getPosition(FMOD_TIMEUNIT_PCM);
        players[uuid]->onPlaybackEnded();

        JNIEnv* env = getJNIEnv();
        if (!env) Log_w("Invalid JNI environment");
        else {
            jstring pid = env->NewStringUTF(uuid);
            jstring msg = env->NewStringUTF(position >= length ? "ended" : "stopped");
            jmethodID mid = env->GetMethodID(jFMODPlayer.classID, "onChannelEvent", "(Ljava/lang/String;Ljava/lang/String;)V");
            env->CallVoidMethod(jFMODPlayer.objRef, mid, pid, msg);
        }
    }
}

/**
 * FMOD recorder ended event handler.
 * @param uuid - the recorder identifier
 * @param path - the recorded audio file path
 * @param error - true if an error occurred during the save process
 */
void FMOD_recorder_ended(const char *uuid, const char *path, bool error)
{
    if (recorders.find(uuid) != recorders.end()) {
        JNIEnv* env = getJNIEnv();
        if (!env) Log_w("Invalid JNI environment");
        else {
            jstring pid   = env->NewStringUTF(uuid);
            jstring msg   = env->NewStringUTF(path);
            jboolean err  = error ? JNI_TRUE : JNI_FALSE;
            jmethodID mid = env->GetMethodID(jFMODRecorder.classID, "onRecordEnd", "(Ljava/lang/String;Ljava/lang/String;Z)V");
            env->CallVoidMethod(jFMODRecorder.objRef, mid, pid, msg, err);
        }
    }
}

/**
 * FMOD metronome event handler.
 * @param uuid - the metronome identifier
 * @param index - the current DSP clock index
 */
void FMOD_metronome_event(const char *uuid, unsigned long long index)
{
    JNIEnv* env = getJNIEnv();
    if (!env) Log_w("Invalid JNI environment");
    else {
        jstring pid = env->NewStringUTF(uuid);
        jmethodID mid = env->GetMethodID(jFMODMetronome.classID, "onMetroEvent", "(Ljava/lang/String;J)V");
        env->CallVoidMethod(jFMODMetronome.objRef, mid, pid, (jlong)index);
    }
}

/**
 * FMOD scheduler step event handler.
 * @param uuid - the scheduler identifier
 * @param step - the current DSP clock step
 */
void FMOD_scheduler_stepevent(const char *uuid, unsigned long long step)
{
    JNIEnv* env = getJNIEnv();
    if (!env) Log_w("Invalid JNI environment");
    else {
        jstring pid = env->NewStringUTF(uuid);
        jmethodID mid = env->GetMethodID(jFMODScheduler.classID, "onSchedulerStepEvent", "(Ljava/lang/String;J)V");
        env->CallVoidMethod(jFMODScheduler.objRef, mid, pid, (jlong)step);
    }
}

/**
 * FMOD scheduler event handler.
 * @param uuid - the scheduler identifier
 * @param type - the event type
 * @param tags - the event tags
 */
void FMOD_scheduler_event(const char *uuid, int type, const char *tags)
{
    JNIEnv* env = getJNIEnv();
    if (!env) Log_w("Invalid JNI environment");
    else {
        jstring pid = env->NewStringUTF(uuid);
        jstring tag = env->NewStringUTF(tags);
        jmethodID mid = env->GetMethodID(jFMODScheduler.classID, "onSchedulerEvent", "(Ljava/lang/String;ILjava/lang/String;)V");
        env->CallVoidMethod(jFMODScheduler.objRef, mid, pid, (jint)type, tag);
    }
}

/**
 * FMOD analyser data handler.
 * @param uuid - the analyser identifier
 * @param data - the audio analysis data
 */
void FMOD_analyser_data(const char *uuid, void *data)
{
    JNIEnv* env = getJNIEnv();
    if (!env) Log_w("Invalid JNI environment");
    else {
        jstring pid = env->NewStringUTF(uuid);
        jdoubleArray jfft = env->NewDoubleArray(1);
        jdoubleArray jmetering = env->NewDoubleArray(4);

        jdouble fft[1];
        jdouble metering[4];

        FMODPlugin::FMOD_DSP_ANALYSER_DATA *analysis = (FMODPlugin::FMOD_DSP_ANALYSER_DATA*)data;

        fft[0] = (double) analysis->fft.centroid;
        env->SetDoubleArrayRegion(jfft, 0, 1, fft);

        metering[0] = (double) analysis->metering.peak[0];
        metering[1] = (double) analysis->metering.peak[1];
        metering[2] = (double) analysis->metering.rms[0];
        metering[3] = (double) analysis->metering.rms[1];
        env->SetDoubleArrayRegion(jmetering, 0, 4, metering);

        jmethodID mid = env->GetMethodID(jFMODAnalyser.classID, "onAnalysisData", "(Ljava/lang/String;[D[D)V");
        env->CallVoidMethod(jFMODAnalyser.objRef, mid, pid, jfft, jmetering);
    }
}



FMODPlugin::Context* FMOD_init_system()
{
    if (!context) context = new FMODPlugin::Context();
    else Log_d("Audio context already exist");
    return context;
}

FMODPlugin::Player* FMOD_get_player(const char* uuid)
{
    map<std::string, FMODPlugin::Player*>::iterator it = players.find(uuid);
    return it != players.end() ? it->second : NULL;
}


extern "C"
{
    /**
     * FMOD (context)
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_init(JNIEnv *env, jobject obj)
    {
        context = FMOD_init_system();
        unsigned int code = context->getVersionCode();

        Log_d(
                "FMOD version (%02x): %x.%02x.%02x",
                FMOD_VERSION,
                (code >> 16) & 0xFFFF,
                (code >> 8) & 0xFF,
                code & 0xFF
        );
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_update(JNIEnv *env, jobject obj) {
        context->update();
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_close(JNIEnv *env, jobject obj) {
        context->close();
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_getVersion(JNIEnv *env, jobject obj)
    {
        unsigned int code = context->getVersionCode();
        return (int)code;
    }

    JNIEXPORT jdoubleArray JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_getCPUUsage(JNIEnv *env, jobject obj)
    {
        jdoubleArray result = env->NewDoubleArray(6);
        FMOD_CPU_USAGE usage = context->getCPUUsage();
        jdouble output[6];

        output[0] = (double) usage.dsp;
        output[1] = (double) usage.stream;
        output[2] = (double) usage.geometry;
        output[3] = (double) usage.update;
        output[4] = (double) usage.convolution1;
        output[5] = (double) usage.convolution2;

        env->SetDoubleArrayRegion(result, 0, 6, output);
        return result;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_getChannelsPlaying(JNIEnv *env, jobject obj)
    {
        return context->getChannelsPlaying();
    }

    JNIEXPORT jlong JNICALL
    Java_mobi_orbe_cordova_fmod_FMOD_getEllapsedTime(JNIEnv *env, jobject obj)
    {
        unsigned long long ms = context->getEllapsedTime();
        return (jlong)ms;
    }


    /**
     * FMOD Player
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_initNative(JNIEnv *env, jobject obj)
    {
        // get the object global reference
        jFMODPlayer.objRef = env->NewGlobalRef(obj);
        // find the locally defined class, and then get its global reference
        jclass cid = env->FindClass("mobi/orbe/cordova/fmod/FMODPlayer");
        jFMODPlayer.classID = (jclass) env->NewGlobalRef(cid);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:players) it.second->~Player();
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        players[id] = context->createPlayer(id);
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_load(JNIEnv *env, jobject obj, jstring uuid, jstring source, jint mode, jboolean stream, jboolean autoplay, jstring dls)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *src = env->GetStringUTFChars(source, 0);
        const char *bank = env->GetStringUTFChars(dls, 0);

        if (players.find(id) != players.end()) {
            if (bank && *bank) players[id]->setMidiSoundBank(bank);
            if (players[id]->load(src, mode, stream, autoplay)) return STATUS_OK;
            else return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_loadFromBuffer(JNIEnv *env, jobject obj, jstring uuid, jcharArray buffer, jint mode, jboolean stream, jboolean autoplay, jstring dls)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *bank = env->GetStringUTFChars(dls, 0);

        if (players.find(id) != players.end()) {
            int length = env->GetArrayLength(buffer);
            char data[length];
            jchar* jdata = env->GetCharArrayElements(buffer, 0);
            for(int i=0; i<length; ++i) data[i] = (char)jdata[i];

            if (bank && *bank) players[id]->setMidiSoundBank(bank);
            if (players[id]->loadFromBuffer(data, length, mode, stream, autoplay)) return STATUS_OK;
            else return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_release(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->release()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Player *>::iterator it = players.find(id);
        if (it != players.end()) {
            if (players[id]->release()) {
                players[id]->~Player();
                players.erase(it);
                return STATUS_OK;
            }
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_play(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->play()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->stop()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_pause(JNIEnv *env, jobject obj, jstring uuid, jboolean paused)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->pause((paused==JNI_TRUE))) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_isPaused(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->isPaused()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_isPlaying(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->isPlaying()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setVolume(JNIEnv *env, jobject obj, jstring uuid, jfloat volume)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setVolume(volume)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setMode(JNIEnv *env, jobject obj, jstring uuid, jint mode)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setMode(mode)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setPosition(JNIEnv *env, jobject obj, jstring uuid, jlong ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setPosition((unsigned int)ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setPitch(JNIEnv *env, jobject obj, jstring uuid, jfloat rate, jboolean shift)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setPitch(rate, (shift==JNI_TRUE))) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setMute(JNIEnv *env, jobject obj, jstring uuid, jboolean muted)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setMute((muted==JNI_TRUE))) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setPan(JNIEnv *env, jobject obj, jstring uuid, jfloat pan)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setPan(pan)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setFadeIn(JNIEnv *env, jobject thiz, jstring uuid, jlong ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setFadeIn((unsigned int)ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_setFadeOut(JNIEnv *env, jobject thiz, jstring uuid, jlong ms, jboolean now)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            if (players[id]->setFadeOut((unsigned int)ms, (now==JNI_TRUE))) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jlong JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_getLength(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            unsigned int length = players[id]->getLength();
            return (jlong)length;
        }

        return STATUS_NOTFOUND;
    }

    JNIEXPORT jlong JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_getPosition(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            unsigned int pos = players[id]->getPosition();
            return (jlong)pos;
        }

        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_getMute(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (players.find(id) != players.end()) {
            bool muted = players[id]->getMute();
            return muted ? STATUS_OK : STATUS_ERR;
        }

        return STATUS_NOTFOUND;
    }

    JNIEXPORT jstring JNICALL
    Java_mobi_orbe_cordova_fmod_FMODPlayer_getLastErrorMessage(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *msg = players[id]->getLastErrorMessage();

        return env->NewStringUTF(msg);
    }


    /**
     * FMOD Metronome
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_initNative(JNIEnv *env, jobject obj)
    {
        // get the object global reference
        jFMODMetronome.objRef = env->NewGlobalRef(obj);
        // find the locally defined class, and then get its global reference
        jclass cid = env->FindClass("mobi/orbe/cordova/fmod/FMODMetronome");
        jFMODMetronome.classID = (jclass) env->NewGlobalRef(cid);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:metronomes) it.second->~Metronome();
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        metronomes[id] = context->createMetronome(id);
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Metronome*>::iterator it = metronomes.find(id);
        if (it != metronomes.end()) {
            metronomes[id]->close();
            metronomes.erase(it);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_start(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->start()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            metronomes[id]->stop();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_reset(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            metronomes[id]->reset();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_setBeatsPerMinute(JNIEnv *env, jobject obj, jstring uuid, jint bpm)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->setBeatsPerMinute(bpm)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_setBeatPeriodInSamples(JNIEnv *env, jobject obj, jstring uuid, jlong samples)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->setBeatPeriodInSamples((unsigned long long)samples)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_setBeatPeriodInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->setBeatPeriodInMilliseconds(ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_setDSPClock(JNIEnv *env, jobject obj, jstring uuid, jlong samples)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->setDSPClock((unsigned long long)samples)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_setDSPClockInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            if (metronomes[id]->setDSPClockInMilliseconds(ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jlong JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_getDSPClock(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            unsigned long long clock = metronomes[id]->getDSPClock();
            return (jlong)clock;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_getDSPClockInMilliseconds(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            float ms = metronomes[id]->getDSPClockInMilliseconds();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_getDSPCycleLatency(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            float ms = metronomes[id]->getDSPCycleLatency();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODMetronome_getDSPCycleExpectedLatency(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (metronomes.find(id) != metronomes.end()) {
            float ms = metronomes[id]->getDSPCycleExpectedLatency();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }


    /**
     * FMOD Scheduler
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_initNative(JNIEnv *env, jobject obj)
    {
        // get the object global reference
        jFMODScheduler.objRef = env->NewGlobalRef(obj);
        // find the locally defined class, and then get its global reference
        jclass cid = env->FindClass("mobi/orbe/cordova/fmod/FMODScheduler");
        jFMODScheduler.classID = (jclass) env->NewGlobalRef(cid);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:schedulers) it.second->~Scheduler();
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        schedulers[id] = context->createScheduler(id);
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Scheduler*>::iterator it = schedulers.find(id);
        if (it != schedulers.end()) {
            schedulers[id]->close();
            schedulers.erase(it);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_start(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->start()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            schedulers[id]->stop();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_pause(JNIEnv *env, jobject obj, jstring uuid, jboolean paused)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            schedulers[id]->pause((paused==JNI_TRUE));
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_reset(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            schedulers[id]->reset();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_registerEvent(JNIEnv *env, jobject obj, jstring uuid, jint type, jstring tags, jlong samples, jint action, jstring player, jboolean once)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            const char *pid = env->GetStringUTFChars(player, 0);
            const char *keys = env->GetStringUTFChars(tags, 0);
            if (!*keys) return -4; // Invalid tag parameter

            FMODPlugin::SCHEDULER_EVENT_TYPE t = static_cast<FMODPlugin::SCHEDULER_EVENT_TYPE>(type);
            if (t==FMODPlugin::FMOD_EVENT_TYPE_CALLBACK) {
                schedulers[id]->registerEvent(t, keys, (unsigned long long) samples, FMODPlugin::FMOD_EVENT_NONE, NULL, (once == JNI_TRUE));
                return STATUS_OK;
            }

            if (t==FMODPlugin::FMOD_EVENT_TYPE_PLAYBACK) {
                FMODPlugin::SCHEDULER_EVENT_ACTION a = static_cast<FMODPlugin::SCHEDULER_EVENT_ACTION>(action);

                // validate
                if (players.find(pid) == players.end()) return -3;  // target player does not exists
                if (!action || a >= FMODPlugin::FMOD_EVENT_NONE) return -2;   // invalid action

                schedulers[id]->registerEvent(t, keys, (unsigned long long) samples, a, players[pid], (once == JNI_TRUE));
                return STATUS_OK;
            }
            return -5; // Invalid type
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_registerEventInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jint type, jstring tags, jfloat ms, jint action, jstring player, jboolean once)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            const char *pid = env->GetStringUTFChars(player, 0);
            const char *keys = env->GetStringUTFChars(tags, 0);
            if (!*keys) return -4; // Invalid tag parameter

            FMODPlugin::SCHEDULER_EVENT_TYPE t = static_cast<FMODPlugin::SCHEDULER_EVENT_TYPE>(type);
            if (t==FMODPlugin::FMOD_EVENT_TYPE_CALLBACK) {
                schedulers[id]->registerEventInMilliseconds(t, keys, ms, FMODPlugin::FMOD_EVENT_NONE, NULL, (once == JNI_TRUE));
                return STATUS_OK;
            }

            if (t==FMODPlugin::FMOD_EVENT_TYPE_PLAYBACK) {
                FMODPlugin::SCHEDULER_EVENT_ACTION a = static_cast<FMODPlugin::SCHEDULER_EVENT_ACTION>(action);

                // validate
                if (players.find(pid) == players.end()) return -3;  // target player does not exists
                if (!action || a >= FMODPlugin::FMOD_EVENT_NONE) return -2;   // invalid action

                schedulers[id]->registerEventInMilliseconds(t, keys, ms, a, players[pid], (once == JNI_TRUE));
                return STATUS_OK;
            }
            return -5; // Invalid type
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_unregisterEvent(JNIEnv *env, jobject obj, jstring uuid, jstring search)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *tag = env->GetStringUTFChars(search, 0);

        if (schedulers.find(id) != schedulers.end()) {
            if(schedulers[id]->unregisterEvent(tag)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_unregisterAllEvents(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            schedulers[id]->unregisterAllEvents();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setMetronome(JNIEnv *env, jobject obj, jstring uuid, jstring metro)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *mid = env->GetStringUTFChars(metro, 0);

        if (schedulers.find(id) != schedulers.end()) {
            if (metronomes.find(mid) != metronomes.end()) {
                FMODPlugin::Metronome *m = metronomes[mid];
                if (schedulers[id]->setMetronome(m)) return STATUS_OK;
                return STATUS_ERR;
            }
            return -2; // metronome not found
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setBeatsPerMinute(JNIEnv *env, jobject obj, jstring uuid, jint bpm)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setBeatsPerMinute(bpm)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setBeatPeriodInSamples(JNIEnv *env, jobject obj, jstring uuid, jlong samples)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setBeatPeriodInSamples((unsigned long long)samples)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setBeatPeriodInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setBeatPeriodInMilliseconds(ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setPeriod(JNIEnv *env, jobject obj, jstring uuid, jlong samples)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setPeriod((unsigned long long)samples)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setPeriodInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setPeriodInMilliseconds(ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setClock(JNIEnv *env, jobject obj, jstring uuid, jlong samples)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setClock((unsigned long long)samples)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_setClockInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            if (schedulers[id]->setClockInMilliseconds(ms)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jlong JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_getDSPClock(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            unsigned long long clock = schedulers[id]->getDSPClock();
            return (jlong)clock;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_getDSPClockInMilliseconds(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            float ms = schedulers[id]->getDSPClockInMilliseconds();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_getDSPCycleLatency(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            float ms = schedulers[id]->getDSPCycleLatency();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jfloat JNICALL
    Java_mobi_orbe_cordova_fmod_FMODScheduler_getDSPCycleExpectedLatency(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (schedulers.find(id) != schedulers.end()) {
            float ms = schedulers[id]->getDSPCycleExpectedLatency();
            return (jfloat)ms;
        }
        return STATUS_NOTFOUND;
    }


    /**
     * FMOD Input
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:inputs) it.second->~Input();
    }

    JNIEXPORT int JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        inputs[id] = context->createInput(id);

        return inputs[id]->isInited() ? STATUS_OK : STATUS_ERR;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Input *>::iterator it = inputs.find(id);
        if (it != inputs.end()) {
            inputs[id]->~Input();
            inputs.erase(it);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_start(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (inputs.find(id) != inputs.end()) {
            if (inputs[id]->start()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (inputs.find(id) != inputs.end()) {
            inputs[id]->stop();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_setInputChannels(JNIEnv *env, jobject obj, jstring uuid, jint channels)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (inputs.find(id) != inputs.end()) {
            if (inputs[id]->setInputChannels(channels)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_setAudioFormat(JNIEnv *env, jobject obj, jstring uuid, jint format)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (inputs.find(id) != inputs.end()) {
            FMOD_SOUND_FORMAT sf = static_cast<FMOD_SOUND_FORMAT>(format);
            if (inputs[id]->setAudioFormat(sf)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jstring JNICALL
    Java_mobi_orbe_cordova_fmod_FMODInput_getLastErrorMessage(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *msg = inputs[id]->getLastErrorMessage();

        return env->NewStringUTF(msg);
    }


    /**
     * FMOD Recorder
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_initNative(JNIEnv *env, jobject obj)
    {
        // get the object global reference
        jFMODRecorder.objRef = env->NewGlobalRef(obj);
        // find the locally defined class, and then get its global reference
        jclass cid = env->FindClass("mobi/orbe/cordova/fmod/FMODRecorder");
        jFMODRecorder.classID = (jclass) env->NewGlobalRef(cid);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:recorders) it.second->~Recorder();
    }

    JNIEXPORT int JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        recorders[id] = context->createRecorder(id);
        return STATUS_OK;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Recorder *>::iterator it = recorders.find(id);
        if (it != recorders.end()) {
            recorders[id]->~Recorder();
            recorders.erase(it);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_start(JNIEnv *env, jobject obj, jstring uuid, jstring file)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *fn = env->GetStringUTFChars(file, 0);
        if (recorders.find(id) != recorders.end()) {
            if (recorders[id]->start(fn)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (recorders.find(id) != recorders.end()) {
            recorders[id]->stop();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }


    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_setInput(JNIEnv *env, jobject obj, jstring uuid, jstring type, jstring unitid)
    {
        const char *id  = env->GetStringUTFChars(uuid, 0);
        if (recorders.find(id) != recorders.end()) {
            const char *uid = env->GetStringUTFChars(unitid, 0);
            FMODPlugin::AudioUnit *unit = context->getAudioUnit(uid);
            if (unit) {
                if (recorders[id]->setInput(unit)) return STATUS_OK;
                else return STATUS_ERR;
            }
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_setSampleRate(JNIEnv *env, jobject obj, jstring uuid, jint samplerate)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (recorders.find(id) != recorders.end()) {
            if (recorders[id]->setSampleRate(samplerate)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_setAudioFormat(JNIEnv *env, jobject obj, jstring uuid, jint format)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (recorders.find(id) != recorders.end()) {
            FMOD_SOUND_FORMAT sf = static_cast<FMOD_SOUND_FORMAT>(format);
            if (recorders[id]->setAudioFormat(sf)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_ignoreLeadingSilence(JNIEnv *env, jobject obj, jstring uuid, jboolean enable, jint level)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (recorders.find(id) != recorders.end()) {
            if (recorders[id]->ignoreLeadingSilence((enable==JNI_TRUE), level)) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jstring JNICALL
    Java_mobi_orbe_cordova_fmod_FMODRecorder_getLastErrorMessage(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *msg = recorders[id]->getLastErrorMessage();

        return env->NewStringUTF(msg);
    }


    /**
     * FMOD Analyser
     */
    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_initNative(JNIEnv *env, jobject obj)
    {
        // get the object global reference
        jFMODAnalyser.objRef = env->NewGlobalRef(obj);
        // find the locally defined class, and then get its global reference
        jclass cid = env->FindClass("mobi/orbe/cordova/fmod/FMODAnalyser");
        jFMODAnalyser.classID = (jclass) env->NewGlobalRef(cid);
    }

    JNIEXPORT void JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_closeNative(JNIEnv *env, jobject obj)
    {
        // cleanup
        for(auto it:analysers) it.second->~Analyser();
    }

    JNIEXPORT int JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_create(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        analysers[id] = context->createAnalyser(id);

        return STATUS_OK;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_close(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);

        map<string, FMODPlugin::Analyser *>::iterator it = analysers.find(id);
        if (it != analysers.end()) {
            analysers[id]->~Analyser();
            analysers.erase(it);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_start(JNIEnv *env, jobject obj, jstring uuid, jobjectArray types)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (analysers.find(id) != analysers.end()) {

            for(int i=0, len=env->GetArrayLength(types); i<len; ++i) {
                jstring type = (jstring) env->GetObjectArrayElement(types, i);
                const char *ctype = env->GetStringUTFChars(type, 0);

                if (strcmp(ctype,"fft")==0) analysers[id]->setFFTAnalysis(true);
                if (strcmp(ctype,"metering")==0) analysers[id]->setSignalMetering(true);
            }

            if (analysers[id]->start()) return STATUS_OK;
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_stop(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (analysers.find(id) != analysers.end()) {
            analysers[id]->stop();
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_setInput(JNIEnv *env, jobject obj, jstring uuid, jstring type, jstring unitid)
    {
        const char *id  = env->GetStringUTFChars(uuid, 0);
        if (analysers.find(id) != analysers.end()) {
            const char *uid = env->GetStringUTFChars(unitid, 0);
            FMODPlugin::AudioUnit *unit = context->getAudioUnit(uid);
            if (unit) {
                if (analysers[id]->setInput(unit)) return STATUS_OK;
                else return STATUS_ERR;
            }
            return STATUS_ERR;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jint JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_setPeriodInMilliseconds(JNIEnv *env, jobject obj, jstring uuid, jfloat ms)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        if (analysers.find(id) != analysers.end()) {
            analysers[id]->setPeriodInMilliseconds(ms);
            return STATUS_OK;
        }
        return STATUS_NOTFOUND;
    }

    JNIEXPORT jstring JNICALL
    Java_mobi_orbe_cordova_fmod_FMODAnalyser_getLastErrorMessage(JNIEnv *env, jobject obj, jstring uuid)
    {
        const char *id = env->GetStringUTFChars(uuid, 0);
        const char *msg = analysers[id]->getLastErrorMessage();

        return env->NewStringUTF(msg);
    }

}
