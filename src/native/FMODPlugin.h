// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <map>

#include "fmod.hpp"
#include "fmod_errors.h"
#include "AudioFile.h"



using namespace std;


#define MS_TO_SAMPLES(ms,samplerate) (ms * samplerate / 1000)
#define SAMPLES_TO_MS(s,samplerate)  (s * 1000.0f / (float)samplerate)


// -------------------------------------------------------------------------------------------------
    #pragma mark log
// -------------------------------------------------------------------------------------------------

#ifdef __ANDROID__
#include <android/log.h>
#else
void NSLog_d(const char* message);
void NSLog_w(const char* message);
void NSLog_e(const char* message);
#endif

/**
 * Log message:
 * use log() for verbose messages
 * use warn() for warning messages
 * use error() for error messages
 */
static void Log_d( const char* format, ... );
static void Log_w( const char* format, ... );
static void Log_e( const char* format, ... );

void Log_d( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    #if DEBUG
    __android_log_write( ANDROID_LOG_INFO, "FMOD plugin", msg );
    #endif
    #else
    NSLog_d(msg);
    #endif
}

void Log_w( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_WARN, "FMOD plugin", msg );
    #else
    NSLog_w(msg);
    #endif
}

void Log_e( const char* format, ... )
{
    char msg[1024];
    va_list args;

    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    #ifdef __ANDROID__
    __android_log_write( ANDROID_LOG_ERROR, "FMOD plugin", msg );
    #else
    NSLog_e(msg);
    #endif
}



/**
 * FMOD internal error
 */
static void FMOD_log_error(FMOD_RESULT result, const char* file, int line);
#define FMOD_ERRCHECK(_result) FMOD_log_error(_result, __FILE__, __LINE__)

void FMOD_log_error(FMOD_RESULT result, const char* file, int line)
{
    if (result != FMOD_OK) {
        string path = string(file);
        string filename = path.substr(path.find_last_of("/\\") + 1);
        Log_e("%s(%d): FMOD internal error %d - %s\n", filename.c_str(), line, result, FMOD_ErrorString(result));
    }
}



// -------------------------------------------------------------------------------------------------
    #pragma mark platforms specific methods
// -------------------------------------------------------------------------------------------------

const char* FMOD_media_path(const char *filename);

#ifndef __ANDROID__
const char* FMOD_tempory_path(const char *filename);
bool FMOD_mkdir(const char *filepath);
#endif

// async callbacks
void FMOD_channel_event(const char *uuid);
void FMOD_recorder_ended(const char *uuid, const char *path, bool error);
void FMOD_metronome_event(const char *uuid, unsigned long long index);
void FMOD_scheduler_stepevent(const char *uuid, unsigned long long index);
void FMOD_scheduler_event(const char *uuid, int type, const char *tags);
void FMOD_analyser_data(const char *uuid, void *data);







namespace FMODPlugin
{
    // class definition
    class Context;
    class Player;
    class Clock;
    class Metronome;
    class Scheduler;
    class Input;
    class Recorder;
    class Analyser;


    struct SoundInfo {
        FMOD_SOUND_TYPE type;       // the type of the sound
        FMOD_SOUND_FORMAT format;   // the format of the sound
        float samplerate;           // the sample rate of the sound
        int channels;               // the number of channels for the sound
        int bits;                   // the number of bits per sample for the sound
        unsigned int samples;       // the length of the sound in samples
        unsigned int duration;      // the length of the sound in milliseconds
    };

    struct Fade {
        Fade() : length(0) {}
        
        unsigned int length;        // the duration of the fade in milliseconds
        unsigned long long samples; // the duration ot fhe fade in samples
        unsigned long long start;   // the start-up DSP clock
        unsigned long long end;     // the end DSP clock
        
        // reset clocks
        void clear() { start = end = 0; };
    };

    struct AudioUnit {
        string name;            // the audio unit name (i.e. identifier)
        
        FMOD::Sound *sound;     // the audio unit sound
        FMOD::Channel *channel; // the audio unit channel
        
        // to be overrided: this method will ba called on each context update
        virtual void update() {};
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Context
    // ---------------------------------------------------------------------------------------------

    class Context
    {
        public:
        
             Context(int samplerate = 48000);
            ~Context();

            // Updates the FMOD system
            void update();
            // Suspend the mixer thread and relinquish usage of audio hardware
            bool suspend();
            // Reseume the ixer thread and reacquire access to audio hardware
            bool resume();
            // Close the connection to the output and return to an uninitialized state,
            // frees resources
            void close();

            // Creates and returns a new sound file player
            Player* createPlayer(string id);
            // Creates and returns a new metronome
            Metronome* createMetronome(string id);
            // Creates and returns a new scheduler
            Scheduler* createScheduler(string id);
            // Creates and returns a new audio input listener
            Input* createInput(string id);
            // Creates and returns a new audio recorder
            Recorder* createRecorder(string id);
            // Creates and returns a new audio analyser
            Analyser* createAnalyser(string id);
        
            // Retrieves a registered audio unit by id
            AudioUnit* getAudioUnit(string id);
            
            // Retrieves the FMOD version code
            unsigned int getVersionCode();
            // Retrieves the FMOD version number
            const char* getVersionNumber();
            // Retrieves the number of currently playing channels
            int getChannelsPlaying();
            // Retrieves the amount of CPU used for different parts of the Core engine
            FMOD_CPU_USAGE getCPUUsage();
            // Retrieves the number of milliseconds elapsed since the FMOD system was started
            unsigned long long getEllapsedTime();
        
        
            #if CORDOVA_PLUGIN_RESONANCEAUDIO
            void addResonanceAudioPlugin();
            void removeResonanceAudioPlugin();
            #endif
        
        
        private:
        
            bool inited;    // wheter the FMOD context is fully inited or not
            int channels;   // the number of currently playing channels
            int samplerate; // the mixer engine sample rate
        
            FMOD::System *system;       // the internal FMOD context
            FMOD::ChannelGroup *master; // the master channel group
        
            // register plugin DSP(s)
            void registerDSPPlugins();
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Player
    // ---------------------------------------------------------------------------------------------

    class Player
    {
        public:
        
             Player(string id);
            ~Player();

            // Initializes within the given FMOD context
            void init(FMOD::System *system);

            // Loads a sound into memory or opens it for streaming
            bool load(string source, int mode = FMOD_LOOP_OFF, bool stream = false, bool autoplay = false);
            // Loads an audio buffer into memory or opens it for streaming
            bool loadFromBuffer(const char *buffer, int length, int mode = FMOD_LOOP_OFF, bool stream = false, bool autoplay = false);
            // Unload a sound and cleanup all resources
            bool release();
        
            // Start the playback of the currently loaded sound
            bool play();
            // Stop the playback of the sound
            bool stop();
            // Retrieves the playing state
            bool isPlaying();
            // Pause or resume the playback of the sound
            bool pause(bool paused);
            // Retrieves the paused state
            bool isPaused();

            // Sets the volume level
            bool setVolume(float volume);
            // Sets the mute state
            bool setMute(bool mute);
            // Sets the relative pitch / playback rate
            bool setPitch(float value, bool shift = false);
            // Sets the loop mode of the sound
            bool setMode(int mode);
            // Sets the left/right pan level
            bool setPan(float pan);
            // Sets the fade in of the sound at the beginning of its play
            bool setFadeIn(unsigned int ms, bool now = false);
            // Sets the fade out of the sound at the end of its play
            bool setFadeOut(unsigned int ms, bool now = false);
            // Sets the current playback position in milliseconds
            bool setPosition(unsigned int ms);
            // Sets the sound bank (DLS) for MIDI playback
            void setMidiSoundBank(const char *path);

        
            // Retrieves the player identifier
            string getId();
            // Retrieves the mute state
            bool getMute();
            // Retrieves the number of channels of the sound
            int getChannels();
            // Retrieves the sound length (default in milliseconds)
            unsigned int getLength(FMOD_TIMEUNIT unit = FMOD_TIMEUNIT_MS);
            // Retrieves the current playback position (default in milliseconds)
            unsigned int getPosition(FMOD_TIMEUNIT unit = FMOD_TIMEUNIT_MS);

            // Logs and stores the error message
            void error(FMOD_RESULT result, const char *file, int line = -1 );
            // Retrieves the lastest error message
            const char* getLastErrorMessage();

            // Channel ended event handler
            void onPlaybackEnded();
        
        
            #if CORDOVA_PLUGIN_RESONANCEAUDIO
            void createResonanceAudioDSP(int source_id);
            void addResonanceAudioDSP();
            void removeResonanceAudioDSP();
            void releaseResonanceAudioDSP();
            #endif

        
        private:
        
            string name;            // the player identifier

            bool muted;             // the muted state
            float volume;           // the user volume level
            float pan;              // the user pan value
            float pitch;            // the user pitch value
            bool pitchShift;        // whether the pitch shifter DSP is enabled or not
            bool loading;           // whether a source is currently loading or not
            unsigned int position;  // the user start position (in milliseconds)
        
            const char* lasterror;  // the last known error
        
            FMOD::System *system;   // the internal FMOD context
            FMOD::Sound *sound;     // the sample data container
            FMOD::Channel *channel; // the source of the audio signal
            FMOD::DSP* psDSP;       // the pitch shifter DSP
        
            Fade fadein;            // fade in effect properties
            Fade fadeout;           // fade out effect properties
        
            SoundInfo* infos;       // the current sound informations
            string dlsfile;         // the sound bank (DLS) for MIDI playback
        
            #if CORDOVA_PLUGIN_RESONANCEAUDIO
            FMOD::DSP *vraudioDSP;  // the ResonanceAudio DSP
            #endif
        
            bool createSound(const char *source, int length, int mode, bool stream, bool autoplay);
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Clock
    // ---------------------------------------------------------------------------------------------

    extern "C" F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL Clock_GetDSPDescription();

    FMOD_RESULT F_CALLBACK Clock_dspCreate         (FMOD_DSP_STATE *dsp_state);
    FMOD_RESULT F_CALLBACK Clock_dspRead           (FMOD_DSP_STATE *dsp_state, float *inbuffer, float *outbuffer, unsigned int length, int inchannels, int *outchannels);
    FMOD_RESULT F_CALLBACK Clock_dspShouldIProcess (FMOD_DSP_STATE *dsp_state, FMOD_BOOL inputsidle, unsigned int length, FMOD_CHANNELMASK inmask, int inchannels, FMOD_SPEAKERMODE speakermode);

    typedef enum CLOCK_TYPE
    {
        FMOD_CLOCK_TYPE_METRONOME = 1,
        FMOD_CLOCK_TYPE_SCHEDULER = 2
    }
    CLOCK_TYPE;

    typedef struct Clock
    {
        CLOCK_TYPE type;
        FMOD::ChannelGroup *mixer;
        void* target;
    }
    Clock;



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Metronome
    // ---------------------------------------------------------------------------------------------

    class Metronome
    {
        public:
            
             Metronome(string id);
            ~Metronome();
            
            // Initializes within the given FMOD context
            void init(FMOD::System *system);
            // Closes the metronome
            void close();
            
            // Starts the metronome
            bool start();
            // Stops the metronome
            bool stop();
            // Reset the metronome
            void reset();
            
            // Sets the beat period using BPM, samples or milliseconds (0 means no event)
            bool setBeatsPerMinute(int BPM);
            bool setBeatPeriodInSamples(unsigned long long samples);
            bool setBeatPeriodInMilliseconds(float ms);
            // Sets the DSP clock using samples
            bool setDSPClock(unsigned long long samples);
            // Sets the DSP clock using milliseconds
            bool setDSPClockInMilliseconds(float ms);
        
            // Retrieves the master DSP clock in samples
            unsigned long long getMasterDSPClock();
            // Retrieves the current DSP clock in samples
            unsigned long long getDSPClock();
            // Retrieves the current DSP clock in milliseconds
            float getDSPClockInMilliseconds();
            // Retrieves the current DSP latency (milliseconds)
            float getDSPCycleLatency();
            // Retrieves the expected DSP cycle latency (milliseconds)
            float getDSPCycleExpectedLatency();
        
            // DSP handler
            void process(unsigned long long samples);
        
        
            // allow multiple callback to the metronome
            // DSP Clock -> Metronome -> Schedulers
            void addCallback(string id, Scheduler *scheduler);
            void removeCallback(string id);
            
            unsigned long long getBeatPeriodInSamples();

            // work with the DSP clock
            void createDSP(CLOCK_TYPE type);
            void closeDSP();
            bool startDSP();
            bool stopDSP();
            void resetDSP();
        
        
        private:
            string name;                    // the metronome identifier

            bool running;                   // wheter the scheduler is currently running pr not
            int samplerate;                 // the mixer engine sample rate
            unsigned int blocksize;         // the mixer engine block size
    
            float cycle_t;                  // the execution time between two DSP cycles
            float cycle_e;                  // the expected time between two DSP cycles

            unsigned long long cycle_p;     // DSP cycle period
            unsigned long long cycle_i;     // DSP currentindex (relative to period)
            unsigned long long index;       // the metronome index (always incremental)
//            unsigned long long startat;     // clock to start the DSP in samples
//            unsigned long long pauseat;     // clock to resume the DSP in samples
            unsigned long long offset;      // DSP clock offset
            unsigned long long clock;       // current DSP clock in samples
            float milliseconds;             // current DSP clock in milliseconds
    
            FMOD::System *system;           // the internal FMOD context
            FMOD::ChannelGroup *master;     // the master channel group
            FMOD::DSP *dsp;                 // internal DSP to perform events on audio time
        
        
        map<string, Scheduler*> callbacks;
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Scheduler
    // ---------------------------------------------------------------------------------------------

    typedef enum SCHEDULER_EVENT_ACTION
    {
        FMOD_EVENT_START    = 1,
        FMOD_EVENT_PAUSE    = 2,
        FMOD_EVENT_RESUME   = 3,
        FMOD_EVENT_STOP     = 4,
        FMOD_EVENT_NONE     = 5
        
    } SCHEDULER_EVENT_ACTION;

    typedef enum SCHEDULER_EVENT_TYPE
    {
        FMOD_EVENT_TYPE_PLAYBACK = 1,
        FMOD_EVENT_TYPE_CALLBACK = 2,
        
    } SCHEDULER_EVENT_TYPE;

    typedef struct FMODSchedulerEvent
    {
        SCHEDULER_EVENT_TYPE type;
        string utag;
        vector<string> tags;
        unsigned long long clock;
        bool once;
        // FMOD_EVENT_TYPE_PLAYBACK only
        Player* target;
        SCHEDULER_EVENT_ACTION action;
        
        bool operator <(const FMODSchedulerEvent & event) const { return clock < event.clock; }
    } FMODSchedulerEvent;



    class Scheduler
    {
        public:
        
             Scheduler(string id);
            ~Scheduler();
        
            // Initializes within the given FMOD context
            void init(FMOD::System *system);
            // Closes and frees the scheduler
            void close();

            // Starts the scheduler
            bool start();
            // Stops the scheduler
            bool stop();
            // Pause or resume the scheduler
            void pause(bool pause);
            // Reset the scheduler clock
            void reset();
        
            // Sets the scheduler cycle period using samples or milliseconds (0 means no cycle)
            bool setPeriod(unsigned long long samples);
            bool setPeriodInMilliseconds(float ms);
            // Sets the DSP clock using samples
            // clear (or not) the previous settings
            // stops (or not) all running players if the clock of their associated event becomes obsolete
            bool setClock(unsigned long long samples, bool clear = false, bool stop = true);
            // Sets the DSP clock using milliseconds
            bool setClockInMilliseconds(float ms);
        
            // Schedule a new event
            void registerEvent(FMODSchedulerEvent event);
            void registerEvent(SCHEDULER_EVENT_TYPE type, string tags, unsigned long long clock, SCHEDULER_EVENT_ACTION action, Player* player, bool once = false, bool now = false);
            void registerEventInMilliseconds(SCHEDULER_EVENT_TYPE type, string tags, float ms, SCHEDULER_EVENT_ACTION action, Player* player, bool once = false, bool now = false);
            // Delete one or more specific event
            // (all events matching the given tag)
            bool unregisterEvent(string tag);
            // Deletes all recorded events
            void unregisterAllEvents();
        
            // Search the next event associated to the given clock
            // stops or not all running players if the clock of their associated event becomes obsolete
            void goToEvent(unsigned long long samples, bool stop = true);
        
            // Performs all recorded events
            // current DSP clock samples
            void process(unsigned long long index, unsigned long long samples);

        
            // metronome related methods -----------------------------------------------------------
        
            // Attachs a specific metronome to this scheduler
            bool setMetronome(Metronome *m);
            // Sets the beat period using BPM, samples or milliseconds (0 means no event)
            bool setBeatsPerMinute(int BPM);
            bool setBeatPeriodInSamples(unsigned long long samples);
            bool setBeatPeriodInMilliseconds(float ms);
            // Retrieves the current DSP clock in samples
            unsigned long long getDSPClock();
            // Retrieves the current DSP clock in milliseconds
            float getDSPClockInMilliseconds();
            // Retrieves the current DSP latency (milliseconds)
            float getDSPCycleLatency();
            // Retrieves the expected DSP cycle latency (milliseconds)
            float getDSPCycleExpectedLatency();
        
        
        
        private:
            
            unsigned long long period;      // DSP cycle period
        
            string name;                    // the scheduler identifier
            bool running;                   // wheter the scheduler is currently running or not
            bool paused;                    // wheter the scheduler is currently paused or not
            int samplerate;                 // the mixer engine sample rate
            unsigned long long startat;     // clock to start the DSP in samples
            unsigned long long pauseat;     // clock to resume the DSP in samples
            unsigned long long index;       // the scheduler index (relative to the metronome index)
            unsigned long long step;        // the scheduler step
            unsigned long long offset;      // DSP clock offset
            unsigned long long clock;       // current DSP clock in samples
            float milliseconds;             // current DSP clock in milliseconds
            bool sharedClock;               // whether the scheduler use an external metronome or not
        
            Metronome *metronome;                       // the internal DSP clock manager
            map<string, Player*> targets;               // the list of target players
            list<FMODSchedulerEvent> events;            // the list of events to process
            list<FMODSchedulerEvent>::iterator event;   // next event
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Input
    // ---------------------------------------------------------------------------------------------

    class Input : public AudioUnit
    {
        public:
        
             Input(string id);
            ~Input();
        
            // Initializes within the given FMOD context
            void init(FMOD::System *system);
            // Closes and frees an audio input and its resources
            void close();

            // Starts listening to the audio input
            bool start();
            // Starts listening to the audio input
            void stop();
        
            // Sets the number of channels of the audio input
            bool setInputChannels(int channels);
            // Sets the audio format of the audio input
            bool setAudioFormat(FMOD_SOUND_FORMAT format);
        
            bool isInited();
            // Retrieves the lastest error message
            const char* getLastErrorMessage();
        
            // delay playback until desired latency is reached
            // @Override
            void update();
        
        
        private:
        
            bool inited;                    // whether the device input is fully initialised
            const char* lasterror;          // the last known error
        
            FMOD::System *system;           // the internal FMOD context
            FMOD_CREATESOUNDEXINFO exinfo;  // options for creating the input sound
    };
         



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Recorder
    // ---------------------------------------------------------------------------------------------

    extern "C" F_EXPORT FMOD_DSP_DESCRIPTION* F_CALL Recorder_GetDSPDescription();

    FMOD_RESULT F_CALLBACK Recorder_dspCreate   (FMOD_DSP_STATE *dsp_state);
    FMOD_RESULT F_CALLBACK Recorder_dspRelease  (FMOD_DSP_STATE *dsp_state);
    FMOD_RESULT F_CALLBACK Recorder_dspProcess  (FMOD_DSP_STATE* dsp_state, unsigned int length, const FMOD_DSP_BUFFER_ARRAY* inbuffer, FMOD_DSP_BUFFER_ARRAY* outbuffer, FMOD_BOOL inidle, FMOD_DSP_PROCESS_OPERATION operation);

    typedef struct
    {
        string uuid;        // the associated recorder identifier
        string file;        // the path of the file where the audio data will be saved
        
        bool recording;     // whether the recording is started or not
        bool waitForSignal; // whether the leading silence should be considered or not
        unsigned int level; // the minimum audio level (in dB) from where the effective recording should start
        
        int channels;       // number of channels
        int sampleRate;     // input sample rate
        int format;         // PCM format
        

        #ifdef __ANDROID__
        // to avoid and/or compute buffer latency (Android only)
        int samples;                  // the number of recorded samples
        int buflatency;               // the buffer latency in samples
        unsigned int buflatencyMS;    // the buffer latency in milliseconds
        #endif
        
        // the record engine
        AudioFile<double> recorder;
        // the audio source to be recorded
        AudioUnit* source;
    }
    FMOD_DSP_RECORDER_DATA;

    class Recorder : public AudioUnit
    {
        public:
        
             Recorder(string id);
            ~Recorder();
        
            // Initializes within the given FMOD context
            void init(FMOD::System *system);
            // Closes and frees the recorder and its resources
            void close();
        
            // Starts the recording
            bool start(const char* filepath);
            // Stops the recording
            void stop();
        
            // Sets the audio signal to be recorded
            bool setInput(AudioUnit* signal);
            // Sets the sample rate for the audio file
            bool setSampleRate(int samplerate);
            // Sets the audio format for the audio file
            bool setAudioFormat(FMOD_SOUND_FORMAT format);
            // Start the effective recording only when the sound is no longer considered silent
            // i.e. from a minimum sound level.
            bool ignoreLeadingSilence(bool enable, unsigned int level);
       
            // Retrieves the lastest error message
            const char* getLastErrorMessage();


        private:
        
            const char* lasterror;  // the last known error
        
            FMOD::System *system;   // the internal FMOD context
            FMOD::DSP *dsp;         // internal DSP to perform recording
        
            FMOD_DSP_RECORDER_DATA data;    // misc data to manage the recording process
    };



    // ---------------------------------------------------------------------------------------------
        #pragma mark FMOD Analyser
    // ---------------------------------------------------------------------------------------------

    typedef struct
    {
        float centroid;     // the centroid spectral
    }
    FMOD_DSP_ANALYSER_FFT_DATA;

    typedef struct
    {
        float rms[2];       // peak level per channel
        float peak[2];      // RMS level per channel
    }
    FMOD_DSP_ANALYSER_METERING_DATA;

    typedef struct
    {
        FMOD_DSP_ANALYSER_FFT_DATA fft;
        FMOD_DSP_ANALYSER_METERING_DATA metering;
    }
    FMOD_DSP_ANALYSER_DATA;

    class Analyser : public AudioUnit
    {
        public:
        
             Analyser(string id);
            ~Analyser();
        
            // Initializes within the given FMOD context
            void init(FMOD::System *system);
            // Closes and frees an audio analyser and its resources
            void close();

            // Starts the audio analysis
            bool start();
            // Stops the audio analysis
            void stop();
            // Updates the audio analysis
            // @Override
            void update();
        
            // Sets the audio signal to be analyzed
            bool setInput(AudioUnit* signal);
            // Sets the interval between two analyses in milliseconds
            // 0 means on each DSP cycle
            void setPeriodInMilliseconds(float ms);
        
            // Enables or disables specific analyses
            void setFFTAnalysis(bool enable);
            void setSignalMetering(bool enable);
        
            // Retrieves the lastest error message
            const char* getLastErrorMessage();
        
        
        private:
        
            bool doFFT;                 // whether or not the FFT analysis is enabled
            bool doMetering;            // whether or not the signal metering is enabled
            bool running;               // whether or not the analysis is started
        
            const char* lasterror;      // the last known error
        
            FMOD::System *system;       // the internal FMOD context
            FMOD::DSP *fft;             // internal DSP to perform FFT analysis
        
            AudioUnit* source;          // the audio source to be analysed
        
            uint64_t last_ms;           // the last stored time (in milliseconds)
            float period;               // the time interval (in milliseconds) between two analyses
    };
}
