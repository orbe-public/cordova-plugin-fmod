// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <chrono>
#include <thread>

#include "FMODPlugin.h"

// external static DSP plugin
#if CORDOVA_PLUGIN_RESONANCEAUDIO
#include "ResonanceAudioDSP.h"
#endif



// Channel events handler
FMOD_RESULT F_CALLBACK channelCallback(FMOD_CHANNELCONTROL *channelControl,
                                       FMOD_CHANNELCONTROL_TYPE controltype,
                                       FMOD_CHANNELCONTROL_CALLBACK_TYPE callbacktype,
                                       void *commanddata1,
                                       void *commanddata2)
{
    if (callbacktype == FMOD_CHANNELCONTROL_CALLBACK_END)
    {
        FMOD::Channel *channel = (FMOD::Channel *)channelControl;
        void* userdata;
        
        FMOD_RESULT result = channel->getUserData(&userdata);
        FMOD_ERRCHECK(result);

        string uuid = *static_cast<string*>(userdata);
        FMOD_channel_event(uuid.c_str());
    }

    return FMOD_OK;
}





FMODPlugin::Player::Player(string id)
{
    name = id;
    muted = false;
    volume = 1.0;
    position = 0;
    pitch = 1.0;
    pan = 0.0;
    pitchShift = false;
    loading = false;
    
    system = NULL;
    sound = NULL;
    channel = NULL;
    psDSP = NULL;
    infos = NULL;
    
    #if CORDOVA_PLUGIN_RESONANCEAUDIO
    vraudioDSP = NULL;
    #endif
    
    init(NULL);
}

FMODPlugin::Player::~Player()
{
    
    #if CORDOVA_PLUGIN_RESONANCEAUDIO
    releaseResonanceAudioDSP();
    #endif
    
    release();
}



void FMODPlugin::Player::init(FMOD::System *sys)
{
    system = sys;
    if (!system) lasterror = "FMOD system is not initialized";
}

// TODO: load from URL
bool FMODPlugin::Player::load(string source, int mode, bool stream, bool autoplay)
{
    const char* path = FMOD_media_path(source.c_str());
    return createSound(path, 0, mode, stream, autoplay);
}

bool FMODPlugin::Player::loadFromBuffer(const char *buffer, int length, int mode, bool stream, bool autoplay)
{
    return createSound(buffer, length, FMOD_OPENMEMORY | mode, stream, autoplay);
}

bool FMODPlugin::Player::createSound(const char *source, int length, int mode, bool stream, bool autoplay)
{
    if (!system) return false;
    
    // To avoid issues with asynchrony, simultaneous loading is not allowed.
    if (loading) {
        lasterror = "A source is already being loaded";
        return false;
    }
    loading = true;
    
    FMOD_RESULT result = FMOD_OK;
    
    // free up previous resources
    if (sound) release();
    
    FMOD_CREATESOUNDEXINFO exinfo;
    memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
    exinfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
    exinfo.dlsname = dlsfile.c_str();
    exinfo.length = length;
    
    // create the sound container
    //result = system->createStream(source, mode, &exinfo, &sound);
    if (stream) result = system->createStream(source, mode, &exinfo, &sound);
    else result = system->createSound(source, mode, &exinfo, &sound);
    error(result, __FILE__, __LINE__);
    
    // retrieve sound informations
    if (sound) {
        infos = new SoundInfo();
        result = sound->getDefaults(&infos->samplerate, 0);
        error(result, __FILE__, __LINE__);
        result = sound->getFormat(&infos->type, &infos->format, &infos->channels, &infos->bits);
        error(result, __FILE__, __LINE__);
        result = sound->getLength(&infos->samples, FMOD_TIMEUNIT_PCM);
        error(result, __FILE__, __LINE__);
        result = sound->getLength(&infos->duration, FMOD_TIMEUNIT_MS);
        error(result, __FILE__, __LINE__);
        if (length) Log_d("Audio buffer loaded, size: %i", length);
        else Log_d("File loaded: '%s'", source);
        Log_d(
              "Sound information, type: %i, duration: %ims, sample rate: %iHz, audio channels: %i, bits per sample: %i, format: %i",
              infos->type,
              infos->duration,
              (int)infos->samplerate,
              infos->channels,
              infos->bits,
              infos->format
        );
    }
    else autoplay = false;
    
    loading = false;

    // auto start if needed
    if (autoplay && play()) result = FMOD_OK;
    
    return result == FMOD_OK;
}

bool FMODPlugin::Player::release()
{
    FMOD_RESULT result = FMOD_OK;
    stop();
    
    if (sound) {
        result = sound->release();
        error(result, __FILE__, __LINE__);
        sound = NULL;
        infos = NULL;
    }
    return result == FMOD_OK;
}

bool FMODPlugin::Player::play()
{
    if (!system) return false;
    
    FMOD_RESULT result = FMOD_OK;
    
    // stop current playback first
    stop();
    
    // sound is required to continue
    if (!sound) {
        lasterror = "Source is missing or not loaded";
        return false;
    }
    
    result = system->playSound(sound, 0, true, &channel);
    error(result, __FILE__, __LINE__);

    result = channel->setUserData(&name);
    error(result, __FILE__, __LINE__);

    result = channel->setCallback(&channelCallback);
    error(result, __FILE__, __LINE__);
    
    // clean effects
    fadein.clear();
    fadeout.clear();
    
    // adjust with user parameeters
    setPan(pan);
    setMute(muted);
    setVolume(volume);
    setPosition(position);
    setPitch(pitch, pitchShift);
    setFadeIn(fadein.length, true);
    setFadeOut(fadeout.length);
    
    #if CORDOVA_PLUGIN_RESONANCEAUDIO
    addResonanceAudioDSP();
    #endif

    result = channel->setPaused(false);
    error(result, __FILE__, __LINE__);
        
    return result == FMOD_OK;
}

bool FMODPlugin::Player::stop()
{
    if (!isPlaying()) return true;
    
    // waits for the fade out then stops
    if (fadeout.length) {
        bool result = setFadeOut(fadeout.length, true);
        thread t([this]() {
            this_thread::sleep_for(chrono::milliseconds(fadeout.length));
            if (channel) channel->stop();
        });
        t.detach();
        
        return result;
    }
    
    // stop now
    if (channel) {
        FMOD_RESULT result = channel->stop();
        error(result, __FILE__, __LINE__);
        return result == FMOD_OK;
    }
        
    return true;
}

bool FMODPlugin::Player::isPlaying()
{
    bool isplaying = false;
    if (channel) channel->isPlaying(&isplaying);
    return isplaying;
}

bool FMODPlugin::Player::pause(bool paused)
{
    if (!isPlaying()) return false;
    
    FMOD_RESULT result = channel->setPaused(paused);
    error(result, __FILE__, __LINE__);
        
    return result == FMOD_OK;
}

bool FMODPlugin::Player::isPaused()
{
    bool paused = false;
    if (channel) channel->getPaused(&paused);
    return paused;
}



bool FMODPlugin::Player::setMode(int mode)
{
    if (!sound) return false;
    
    FMOD_RESULT result = sound->setMode(mode);
    error(result, __FILE__, __LINE__);
    if (isPlaying()) {
        result = channel->setMode(mode);
        error(result, __FILE__, __LINE__);
    }
    
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setFadeIn(unsigned int ms, bool now)
{
    FMOD_RESULT result = FMOD_OK;
    
    // set the fade in only when requested
    if (ms && channel && infos && now ) {
        unsigned long long clock;
        
        result = channel->getDSPClock(NULL, &clock);
        error(result, __FILE__, __LINE__);
        
        fadein.samples = MS_TO_SAMPLES(ms, infos->samplerate);
        fadein.start = clock;
        fadein.end = clock + fadein.samples;
        
        result = channel->addFadePoint(fadein.start, 0.0f);
        error(result, __FILE__, __LINE__);
        result = channel->addFadePoint(fadein.end, 1.0f);
        error(result, __FILE__, __LINE__);
    }

    fadein.length = ms;
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setFadeOut(unsigned int ms, bool now)
{
    FMOD_RESULT result = FMOD_OK;

    if (ms && channel && infos) {
        
        // get the current playback loop mode
        FMOD_MODE mode;
        channel->getMode(&mode);
        error(result, __FILE__, __LINE__);
        bool loop = (mode & FMOD_LOOP_NORMAL) || (mode & FMOD_LOOP_BIDI);
        
        // do nothing if the fade out is already started or loop mode enabled
        // except if needed
        if (!loop || now) {
    
            // get the current DSP clock
            unsigned long long clock;
            channel->getDSPClock(NULL, &clock);
            error(result, __FILE__, __LINE__);
            
            if (!fadeout.start || (now && clock < fadeout.start)) {
                
                // first, cleans fade out points (typically stop before the end)
                if (clock < fadeout.start) {
                    result = channel->removeFadePoints(fadein.end+1, fadeout.end);
                    error(result, __FILE__, __LINE__);
                }
                
                unsigned long long pos = MS_TO_SAMPLES(position, infos->samplerate);
                fadeout.samples = MS_TO_SAMPLES(ms, infos->samplerate);
                if ((pos+fadeout.samples > infos->samples) && !loop) {
                    lasterror = "not enough samples to set up a fade out";
                    result = FMOD_RESULT_FORCEINT;
                    Log_d(lasterror);
                }
                else {
                    fadeout.start = clock + (now ? 0 : infos->samples - fadeout.samples);
                    fadeout.end = clock + (now ? fadeout.samples : infos->samples);
                    
                    result = channel->addFadePoint(fadeout.start, 1.0f);
                    error(result, __FILE__, __LINE__);
                    result = channel->addFadePoint(fadeout.end, 0.0f);
                    error(result, __FILE__, __LINE__);
                }
            }
            else if (now) {
                lasterror = "the fade out already started";
                result = FMOD_RESULT_FORCEINT;
                Log_d(lasterror);
            }
        }
        else if (loop) {
            lasterror = "fade out is not allowed with a looped playback";
            result = FMOD_RESULT_FORCEINT;
            Log_d(lasterror);
        }
    }

    fadeout.length = ms;
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setPosition(unsigned int value)
{
    FMOD_RESULT result = FMOD_OK;

    if (channel) {
        result = channel->setPosition(value, FMOD_TIMEUNIT_MS);
        error(result, __FILE__, __LINE__);
    }
    
    position = value;
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setVolume(float value)
{
    FMOD_RESULT result = FMOD_OK;
    if (channel) {
        result = channel->setVolume(value);
        error(result, __FILE__, __LINE__);
    }
    
    volume = value;
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setPitch(float value, bool shift)
{
    FMOD_RESULT result = FMOD_OK;
    
    if (channel) {
        result = channel->setPitch(value);
        error(result, __FILE__, __LINE__);
    
        if (shift) {
            if (value!=1.0) {
                if (psDSP==NULL) {
                    result = system->createDSPByType(FMOD_DSP_TYPE_PITCHSHIFT, &psDSP);
                    error(result, __FILE__, __LINE__);
                    result = channel->addDSP(0, psDSP);
                    error(result, __FILE__, __LINE__);
                }
            }
            else if (psDSP) {
                channel->removeDSP(psDSP);
                psDSP = NULL;
            }
        }
    }
    
    pitch = value;
    pitchShift = shift;
    
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setPan(float value)
{
    FMOD_RESULT result = FMOD_OK;
    if (channel) {
        result = channel->setPan(value);
        error(result, __FILE__, __LINE__);
    }
    
    pan = value;
    return result == FMOD_OK;
}

bool FMODPlugin::Player::setMute(bool mute)
{
    FMOD_RESULT result = FMOD_OK;
    if (channel) {
        result = channel->setMute(mute);
        error(result, __FILE__, __LINE__);
    }
    
    muted = mute;
    return result == FMOD_OK;
}

void FMODPlugin::Player::setMidiSoundBank(const char *path)
{
    dlsfile = FMOD_media_path(path);
}



string FMODPlugin::Player::getId()
{
    return name;
}

bool FMODPlugin::Player::getMute()
{
    bool muted = false;
    if (channel) channel->getMute(&muted);
    return muted;
}

int FMODPlugin::Player::getChannels()
{
    int channels = 0;
    if (sound) {
        if (infos) channels = infos->channels;
        else sound->getFormat(NULL,NULL, &channels, NULL);
    }
    return channels;
}

unsigned int FMODPlugin::Player::getLength(FMOD_TIMEUNIT unit)
{
    unsigned int length = 0;
    if (sound) sound->getLength(&length, unit);
    return length;
}

unsigned int FMODPlugin::Player::getPosition(FMOD_TIMEUNIT unit)
{
    unsigned int position = 0;
    if (channel) channel->getPosition(&position, unit);
    return position;
}



void FMODPlugin::Player::error(FMOD_RESULT result, const char *file, int line)
{
    if (result != FMOD_OK) {
        lasterror = FMOD_ErrorString(result);
        #if DEBUG
        FMOD_log_error(result, file, line);
        #endif
    }
}

const char* FMODPlugin::Player::getLastErrorMessage()
{
    return lasterror;
}



void FMODPlugin::Player::onPlaybackEnded()
{
    #if CORDOVA_PLUGIN_RESONANCEAUDIO
    removeResonanceAudioDSP();
    #endif
    
    channel = NULL;
}





// external Resonance Audio plugin
#if CORDOVA_PLUGIN_RESONANCEAUDIO
void FMODPlugin::Player::createResonanceAudioDSP(int source_id)
{
    if (vraudioDSP == nullptr) {
        ResonanceAudioPlugin::DSP::fmod::SourceState *data = new ResonanceAudioPlugin::DSP::fmod::SourceState(source_id);
        vraudioDSP = ResonanceAudioPlugin::DSP::fmod::createSourceDSP(system, data);
    }
    else Log_w("the ResonanceAudio DSP is already created for another source.");
}
void FMODPlugin::Player::addResonanceAudioDSP()
{
    if (vraudioDSP != nullptr) {
        ResonanceAudioPlugin::DSP::fmod::addSourceDSP(channel, vraudioDSP);
    }
}
void FMODPlugin::Player::removeResonanceAudioDSP()
{
    if (vraudioDSP != nullptr) {
        ResonanceAudioPlugin::DSP::fmod::removeSourceDSP(channel, vraudioDSP);
    }
}
void FMODPlugin::Player::releaseResonanceAudioDSP()
{
    if (vraudioDSP != nullptr) {
        ResonanceAudioPlugin::DSP::fmod::releaseSourceDSP(vraudioDSP);
        vraudioDSP = nullptr;
    }
    else Log_w("the ResonanceAudio DSP is already released.");
}
#endif
