// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#include "FMODPlugin.h"



@interface CDVFMODMetronome : CDVPlugin
{}

// Get the metronome identified by the specified ID
+ (FMODPlugin::Metronome*)getMetronome:(NSString*)identifier;

// Creates and initializes a new metronome
- (void)init:(CDVInvokedUrlCommand*)command;
// Closes the metronome
- (void)close:(CDVInvokedUrlCommand*)command;
// Starts the metronome
- (void)start:(CDVInvokedUrlCommand*)command;
// Stops the metronome
- (void)stop:(CDVInvokedUrlCommand*)command;
// Reset the metronome clock
- (void)reset:(CDVInvokedUrlCommand*)command;

// Sets the beats frequency in BPM
- (void)setBeatsPerMinute:(CDVInvokedUrlCommand*)command;
//  Sets the beat period in samples
- (void)setBeatPeriodInSamples:(CDVInvokedUrlCommand*)command;
// Sets the beat period in milliseconds
- (void)setBeatPeriodInMilliseconds:(CDVInvokedUrlCommand*)command;
// Sets the DSP clock to the specified milliseconds
- (void)setDSPClockInMilliseconds:(CDVInvokedUrlCommand*)command;
// Sets the DSP clock to the given number of samples
- (void)setDSPClock:(CDVInvokedUrlCommand*)command;

// Retrieves information about the DSP clock and DSP cycle usage
- (void)getDSPUsage:(CDVInvokedUrlCommand*)command;

// Register the metronome event callback
- (void)registerEventCallback:(CDVInvokedUrlCommand*)command;
+ (void)registerEventCallback:(CDVInvokedUrlCommand*)command;
// Unregister the metronome event callback
- (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command;
+ (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command;

@end
