// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#include "FMODPlugin.h"



@interface CDVFMODRecorder : CDVPlugin
{}

// Creates and initializes a new recorder
- (void)init:(CDVInvokedUrlCommand*)command;
// Closes and frees the recorder and its resources
- (void)close:(CDVInvokedUrlCommand*)command;
// Starts the recording
- (void)start:(CDVInvokedUrlCommand*)command;
// Stops the recording
- (void)stop:(CDVInvokedUrlCommand*)command;

// Sets the audio signal to be recorded
- (void)setInput:(CDVInvokedUrlCommand*)command;
// Sets the sample rate for the audio file
- (void)setSampleRate:(CDVInvokedUrlCommand*)command;
// Sets the audio format for the audio file
- (void)setAudioFormat:(CDVInvokedUrlCommand*)command;

// Allows you to start the effective recording only when the sound is no longer considered silent
- (void)ignoreLeadingSilence:(CDVInvokedUrlCommand*)command;

// Register the recorder ended callback
- (void)registerEndedCallback:(CDVInvokedUrlCommand*)command;
// Unregister the recorder ended callback
- (void)unregisterEndedCallback:(CDVInvokedUrlCommand*)command;

@end
