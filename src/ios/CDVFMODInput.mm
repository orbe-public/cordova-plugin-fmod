// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODInput.h"
#import "CDVFMOD.h"




@interface CDVFMODInput()
@end

@implementation CDVFMODInput
{
    @private map<string, FMODPlugin::Input*> inputs;
    
    FMODPlugin::Context *context;
}



- (bool)inputExists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || inputs.find(uuid) == inputs.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native input not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    
    NSLog_d("FMOD Input initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:inputs) it.second->close();
    //context->close();
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];

    if (!uuid.empty()) {
        self->inputs[uuid] = context->createInput(uuid);
        if (self->inputs[uuid]->isInited())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", self->inputs[uuid]->getLastErrorMessage()]];
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->inputs.find(uuid);
        if (it != self->inputs.end()) {
            self->inputs[uuid]->~Input();
            self->inputs.erase(it);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self inputExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (inputs[uuid]->start())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", inputs[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self inputExists:uuid fromUrlCommand:command];
    
    if (exists) {
        inputs[uuid]->stop();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setInputChannels:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self inputExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *channels = [args valueForKey:@"channels"];
        
        if (channels!=nil) {
            if (inputs[uuid]->setInputChannels([channels intValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing channels value"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setAudioFormat:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self inputExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *format = [args valueForKey:@"format"];
        
        if (format!=nil) {
            FMOD_SOUND_FORMAT sf = static_cast<FMOD_SOUND_FORMAT>([format intValue]);
            if (inputs[uuid]->setAudioFormat(sf))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing format value"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
