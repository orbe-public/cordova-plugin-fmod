// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import <AVFoundation/AVAudioSession.h>
#import "CDVFMOD.h"




static CDVFMOD *fmodplugin;

@interface CDVFMOD()
@end

@implementation CDVFMOD
{
    FMODPlugin::Context *context;
    CDVInvokedUrlCommand *onAudioInterruptCallback;
    
    @private NSThread *thread;
}



+ (FMODPlugin::Context*)initFMODSystem
{
    static dispatch_once_t queue;
    static FMODPlugin::Context *context = nil;

    dispatch_once(&queue, ^{
        
        // TODO: allow user to set his own audio configuration
        double samplerate = 48000;
        int blocksize = 512;
        
        // init audio context
        FMOD_AVSession_Init(samplerate, blocksize);
        // create FMOD system
        context = new FMODPlugin::Context(samplerate);
        // register audio interuptions handler
        FMODAudioContext = context;
    });

    return context;
}

+ (string)getUUIDFromUrlCommand:(CDVInvokedUrlCommand*)command fromClass:(CDVPlugin*)plugin
{
    NSDictionary *arg = [command argumentAtIndex:0 withDefault:nil andClass:[NSDictionary class]];
    NSString *identifier = arg==nil ? [command argumentAtIndex:0] : [arg valueForKey:@"uuid"];

    if (identifier==nil) {
        NSString *name = [[command className] stringByReplacingOccurrencesOfString:@"FMOD" withString:@""];
        NSString *message = [NSString stringWithFormat:@"Missing or invalid native %@ identifier", [name lowercaseString]];
        [plugin.commandDelegate sendPluginResult:[CDVPluginResult
                                                resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:message]
                                      callbackId:command.callbackId];
        return "";
    }
    
    return [identifier UTF8String];
}



- (void)pluginInitialize
{
    // register this instance
    fmodplugin = self;
    
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    
    thread = [[NSThread alloc] initWithTarget:self selector:@selector(update) object:nil];
    [thread start];
    

    NSLog(@"FMOD version (%02x): %s", FMOD_VERSION, context->getVersionNumber());
    NSLog_d("FMOD initialized");
}

- (void)update
{
    @autoreleasepool {
        NSTimeInterval freq = (50 / 1000.0f);
        while(![thread isCancelled]) {
            [NSThread sleepForTimeInterval:freq];
            context->update();
        }
    }
}

- (void)onAppTerminate
{
//    // cleanup on app exit
//    context->close();
}



// -------------------------------------------------------------------------------------------------
// Getters
// -------------------------------------------------------------------------------------------------

- (void)getVersion:(CDVInvokedUrlCommand*)command
{
    // 0xaaaabbcc -> aaaa = product version, bb = major version, cc = minor version
    unsigned int code = context->getVersionCode();
    
    int product = (code >> 16) & 0xFFFF;
    int major = (code >> 8) & 0xFF;
    int minor = code & 0xFF;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    NSDictionary *result = @{
        @"number": [NSString stringWithFormat:@"%x.%02x.%02x", product, major, minor],
        @"code": [formatter numberFromString:[NSString stringWithFormat:@"%x", code]],
        @"product": [formatter numberFromString:[NSString stringWithFormat:@"%x", product]],
        @"major": [formatter numberFromString:[NSString stringWithFormat:@"%x", major]],
        @"minor": [formatter numberFromString:[NSString stringWithFormat:@"%x", minor]]
    };
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult
                                            resultWithStatus:CDVCommandStatus_OK
                                            messageAsDictionary:result]
                                callbackId:command.callbackId];
}

- (void)getChannelsPlaying:(CDVInvokedUrlCommand*)command
{
    int channels = context->getChannelsPlaying();
    [self.commandDelegate sendPluginResult:[CDVPluginResult
                                            resultWithStatus:CDVCommandStatus_OK
                                            messageAsInt:channels]
                                callbackId:command.callbackId];
}

- (void)getCPUUsage:(CDVInvokedUrlCommand*)command
{
    FMOD_CPU_USAGE usage  = context->getCPUUsage();
    NSDictionary *result = @{
        @"dsp": [NSNumber numberWithFloat: usage.dsp],
        @"stream": [NSNumber numberWithFloat: usage.stream],
        @"geometry": [NSNumber numberWithFloat: usage.geometry],
        @"update": [NSNumber numberWithFloat: usage.update],
        @"convolution1": [NSNumber numberWithFloat: usage.convolution1],
        @"convolution2": [NSNumber numberWithFloat: usage.convolution2]
    };
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult
                                            resultWithStatus:CDVCommandStatus_OK
                                            messageAsDictionary:result]
                                callbackId:command.callbackId];
}

- (void)getEllapsedTime:(CDVInvokedUrlCommand*)command
{
    unsigned long long millis = context->getEllapsedTime();
    [self.commandDelegate sendPluginResult:[CDVPluginResult
                                            resultWithStatus:CDVCommandStatus_OK
                                            messageAsNSUInteger:(NSUInteger)millis]
                                callbackId:command.callbackId];
}



// -------------------------------------------------------------------------------------------------
// Events
// -------------------------------------------------------------------------------------------------


- (void)suspendAudioOutput:(CDVInvokedUrlCommand*)command
{
    bool suspended = FMOD_AudioInterruptCallback(true);
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:suspended ? CDVCommandStatus_OK : CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)resumeAudioOutput:(CDVInvokedUrlCommand*)command
{
    bool resumed = FMOD_AudioInterruptCallback(false);
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:resumed ? CDVCommandStatus_OK : CDVCommandStatus_ERROR];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)registerAudioInterruptCallback:(CDVInvokedUrlCommand*)command
{
    self->onAudioInterruptCallback = command;
}





// -------------------------------------------------------------------------------------------------
// FMODPlugin methods implementation
// -------------------------------------------------------------------------------------------------

const char* FMOD_media_path(const char *filename)
{
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%s", filename]];
    
    // local or distant URL
    if ([URL scheme] != nil) {
        // for local files (i.e. file://), use the system path instead
        if ([[URL scheme] isEqual:@"file"]) return [[NSString stringWithFormat:@"%@", [URL path]] UTF8String];
        return filename;
    }
    
    // absolute file path
    if (filename[0] == '/') return filename;
    
    // relative to bundle not DocumentRoot (i.e www)
    string str(filename);
    if (str.find("www/") == 0) str.erase(0, 4);
    
    return [[NSString stringWithFormat:@"%@/www/%s", [[NSBundle mainBundle] resourcePath], str.c_str()] UTF8String];
}

const char* FMOD_tempory_path(const char *filename)
{
    // do not prepend the tempory directory for absolute file paths
    if (filename[0] == '/') return filename;
    if (string(filename).find("file://") == 0) return filename;
    
    NSString *path = [NSString stringWithFormat:@"%@%s", NSTemporaryDirectory(), filename];
    return [path UTF8String];
}

bool FMOD_mkdir(const char *filepath)
{
    NSString *path = [NSString stringWithFormat:@"%s", filepath];
    NSString *dir  = [path stringByDeletingLastPathComponent];
    
    NSError *error = nil;
    NSFileManager *manager = [NSFileManager defaultManager];
    if (![manager createDirectoryAtPath:dir withIntermediateDirectories:YES attributes: nil error:&error]) {
         NSLog(@"Failed to create directory \"%@\": %@", dir, [error localizedDescription]);
    }
    
    return error == nil;
}



// -------------------------------------------------------------------------------------------------
// Loggers
// -------------------------------------------------------------------------------------------------

void NSLog_d(const char *message)
{
    #if DEBUG
    NSLog(@"[FMOD plugin] %s", message);
    #endif
}

void NSLog_w(const char *message)
{
    NSLog(@"[FMOD plugin] warning: %s", message);
}

void NSLog_e(const char *message)
{
    NSLog(@"[FMOD plugin] error: %s", message);
}



// -------------------------------------------------------------------------------------------------
// Audio session
// -------------------------------------------------------------------------------------------------

// audio events handler
FMODPlugin::Context *FMODAudioContext = 0;
bool FMOD_AudioInterruptCallback(bool began)
{
    bool result = false;
    if (FMODAudioContext) {
        result = began ? FMODAudioContext->suspend() : FMODAudioContext->resume();
    }
    
    // send to plugin's handlers
    if (fmodplugin->onAudioInterruptCallback) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                            messageAsString:began ? @"began" : @"ended"];
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodplugin.commandDelegate sendPluginResult:pluginResult callbackId:fmodplugin->onAudioInterruptCallback.callbackId];
    }
    
    return result;
}

void FMOD_AVSession_Init(double samplerate, int blocksize)
{
    long channels = 2;
    BOOL success = false;
    BOOL isHeadsetConnected = false;
    

    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    // checks if an headset is connected
    AVAudioSessionRouteDescription* route = [session currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
      if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones] ||
          [[desc portType] isEqualToString:AVAudioSessionPortBluetoothHFP] ||
          [[desc portType] isEqualToString:AVAudioSessionPortBluetoothA2DP] ||
          [[desc portType] isEqualToString:AVAudioSessionPortBluetoothLE]) {
          isHeadsetConnected = YES;
      }
    }

    // make our category 'playback and record' with Bluetooth option to satisfy all context
    success = [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetoothA2DP error:nil];
    assert(success);
    
    // because we use the PlayAndRecord category, force the audio output to the built-in speaker
    // only if headset are not connected
    if (!isHeadsetConnected)
    {
        success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
        assert(success);
    }
    
    
    
    // set our preferred rate and activate the session to test it
    success = [session setPreferredSampleRate:samplerate error:nil];
    assert(success);
    success = [session setActive:TRUE error:nil];
    assert(success);
    
    // query the actual supported rate and max channels
    samplerate = [session sampleRate];
    channels = [session respondsToSelector:@selector(maximumOutputNumberOfChannels)] ? [session maximumOutputNumberOfChannels] : 2;
    
    // deactivate the session so we can change parameters without route changes each time
    success = [session setActive:FALSE error:nil];
    assert(success);
    
    // set the duration and channels based on known supported values
    success = [session setPreferredIOBufferDuration:blocksize/samplerate error:nil];
    assert(success);
    if ([session respondsToSelector:@selector(setPreferredOutputNumberOfChannels:error:)])
    {
        success = [session setPreferredOutputNumberOfChannels:channels error:nil];
        assert(success);
    }
    
    
    
    // set up some observers for various notifications
    // receives an interruption notification
    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionInterruptionNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        bool began = [[notification.userInfo valueForKey:AVAudioSessionInterruptionTypeKey] intValue] == AVAudioSessionInterruptionTypeBegan;
        NSLog_w([[NSString stringWithFormat:@"interruption %s", began ? "began" : "ended"] UTF8String]);
        
        if (!began) [[AVAudioSession sharedInstance] setActive:TRUE error:nil];
        FMOD_AudioInterruptCallback(began);
    }];
    
    // when optional secondary audio muting should begin or end
    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionSilenceSecondaryAudioHintNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        bool began = [[notification.userInfo valueForKey:AVAudioSessionSilenceSecondaryAudioHintTypeKey] intValue] == AVAudioSessionSilenceSecondaryAudioHintTypeBegin;
        NSLog_w([[NSString stringWithFormat:@"silence secondary audio %s", began ? "began" : "ended"] UTF8String]);
    }];
    
    
    // get more information about the route change
    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionRouteChangeNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        NSNumber *key = [[notification userInfo] valueForKey:AVAudioSessionRouteChangeReasonKey];
        AVAudioSessionPortDescription *previousRoute = [[[notification userInfo] valueForKey:AVAudioSessionRouteChangePreviousRouteKey] outputs][0];
        AVAudioSessionPortDescription *currentRoute = [[[AVAudioSession sharedInstance] currentRoute] outputs][0];

        const char *reason = NULL;
        switch ([key intValue])
        {
            case AVAudioSessionRouteChangeReasonNewDeviceAvailable:         reason = "new device available";              break;
            case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:       reason = "old device unavailable";            break;
            case AVAudioSessionRouteChangeReasonCategoryChange:             reason = "category change";                   break;
            case AVAudioSessionRouteChangeReasonOverride:                   reason = "override";                          break;
            case AVAudioSessionRouteChangeReasonWakeFromSleep:              reason = "wake from sleep";                   break;
            case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory: reason = "no suitable route for category";    break;
            case AVAudioSessionRouteChangeReasonRouteConfigurationChange:   reason = "configuration change";              break;
            default:                                                        reason = "unknown";
        }

        NSLog_w([
            [NSString stringWithFormat:@"output route has changed from %dch %@ to %dch %@ due to '%s'",
             (int)[[previousRoute channels] count], [previousRoute portName],
             (int)[[currentRoute channels] count], [currentRoute portName], reason]
            UTF8String]
        );
        
        // headphones have been plugged in
        if ([key intValue]==AVAudioSessionRouteChangeReasonNewDeviceAvailable)
        {
            BOOL success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
            assert(success);
            NSLog_d("route audio to the default output");
        }
        
        // headphones have been unplugged
        if ([key intValue]==AVAudioSessionRouteChangeReasonOldDeviceUnavailable)
        {
            // FMOD_AudioInterruptCallback(true);
            BOOL success = [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
            assert(success);
            NSLog_d("route audio output to built-in speaker");
        }
    }];
    
    
    // media services notification
    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionMediaServicesWereLostNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        NSLog_d("media services were lost");
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:AVAudioSessionMediaServicesWereResetNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        NSLog_d("media services were reset");
    }];
    
    
    // app events notification
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        //NSLog_d("application did become active");
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        //NSLog_d("application will resign active");
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        //NSLog_d("application did enter background");
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification object:nil queue:nil usingBlock:^(NSNotification *notification)
    {
        //NSLog_d("application will enter foreground");
    }];
    
    
    
    // activate the audio session
    success = [session setActive:TRUE error:nil];
    assert(success);

    NSLog_d("FMOD audio session inited");
}

@end
