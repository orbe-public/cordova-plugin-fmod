// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODScheduler.h"
#import "CDVFMODPlayer.h"
#import "CDVFMODMetronome.h"
#import "CDVFMOD.h"



static CDVFMODScheduler *fmodscheduler;

@interface CDVFMODScheduler()
@end

@implementation CDVFMODScheduler
{
    @private map<string, FMODPlugin::Scheduler*> schedulers;
    @private map<string, CDVInvokedUrlCommand*> stepsCallbacks;
    @private map<string, CDVInvokedUrlCommand*> eventCallbacks;
    
    FMODPlugin::Context *context;
}



- (FMODPlugin::Scheduler*)getScheduler:(NSString*)identifier
{
    if (identifier!=nil) {
        string uuid = [identifier UTF8String];
        if (schedulers.find(uuid) != schedulers.end()) return schedulers[uuid];
    }
    return NULL;
}

- (bool)exists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || schedulers.find(uuid) == schedulers.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native scheduler not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    context = [CDVFMOD initFMODSystem];
    fmodscheduler = self;
    
    NSLog_d("FMOD Scheduler initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:schedulers) {
        it.second->unregisterAllEvents();
        it.second->stop();
    }
    //context->close();
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if (!uuid.empty()) {
        self->schedulers[uuid] = context->createScheduler(uuid);
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->schedulers.find(uuid);
        if (it != self->schedulers.end()) {
            self->schedulers[uuid]->close();
            self->schedulers.erase(it);
            [self eraseEventCallbackForUUID:uuid];
            [self unregisterMetroEventCallback:command];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (schedulers[uuid]->start())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Already started"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (schedulers[uuid]->stop())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Already stopped"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)pause:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        schedulers[uuid]->pause(true);
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                    callbackId:command.callbackId];
    }
}

- (void)resume:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        schedulers[uuid]->pause(false);
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                    callbackId:command.callbackId];
    }
}

- (void)reset:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        schedulers[uuid]->reset();
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                    callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Events
// -------------------------------------------------------------------------------------------------

- (void)registerEvent:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSDictionary *event = [args valueForKey:@"event"];
        
        if (event==Nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                             messageAsString:@"Invalid event parameter"];
            return;
        }
        
        NSNumber *type = [event valueForKey:@"type"];
        NSString *tags = [event valueForKey:@"tags"];
        NSString *player = [event valueForKey:@"player"];
        NSNumber *time = [event valueForKey:@"time"];
        NSNumber *samples = [event valueForKey:@"samples"];
        NSNumber *action = [event valueForKey:@"action"];
        NSNumber *once = [event valueForKey:@"once"];
        NSNumber *now = [event valueForKey:@"now"];
        
        
        if (tags==nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                             messageAsString:@"Invalid tag parameter"];
        }
        else if (samples==nil && time==nil) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                             messageAsString:@"Missing samples or time parameter"];
        }
        else {
            
            FMODPlugin::SCHEDULER_EVENT_TYPE t = static_cast<FMODPlugin::SCHEDULER_EVENT_TYPE>([type intValue]);
            if (t==FMODPlugin::FMOD_EVENT_TYPE_PLAYBACK) {
                FMODPlugin::Player *p = [CDVFMODPlayer getPlayer:player];
                FMODPlugin::SCHEDULER_EVENT_ACTION e = static_cast<FMODPlugin::SCHEDULER_EVENT_ACTION>([action intValue]);
                
                if (p==NULL) {
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                     messageAsString:@"Target player not found"];
                }
                else if (action!=nil && e!=0 && e<FMODPlugin::FMOD_EVENT_NONE) {
                    if (samples!=nil) {
                        schedulers[uuid]->registerEvent(t, [tags UTF8String], [samples unsignedLongLongValue], e, p, [once boolValue], [now boolValue]);
                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                    }
                    else if (time!=nil) {
                        schedulers[uuid]->registerEventInMilliseconds(t, [tags UTF8String], [time floatValue], e, p, [once boolValue], [now boolValue]);
                        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                    }
                }
                else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                      messageAsString:@"Invalid action"];
            }
            else if (t==FMODPlugin::FMOD_EVENT_TYPE_CALLBACK) {
                if (samples!=nil) {
                    schedulers[uuid]->registerEvent(t, [tags UTF8String], [samples unsignedLongLongValue], FMODPlugin::FMOD_EVENT_NONE, NULL, [once boolValue], [now boolValue]);
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                }
                else if (time!=nil) {
                    schedulers[uuid]->registerEventInMilliseconds(t, [tags UTF8String], [time floatValue], FMODPlugin::FMOD_EVENT_NONE, NULL, [once boolValue], [now boolValue]);
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                }
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"Invalid type"];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)unregisterEvent:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSString *tag= [args valueForKey:@"tag"];
        
        if (tag!=nil) {
            if (schedulers[uuid]->unregisterEvent([tag UTF8String]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"Tag not found"];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid tag"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)unregisterAllEvents:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        schedulers[uuid]->unregisterAllEvents();
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                    callbackId:command.callbackId];
    }
}

- (void)setEventCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    eventCallbacks[uuid] = command;
}

- (void)unsetEventCallback:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseEventCallbackForUUID:uuid])
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Event callback not found"];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (bool)eraseEventCallbackForUUID:(string)uuid
{
    auto it = eventCallbacks.find(uuid);
    if (it != eventCallbacks.end()) {
        eventCallbacks.erase(it);
        return true;
    }
    
    return false;
}


void FMOD_scheduler_event(const char *uuid, int type, const char *tags)
{
    if (fmodscheduler->eventCallbacks.find(uuid) != fmodscheduler->eventCallbacks.end()) {
        CDVInvokedUrlCommand *command = fmodscheduler->eventCallbacks[uuid];
        NSDictionary *args = @{
            @"type": [NSNumber numberWithInt: type],
            @"tags": [NSString stringWithFormat:@"%s", tags],
        };
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsDictionary:args];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodscheduler.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setPeriod:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *samples= [args valueForKey:@"samples"];
        
        if (samples!=nil) {
            if (schedulers[uuid]->setPeriod([samples unsignedLongLongValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid samples value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setPeriodInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *ms= [args valueForKey:@"ms"];
        
        if (ms!=nil && [ms intValue]>=0) {
            if (schedulers[uuid]->setPeriodInMilliseconds([ms floatValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid milliseconds value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setClock:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *samples= [args valueForKey:@"samples"];
        
        if (samples!=nil) {
            if (schedulers[uuid]->setClock([samples unsignedLongLongValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing clock value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setClockInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *ms= [args valueForKey:@"ms"];
        
        if (ms!=nil) {
            if (schedulers[uuid]->setClockInMilliseconds([ms floatValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing milliseconds value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Getter
// -------------------------------------------------------------------------------------------------

- (void)getDSPUsage:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *result = @{
            @"samples": [NSNumber numberWithUnsignedLongLong: schedulers[uuid]->getDSPClock()],
            @"millisecondes": [NSNumber numberWithFloat: schedulers[uuid]->getDSPClockInMilliseconds()],
            @"latency": [NSNumber numberWithFloat: schedulers[uuid]->getDSPCycleLatency()],
            @"expectedLatency": [NSNumber numberWithFloat: schedulers[uuid]->getDSPCycleExpectedLatency()],
        };
        
        [self.commandDelegate sendPluginResult:[CDVPluginResult
                                                resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:result]
                                    callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
//  Metronome
// -------------------------------------------------------------------------------------------------

- (void)registerMetroEventCallback:(CDVInvokedUrlCommand*)command
{
    //[CDVFMODMetronome registerEventCallback:command];
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    stepsCallbacks[uuid] = command;
}

- (void)unregisterMetroEventCallback:(CDVInvokedUrlCommand*)command
{
    //[CDVFMODMetronome unregisterEventCallback:command];
    CDVPluginResult *pluginResult = nil;

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseStepsCallbackForUUID:uuid])
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Steps callback not found"];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (bool)eraseStepsCallbackForUUID:(string)uuid
{
    auto it = stepsCallbacks.find(uuid);
    if (it != stepsCallbacks.end()) {
        stepsCallbacks.erase(it);
        return true;
    }
    
    return false;
}

- (void)setMetronome:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSString *metro = [args valueForKey:@"metronome"];
        
        FMODPlugin::Metronome *m = [CDVFMODMetronome getMetronome:metro];
        if (metro!=nil && m!=NULL) {
            if (schedulers[uuid]->setMetronome(m))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Target metronome not found"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setBeatsPerMinute:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *bpm= [args valueForKey:@"bpm"];
        
        if (bpm!=nil && [bpm intValue]>=0) {
            if (schedulers[uuid]->setBeatsPerMinute([bpm intValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid BPM value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setBeatPeriodInSamples:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *samples= [args valueForKey:@"samples"];
        
        if (samples!=nil && [samples intValue]>=0) {
            if (schedulers[uuid]->setBeatPeriodInSamples([samples unsignedLongLongValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid samples value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setBeatPeriodInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *ms= [args valueForKey:@"ms"];
        
        if (ms!=nil && [ms intValue]>=0) {
            if (schedulers[uuid]->setBeatPeriodInMilliseconds([ms floatValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid milliseconds value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

void FMOD_scheduler_stepevent(const char *uuid, unsigned long long step)
{
    if (fmodscheduler->stepsCallbacks.find(uuid) != fmodscheduler->stepsCallbacks.end()) {
        CDVInvokedUrlCommand *command = fmodscheduler->stepsCallbacks[uuid];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsNSUInteger:(NSUInteger)step];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodscheduler.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
