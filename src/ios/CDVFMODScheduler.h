// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#include "FMODPlugin.h"



@interface CDVFMODScheduler : CDVPlugin
{}

// Creates and initializes a new scheduler
- (void)init:(CDVInvokedUrlCommand*)command;
// Closes and frees the scheduler
- (void)close:(CDVInvokedUrlCommand*)command;
// Starts the scheduler
- (void)start:(CDVInvokedUrlCommand*)command;
// Stops the scheduler
- (void)stop:(CDVInvokedUrlCommand*)command;
// Pauses the scheduler
- (void)pause:(CDVInvokedUrlCommand*)command;
// Resumes the scheduler
- (void)resume:(CDVInvokedUrlCommand*)command;
// Reset the scheduler clock
- (void)reset:(CDVInvokedUrlCommand*)command;

// Schedule a new event
- (void)registerEvent:(CDVInvokedUrlCommand*)command;
// Delete one or more event
- (void)unregisterEvent:(CDVInvokedUrlCommand*)command;
// Deletes all recorded events
- (void)unregisterAllEvents:(CDVInvokedUrlCommand*)command;
// Sets events callback
- (void)setEventCallback:(CDVInvokedUrlCommand*)command;
// Remove events callback
- (void)unsetEventCallback:(CDVInvokedUrlCommand*)command;

// Attachs the scheduler to a specific external Metronome
- (void)setMetronome:(CDVInvokedUrlCommand*)command;
// Sets the beats frequency in BPM
- (void)setBeatsPerMinute:(CDVInvokedUrlCommand*)command;
// Sets the beat period in samples
- (void)setBeatPeriodInSamples:(CDVInvokedUrlCommand*)command;
// Sets the beat period in milliseconds
- (void)setBeatPeriodInMilliseconds:(CDVInvokedUrlCommand*)command;

// Register the metronome event callback
- (void)registerMetroEventCallback:(CDVInvokedUrlCommand*)command;
// Unregister the metronome event callback
- (void)unregisterMetroEventCallback:(CDVInvokedUrlCommand*)command;

// Sets the DSP cycle to the given number of samples
- (void)setPeriod:(CDVInvokedUrlCommand*)command;
// Sets the DSP cycle to the specified milliseconds
- (void)setPeriodInMilliseconds:(CDVInvokedUrlCommand*)command;
// Sets the DSP clock to the specified milliseconds
- (void)setClockInMilliseconds:(CDVInvokedUrlCommand*)command;
// Sets the DSP clock to the given number of samples
- (void)setClock:(CDVInvokedUrlCommand*)command;

// Retrieves information about the DSP clock and DSP cycle usage
- (void)getDSPUsage:(CDVInvokedUrlCommand*)command;

@end
