// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODMetronome.h"
#import "CDVFMOD.h"



static CDVFMODMetronome *fmodmetronome;

@interface CDVFMODMetronome()
@end

@implementation CDVFMODMetronome
{
    @private map<string, FMODPlugin::Metronome*> metronomes;
    @private map<string, CDVInvokedUrlCommand*> eventCallbacks;

    FMODPlugin::Context *context;
}



+ (FMODPlugin::Metronome*)getMetronome:(NSString*)identifier
{
    if (identifier!=nil) {
        string uuid = [identifier UTF8String];
        if (fmodmetronome->metronomes.find(uuid) != fmodmetronome->metronomes.end()) return fmodmetronome->metronomes[uuid];
    }
    return NULL;
}

- (bool)exists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || metronomes.find(uuid) == metronomes.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native metronome not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    fmodmetronome = self;
    
    NSLog_d("FMOD Metronome initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:metronomes) it.second->close();
    //context->close();
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];

    if (!uuid.empty()) {
        self->metronomes[uuid] = context->createMetronome(uuid);
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->metronomes.find(uuid);
        if (it != self->metronomes.end()) {
            self->metronomes[uuid]->close();
            self->metronomes.erase(it);
            [self eraseEventCallbackForUUID:uuid];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (metronomes[uuid]->start())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Already started"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (metronomes[uuid]->stop())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"started"];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Already stopped"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)reset:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        metronomes[uuid]->reset();
        [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setBeatsPerMinute:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *bpm= [args valueForKey:@"bpm"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (bpm!=nil && [bpm intValue]>=0) {
            if (metronomes[uuid]->setBeatsPerMinute([bpm intValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsString:@"Invalid BPM value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setBeatPeriodInSamples:(CDVInvokedUrlCommand *)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *samples= [args valueForKey:@"samples"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (samples!=nil && [samples intValue]>=0) {
            if (metronomes[uuid]->setBeatPeriodInSamples([samples unsignedLongLongValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsString:@"Invalid samples value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setBeatPeriodInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *ms= [args valueForKey:@"ms"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (ms!=nil && [ms intValue]>=0) {
            if (metronomes[uuid]->setBeatPeriodInMilliseconds([ms floatValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsString:@"Invalid milliseconds value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setDSPClock:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *samples= [args valueForKey:@"samples"];

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (samples!=nil) {
            if (metronomes[uuid]->setDSPClock([samples unsignedLongLongValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsString:@"Missing clock value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setDSPClockInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *ms= [args valueForKey:@"ms"];

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (ms!=nil) {
            if (metronomes[uuid]->setDSPClockInMilliseconds([ms floatValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsString:@"Missing milliseconds value"];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Getter
// -------------------------------------------------------------------------------------------------

- (void)getDSPUsage:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self exists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *result = @{
            @"samples": [NSNumber numberWithUnsignedLongLong: metronomes[uuid]->getDSPClock()],
            @"millisecondes": [NSNumber numberWithFloat: metronomes[uuid]->getDSPClockInMilliseconds()],
            @"latency": [NSNumber numberWithFloat: metronomes[uuid]->getDSPCycleLatency()],
            @"expectedLatency": [NSNumber numberWithFloat: metronomes[uuid]->getDSPCycleExpectedLatency()],
        };
        
        [self.commandDelegate sendPluginResult:[CDVPluginResult
                              resultWithStatus:CDVCommandStatus_OK messageAsDictionary:result]
                                    callbackId:command.callbackId];
    }
    
}



// -------------------------------------------------------------------------------------------------
//  Events
// -------------------------------------------------------------------------------------------------

+ (void)registerEventCallback:(CDVInvokedUrlCommand*)command
{
    [fmodmetronome registerEventCallback:command];
}

+ (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command
{
    [fmodmetronome unregisterEventCallback:command];
}

- (void)registerEventCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    eventCallbacks[uuid] = command;
}

- (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseEventCallbackForUUID:uuid])
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Event callback not found"];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (bool)eraseEventCallbackForUUID:(string)uuid
{
    auto it = eventCallbacks.find(uuid);
    if (it != eventCallbacks.end()) {
        eventCallbacks.erase(it);
        return true;
    }
    
    return false;
}

void FMOD_metronome_event(const char *uuid, unsigned long long step)
{
    if (fmodmetronome->eventCallbacks.find(uuid) != fmodmetronome->eventCallbacks.end()) {
        CDVInvokedUrlCommand *command = fmodmetronome->eventCallbacks[uuid];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsNSUInteger:(NSUInteger)step];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodmetronome.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
