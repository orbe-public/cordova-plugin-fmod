// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODAnalyser.h"
#import "CDVFMOD.h"




static CDVFMODAnalyser *fmodanalyser;

@interface CDVFMODAnalyser()
@end

@implementation CDVFMODAnalyser
{
    @private map<string, FMODPlugin::Analyser*> analysers;
    @private map<string, CDVInvokedUrlCommand*> dataCallbacks; // list of each analyser callbacks
    
    FMODPlugin::Context *context;
}



- (bool)analyserExists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || analysers.find(uuid) == analysers.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native analyser not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    fmodanalyser = self;
    
    NSLog_d("FMOD Analyser initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:analysers) it.second->close();
    //context->close();
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];

    if (!uuid.empty()) {
        self->analysers[uuid] = context->createAnalyser(uuid);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->analysers.find(uuid);
        if (it != self->analysers.end()) {
            self->analysers[uuid]->~Analyser();
            self->analysers.erase(it);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self analyserExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSArray *options = [args valueForKey:@"options"];
        
        if ([options containsObject:@"fft"]) analysers[uuid]->setFFTAnalysis(true);
        if ([options containsObject:@"metering"]) analysers[uuid]->setSignalMetering(true);
        
        if (analysers[uuid]->start()) pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", analysers[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self analyserExists:uuid fromUrlCommand:command];
    
    if (exists) {
        analysers[uuid]->stop();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setInput:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self analyserExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSDictionary *unit = [args valueForKey:@"audio"];
        
        if (unit!=nil) {
            NSString *input = [unit valueForKey:@"uuid"];
            FMODPlugin::AudioUnit *source = context->getAudioUnit([input UTF8String]);
            
            if (unit) {
                if (analysers[uuid]->setInput(source))
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"The internal audio unit could not be found"];
                
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing audio unit"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setPeriodInMilliseconds:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self analyserExists:uuid fromUrlCommand:command];
    
    if (exists) {
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *ms= [args valueForKey:@"ms"];
        
        if (ms!=nil) {
            analysers[uuid]->setPeriodInMilliseconds([ms floatValue]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Invalid milliseconds value"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Events
// -------------------------------------------------------------------------------------------------

- (void)setCallback:(CDVInvokedUrlCommand*)command
{
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    dataCallbacks[uuid] = command;
}

- (void)unsetCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseCallbackForUUID:uuid]) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (bool)eraseCallbackForUUID:(string)uuid
{
    auto it = dataCallbacks.find(uuid);
    if (it != dataCallbacks.end()) {
        dataCallbacks.erase(it);
        return true;
    }
    
    return false;
}

void FMOD_analyser_data(const char *uuid, void *data)
{
    if (fmodanalyser->dataCallbacks.find(uuid) != fmodanalyser->dataCallbacks.end()) {

        FMODPlugin::FMOD_DSP_ANALYSER_DATA *analysis = (FMODPlugin::FMOD_DSP_ANALYSER_DATA*)data;
        NSDictionary *result = @{
            @"fft": @{
                @"centroid": [NSNumber numberWithFloat: analysis->fft.centroid],
            },
            @"metering": @{
                @"peak": @[[NSNumber numberWithFloat: analysis->metering.peak[0]], [NSNumber numberWithFloat: analysis->metering.peak[1]]],
                @"rms": @[[NSNumber numberWithFloat: analysis->metering.rms[0]], [NSNumber numberWithFloat: analysis->metering.rms[1]]]
            }
        };
        
        CDVInvokedUrlCommand *command = fmodanalyser->dataCallbacks[uuid];
        CDVPluginResult *pluginResult = pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:result];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodanalyser.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
