// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#include "FMODPlugin.h"



@interface CDVFMOD : CDVPlugin
{}

// Initialize once the FMOD core system
+ (FMODPlugin::Context*)initFMODSystem;
// Extract the native object identifier from the JS command
+ (string)getUUIDFromUrlCommand:(CDVInvokedUrlCommand*)command fromClass:(CDVPlugin*)plugin;

// Provides informations about the internal FMOD library version
- (void)getVersion:(CDVInvokedUrlCommand*)command;
// Retrieves the number of currently playing channels
- (void)getChannelsPlaying:(CDVInvokedUrlCommand*)command;
// Retrieves the amount of CPU used for different parts of the Core engine
- (void)getCPUUsage:(CDVInvokedUrlCommand*)command;
// Retrieves the number of milliseconds elapsed since the FMOD system was started
- (void)getEllapsedTime:(CDVInvokedUrlCommand*)command;

// Suspend mixer thread and relinquish usage of audio hardware while maintaining internal state
- (void)suspendAudioOutput:(CDVInvokedUrlCommand*)command;
// Resume mixer thread and reacquire access to audio hardware
- (void)resumeAudioOutput:(CDVInvokedUrlCommand*)command;
// Register callback to handle audio interruptions.
- (void)registerAudioInterruptCallback:(CDVInvokedUrlCommand*)command;

@end
