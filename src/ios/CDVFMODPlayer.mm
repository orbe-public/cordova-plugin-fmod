// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODPlayer.h"
#import "CDVFMOD.h"




static CDVFMODPlayer *fmodplayer;

@interface CDVFMODPlayer()
@end

@implementation CDVFMODPlayer
{
    @private map<string, FMODPlugin::Player*> players;
    @private map<string, CDVInvokedUrlCommand*> eventCallbacks;
    
    FMODPlugin::Context *context;
}



+ (FMODPlugin::Player*)getPlayer:(NSString*)identifier
{
    if (identifier!=nil) {
        string uuid = [identifier UTF8String];
        if (fmodplayer->players.find(uuid) != fmodplayer->players.end()) return fmodplayer->players[uuid];
    }
    return NULL;
}

- (bool)playerExists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || players.find(uuid) == players.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native player not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    fmodplayer = self;
    
    NSLog_d("FMOD Player initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:players) it.second->release();
    context->close();
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];

    if (!uuid.empty()) {
        self->players[uuid] = context->createPlayer(uuid);
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->players.find(uuid);
        if (it != self->players.end()) {
            if (self->players[uuid]->release()) {
                //it->second->~Player();
                #if CORDOVA_PLUGIN_RESONANCEAUDIO
                it->second->releaseResonanceAudioDSP();
                #endif
                self->players.erase(it);
                [self eraseCallbackForUUID:uuid];
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:[NSString stringWithFormat:@"%s", self->players[uuid]->getLastErrorMessage()]];
            
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)load:(CDVInvokedUrlCommand*)command
{
    NSDictionary *args      = [command argumentAtIndex:0];
    NSString *source        = [args valueForKey:@"source"];
    NSNumber *mode          = [args valueForKey:@"loop"];
    NSNumber *stream        = [args valueForKey:@"stream"];
    NSNumber *autoplay      = [args valueForKey:@"autoplay"];
    NSMutableArray *buffer  = [args valueForKey:@"buffer"];
    NSString *dls           = [args valueForKey:@"dls"];
    
    
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        bool exists = [self playerExists:uuid fromUrlCommand:command];
        
        
        if (exists) {
            if (dls!=nil) self->players[uuid]->setMidiSoundBank([dls UTF8String]);
            if (source!=nil) {
                if (self->players[uuid]->load([source UTF8String], [mode intValue], [stream boolValue], [autoplay boolValue]))
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                      messageAsString:[NSString stringWithFormat:@"%s", self->players[uuid]->getLastErrorMessage()]];
            }
            else if (buffer!=nil) {
                int length = (int)[buffer count];
                char data[length];
                for (int i = 0; i<length; ++i) data[i] = [[buffer objectAtIndex:i] charValue];
                
                if (self->players[uuid]->loadFromBuffer(data, length, [mode intValue], [stream boolValue], [autoplay boolValue]))
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                      messageAsString:[NSString stringWithFormat:@"%s", self->players[uuid]->getLastErrorMessage()]];
                
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"Invalid source"];

            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}

- (void)release:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        bool exists = [self playerExists:uuid fromUrlCommand:command];
        
        if (exists) {
            if (self->players[uuid]->release())
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:[NSString stringWithFormat:@"%s", self->players[uuid]->getLastErrorMessage()]];
            
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}

- (void)play:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->play())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->stop())
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)pause:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *pause = [args valueForKey:@"paused"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->pause([pause boolValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Playback status
// -------------------------------------------------------------------------------------------------

- (void)isPaused:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        bool paused = players[uuid]->isPaused();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:paused];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)isPlaying:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        bool playing = players[uuid]->isPlaying();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:playing];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setVolume:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *volume = [args valueForKey:@"value"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setVolume([volume floatValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setMute:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *mute = [args valueForKey:@"muted"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setMute([mute boolValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setPan:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *mute = [args valueForKey:@"pan"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setPan([mute floatValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setFadeIn:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *ms = [args valueForKey:@"ms"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setFadeIn([ms unsignedIntValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setFadeOut:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *ms = [args valueForKey:@"ms"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setFadeOut([ms unsignedIntValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setLoopMode:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *mode = [args valueForKey:@"mode"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setMode([mode intValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setPosition:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *position = [args valueForKey:@"position"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setPosition([position unsignedIntValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setPitch:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    NSDictionary *args = [command argumentAtIndex:0];
    NSNumber *rate = [args valueForKey:@"rate"];
    NSNumber *shift = [args valueForKey:@"shift"];
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (players[uuid]->setPitch([rate floatValue], [shift boolValue]))
             pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:[NSString stringWithFormat:@"%s", players[uuid]->getLastErrorMessage()]];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Getters
// -------------------------------------------------------------------------------------------------

- (void)getLength:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        unsigned int length = players[uuid]->getLength();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:length];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)getPosition:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        unsigned int pos = players[uuid]->getPosition();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:pos];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)getMute:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self playerExists:uuid fromUrlCommand:command];
    
    if (exists) {
        bool state = players[uuid]->getMute();
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                           messageAsBool:state];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Events
// -------------------------------------------------------------------------------------------------

- (void)registerEventCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    eventCallbacks[uuid] = command;
}

- (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;

    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseCallbackForUUID:uuid])
         pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                          messageAsString:@"Event callback not found"];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (bool)eraseCallbackForUUID:(string)uuid
{
    auto it = eventCallbacks.find(uuid);
    if (it != eventCallbacks.end()) {
        eventCallbacks.erase(it);
        return true;
    }
    
    return false;
}

void FMOD_channel_event(const char* uuid)
{
    unsigned int length = 0;
    unsigned int position = 0;
    
    if (fmodplayer->players.find(uuid) != fmodplayer->players.end()) {
        length = fmodplayer->players[uuid]->getLength(FMOD_TIMEUNIT_PCM);
        position = fmodplayer->players[uuid]->getPosition(FMOD_TIMEUNIT_PCM);
        fmodplayer->players[uuid]->onPlaybackEnded();
    }
    
    if (fmodplayer->eventCallbacks.find(uuid) != fmodplayer->eventCallbacks.end()) {
        NSString *message = position >= length ? @"ended" : @"stopped";
        CDVInvokedUrlCommand *command = fmodplayer->eventCallbacks[uuid];
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                          messageAsString:message];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodplayer.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end
