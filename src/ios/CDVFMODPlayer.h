// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Cordova/CDVPlugin.h>
#include "FMODPlugin.h"



@interface CDVFMODPlayer : CDVPlugin
{}

// Get the player identified by the specified ID
+ (FMODPlugin::Player*)getPlayer:(NSString*)identifier;

// Creates and initializes a new player
- (void)init:(CDVInvokedUrlCommand*)command;
// Closes and frees a player and its resources
- (void)close:(CDVInvokedUrlCommand*)command;
// Loads a sound into memory or opens it for streaming
- (void)load:(CDVInvokedUrlCommand*)command;
// Unload a sound and cleanup all resources
- (void)release:(CDVInvokedUrlCommand*)command;
// Start the playback of the currently loaded sound
- (void)play:(CDVInvokedUrlCommand*)command;
// Stop the playback of the sound
- (void)stop:(CDVInvokedUrlCommand*)command;
// Retrieves the playing state
- (void)isPlaying:(CDVInvokedUrlCommand*)command;
// Pause or resume the playback of the sound
- (void)pause:(CDVInvokedUrlCommand*)command;
// Retrieves the paused state
- (void)isPaused:(CDVInvokedUrlCommand*)command;
// Sets the volume level
- (void)setVolume:(CDVInvokedUrlCommand*)command;
// Sets the mute state
- (void)setMute:(CDVInvokedUrlCommand*)command;
// Retrieves the mute state
- (void)getMute:(CDVInvokedUrlCommand*)command;
// Sets the fade in of the sound at the beginning of its playback
- (void)setFadeIn:(CDVInvokedUrlCommand*)command;
// Sets the fade out of the sound at the end of its playback
- (void)setFadeOut:(CDVInvokedUrlCommand*)command;
// Sets the loop mode of the sound
- (void)setLoopMode:(CDVInvokedUrlCommand*)command;
// Sets the relative pitch / playback rate
- (void)setPitch:(CDVInvokedUrlCommand*)command;
// Sets the left/right pan level
- (void)setPan:(CDVInvokedUrlCommand*)command;
// Sets the current playback position in milliseconds
- (void)setPosition:(CDVInvokedUrlCommand*)command;
// Retrieves the current playback position in milliseconds
- (void)getPosition:(CDVInvokedUrlCommand*)command;
// Retrieves the sound length in milliseconds
- (void)getLength:(CDVInvokedUrlCommand*)command;

// Register the player event callback
- (void)registerEventCallback:(CDVInvokedUrlCommand*)command;
// Unregister the player event callback
- (void)unregisterEventCallback:(CDVInvokedUrlCommand*)command;

@end
