// The MIT License (MIT)
// Copyright © 2021 Orbe
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


#import "CDVFMODRecorder.h"
#import "CDVFMOD.h"




static CDVFMODRecorder *fmodrecorder;

@interface CDVFMODRecorder()
@end

@implementation CDVFMODRecorder
{
    
    FMODPlugin::Context *context;
    
    @private map<string, FMODPlugin::Recorder*> recorders;      // all recorders
    @private map<string, CDVInvokedUrlCommand*> endedCallbacks; // list of each recorder callbacks
    @private map<string, NSString*> audiofiles;                 // the user file paths stored by recorder id
    
    NSMutableArray *files;  // list of all recorded audio files
}



- (bool)recorderExists:(string)uuid fromUrlCommand:(CDVInvokedUrlCommand*)command
{
    // not found
    if (uuid.empty() || recorders.find(uuid) == recorders.end()) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"Native recorder not found"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        return false;
    }
    
    return true;
}



- (void)pluginInitialize
{
    // initialize FMOD context
    context = [CDVFMOD initFMODSystem];
    fmodrecorder = self;
    
    // initialize array
    files = [[NSMutableArray alloc]init];
    
    NSLog_d("FMOD Recorder initialized");
}

- (void)onAppTerminate
{
    // cleanup on app exit
    for(auto it:recorders) it.second->close();
    //context->close();
    
    // cleanup temporary files
    NSString *item;
    NSFileManager *manager = [NSFileManager defaultManager];
    for(item in files) {
        if ([manager fileExistsAtPath:item]) {
            NSLog_d([[NSString stringWithFormat:@"Delete recorded file %@", item] UTF8String]);
            [manager removeItemAtPath:item error:nil];
        }
    }
}



- (void)init:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];

    if (!uuid.empty()) {
        self->recorders[uuid] = context->createRecorder(uuid);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)close:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult *pluginResult = nil;
        string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
        
        auto it = self->recorders.find(uuid);
        if (it != self->recorders.end()) {
            self->recorders[uuid]->~Recorder();
            self->recorders.erase(it);
            [self eraseCallbackForUUID:uuid];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}



// -------------------------------------------------------------------------------------------------
// Actions
// -------------------------------------------------------------------------------------------------

- (void)start:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSString *filepath = [args valueForKey:@"filepath"];
        NSString *usrpath  = [NSString stringWithFormat:@"%s", FMOD_tempory_path([filepath UTF8String])];
        NSString *recpath  = [usrpath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        
        if ( FMOD_mkdir([recpath UTF8String]) ) {
            if (recorders[uuid]->start([recpath UTF8String])) {
                audiofiles[uuid] = usrpath;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:[NSString stringWithFormat:@"%s", recorders[uuid]->getLastErrorMessage()]];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Permission denied to save file to the target directory"];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)stop:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        if (audiofiles.find(uuid) != audiofiles.end()) {
            recorders[uuid]->stop();
            CDVPluginResult *pluginResult = pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }
}



// -------------------------------------------------------------------------------------------------
// Setters
// -------------------------------------------------------------------------------------------------

- (void)setInput:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSDictionary *unit = [args valueForKey:@"audio"];
        
        if (unit!=nil) {
            NSString *input = [unit valueForKey:@"uuid"];
            FMODPlugin::AudioUnit *source = context->getAudioUnit([input UTF8String]);
            
            if (unit) {
                if (recorders[uuid]->setInput(source))
                     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            }
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                  messageAsString:@"The internal audio unit could not be found"];
                
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing audio unit"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setSampleRate:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *samplerate = [args valueForKey:@"samplerate"];
        
        if (samplerate!=nil) {
            if (recorders[uuid]->setSampleRate([samplerate intValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing samplerate value"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)setAudioFormat:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *format = [args valueForKey:@"format"];
        
        if (format!=nil) {
            FMOD_SOUND_FORMAT sf = static_cast<FMOD_SOUND_FORMAT>([format intValue]);
            if (recorders[uuid]->setAudioFormat(sf))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing format value"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)ignoreLeadingSilence:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult *pluginResult = nil;
    
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    bool exists = [self recorderExists:uuid fromUrlCommand:command];
    
    if (exists) {
        
        NSDictionary *args = [command argumentAtIndex:0];
        NSNumber *enable = [args valueForKey:@"enable"];
        NSNumber *level = [args valueForKey:@"level"];
        
        if (enable!=nil && level!=nil) {
            if (recorders[uuid]->ignoreLeadingSilence([enable boolValue], [level unsignedIntValue]))
                 pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        else pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                              messageAsString:@"Missing arguments values"];
    
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}



// -------------------------------------------------------------------------------------------------
// Events
// -------------------------------------------------------------------------------------------------

- (void)registerEndedCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    endedCallbacks[uuid] = command;
}

- (void)unregisterEndedCallback:(CDVInvokedUrlCommand*)command
{
    string uuid = [CDVFMOD getUUIDFromUrlCommand:command fromClass:self];
    if ([self eraseCallbackForUUID:uuid]) {
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (bool)eraseCallbackForUUID:(string)uuid
{
    auto it = endedCallbacks.find(uuid);
    if (it != endedCallbacks.end()) {
        endedCallbacks.erase(it);
        return true;
    }
    
    return false;
}

void FMOD_recorder_ended(const char *uuid, const char *path, bool error)
{
    if (fmodrecorder->endedCallbacks.find(uuid) != fmodrecorder->endedCallbacks.end()) {
        
        auto it = fmodrecorder->audiofiles.find(uuid);
        NSString* message = error ? @"unable to save audio file" : [NSString stringWithFormat:@"%@", it->second];
        
        // store created file
        // and remove the referenced file for the target recorder
        if (!error) {
            [fmodrecorder->files addObject:[NSString stringWithFormat:@"%s", path]];
            fmodrecorder->audiofiles.erase(it);
        }
        else NSLog_e([message UTF8String]);
        
        CDVInvokedUrlCommand *command = fmodrecorder->endedCallbacks[uuid];
        CDVPluginResult *pluginResult = pluginResult = [CDVPluginResult resultWithStatus:(error?CDVCommandStatus_ERROR:CDVCommandStatus_OK)
                                                          messageAsString:message];
        
        [pluginResult setKeepCallbackAsBool:YES];
        [fmodrecorder.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

@end

